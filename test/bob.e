note
	description: "Summary description for {BOB}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BOB

inherit
	THREAD
		rename
			make as make_thread
		end

create
	make

feature
	make
		do
			make_thread
		end

feature -- Attributes

	public_key: detachable SODIUM_KEY

	private_key: detachable SODIUM_KEY

feature -- Code

	execute
		do

		end
end
