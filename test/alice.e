note
	description: "Summary description for {ALICE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ALICE

inherit
	THREAD
		rename
			make as make_thread
		end

create
	make

feature
	make
		do
			make_thread
		end

	execute
		do

		end

end
