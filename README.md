# eiffel-libsodium 
is an Eiffel library for access to the cryptographic library 'libsodium'.

# Installation

## Compilation on Unix-like systems
Sodium is a shared library with a machine-independent set of headers, so that it can easily be used by 3rd party projects.

The library is built using autotools, making it easy to package.

Installation is trivial, and both compilation and testing can take advantage of multiple CPU cores.

Download a tarball of libsodium, preferably the latest stable version, then follow the ritual:

```
./configure
make && make check
sudo make install
```

Since different files are compiled for different CPU classes, and to prevent unwanted optimizations, link-time optimization (LTO) should not be used.

On Linux, if the process hangs at the make check step, your system PRG may not have been properly seeded. Please refer to the notes in the "Usage" section for ways to address this.

Also on Linux, like any manually installed library, running the `ldconfig` command is required in order to make the dynamic linker aware of the new library.

```
sudo ldconfig
```

The library is called sodium (use -lsodium to link it), and proper compilation/linker flags can be obtained using `pkg-config` on systems where it is available:

```
pkg-config --cflags --libs libsodium
```

```
-I/usr/local/include -L/usr/local/lib -lsodium
```


## Compilation on Windows
Compilation on Windows is usually not required, as pre-built libraries for MinGW and Visual Studio are available (see below).
However, if you want to compile it yourself, start by cloning the stable branch from the Git repository.
Visual Studio solutions can be then found in the builds/msvc directory.

In order to compile with MingW, run either ./dist-build/msys2-win32.sh or ./dist-build/msys2-win64.sh for Win32 or x64 targets.
Alternatively, you can build and install libsodium using vcpkg dependency manager:

```
git clone https://github.com/Microsoft/vcpkg.git
cd vcpkg
./bootstrap-vcpkg.bat
./vcpkg integrate install
./vcpkg install libsodium:x64-Windows
```

Then copy the `libsodium.lib` to the path $YOUR_PATH/eiffel-libsodium/C/lib, where $YOUR_PATH is the place where you have downloaded the 
eiffel-libsodium library.
Be sure to have the `libsodium.dll` in the Path when you run an application using eiffel-libsodium library.


To learn more about how to install LibSodium read the following [LibSodium Tutorial](https://libsodium.gitbook.io/doc/installation).
