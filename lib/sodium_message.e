note
	description: "Summary description for {SODIUM_MESSAGE}."
	author: "Howard Thomson"
	date: "$Date$"
	revision: "$Revision$"

class
	SODIUM_MESSAGE

inherit
	LIBSODIUM
		undefine is_equal, copy end
	MANAGED_POINTER
		export {MANAGED_POINTER} all
		end
create
	make_from_string,
	make_from_encrypted

feature -- Creation
	make_from_string (a_string: STRING)
		do
			make_from_pointer (a_string.area.base_address, a_string.count)
		end

	make_from_encrypted (a_encrypted: SODIUM_ENCRYPTED; a_nonce: SODIUM_NONCE; a_key: SODIUM_KEY)
		do
			make (a_encrypted.count - crypto_secretbox_macbytes)
			secretbox_open_easy (Current, a_encrypted, a_nonce, a_key)
		end

end
