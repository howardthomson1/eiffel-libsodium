note
	description: "Summary description for {SODIUM_KEY}."
	author: "Howard Thomson"
	date: "$Date$"
	revision: "$Revision$"

	TODO: "[
		Consider how crypto_box_keypair can generate a pair of:
			SODIUM_PRIVATE_KEY / SODIUM_PUBLIC_KEY
		Varying key sizes ? All 32 bytes, except for [deprecated] shorthash
	]"

class
	SODIUM_KEY

inherit
	LIBSODIUM
		undefine is_equal, copy, default_create end
	MANAGED_POINTER
		export {MANAGED_POINTER} all
		undefine default_create
		end
create
	default_create
feature
	default_create
		do
			make (crypto_secretbox_keybytes)
			secretbox_keygen (Current)
		end

end
