note
	description: "Summary description for {LIBSODIUM}."
	author: "Howard Thomson"
	date: "$Date$"
	revision: "$Revision$"

class
	LIBSODIUM

inherit
	LIBSODIUM_API
create
	init

feature {NONE} --Initialization

	init
		do
			if sodium_init < 0 then
					-- Panic ...
				check init_failure: False end
			else
				init_successful := True
			end
		end

feature -- Version

	version_string: STRING
		do
			create Result.make_from_c (sodium_version_string)
		end

feature -- Access

	init_successful: BOOLEAN

	new_randombytes_buffer (n: INTEGER): LIBSODIUM_RANDOMBYTES
		do
			create Result.make (n)
		end

	secretbox_keygen (a_buffer: MANAGED_POINTER)
		do
			c_crypto_secretbox_keygen (a_buffer.item)
		end

	secretbox_easy (a_encrypted, a_msg, a_nonce, a_key: MANAGED_POINTER)
		local
			l_int: INTEGER
		do
				-- TODO
			l_int := crypto_secretbox_easy (a_encrypted.item, a_msg.item, a_msg.count, a_nonce.item, a_key.item)
			print ("secretbox_easy result = "); print (l_int.out); print ("%N")
			check result_equal_zero_or_minus_1: l_int = 0 or l_int = -1 end

		end

	secretbox_open_easy (a_decrypted, a_encrypted, a_nonce, a_key: MANAGED_POINTER)
		local
			l_int: INTEGER
		do
				-- TODO
			l_int := crypto_secretbox_open_easy (a_decrypted.item, a_encrypted.item, a_encrypted.count, a_nonce.item, a_key.item)
			check l_int = 0 end
		end

	compare (a_buffer, b_buffer: MANAGED_POINTER): BOOLEAN
		require
			same_size: a_buffer.count = b_buffer.count
		do
			Result := sodium_compare (a_buffer.item, b_buffer.item, a_buffer.count) = 0
		end

	bin_to_hex (a_bin: MANAGED_POINTER): STRING
		local
			l_count: INTEGER
			l_result: POINTER
		do
			l_count := a_bin.count
			create Result.make (l_count * 2)
			l_result := sodium_bin2hex (Result.area.base_address, l_count * 2, a_bin.item, l_count)
		end

end
