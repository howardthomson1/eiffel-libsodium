note
	description: "Summary description for {SODIUM_GENERIC_HASH}."
	author: "Howard Thomson"
	date: "$Date$"

class
	SODIUM_GENERIC_HASH

inherit
	LIBSODIUM
		undefine is_equal, copy, default_create end
	MANAGED_POINTER
		export {MANAGED_POINTER} all
		undefine default_create
		end

create
	default_create,
	make_with_key

feature -- Creation

	default_create
		do
			make (crypto_generichash_bytes)
		end

	make_with_key (a_key: SODIUM_KEY)
		do
			key := a_key
		end

feature -- Attributes

	key: detachable SODIUM_KEY

	set_key (a_key: SODIUM_KEY)
		do
			key := a_key
		end

feature {NONE} -- Internal state, multiple chunk

	hash_state: detachable MANAGED_POINTER

feature -- Hashing, single buffer

	hash (a_buffer: MANAGED_POINTER)
		local
			i: INTEGER
		do
			if attached key as l_key then
				i := crypto_generichash (item, count, a_buffer.item, a_buffer.count, l_key.item, l_key.count)
			else
				i := crypto_generichash (item, count, a_buffer.item, a_buffer.count, default_pointer, 0)
			end
			check return_ok: i = 0 end
		end

feature -- Hashing, multiple buffers

	hash_init
		local
			i: INTEGER
		do
			if not attached hash_state then
				create hash_state.make (crypto_generichash_statebytes)
			end
			if attached hash_state as l_state then
				if attached key as l_key then
					i := c_crypto_generichash_init (l_state.item, l_key.item, l_key.count, count)
				else
					i := c_crypto_generichash_init (l_state.item, default_pointer, 0, count)
				end
			end
		end

	hash_update (a_buffer: MANAGED_POINTER)
		local
			i: INTEGER
		do
			if attached hash_state as l_state then
				i := c_crypto_generichash_update (l_state.item, a_buffer.item, a_buffer.count)
			end
		end

	hash_update_partial (a_buffer: MANAGED_POINTER; a_count: INTEGER)
		require
			count_gt_0:  a_count > 0
			count_le_buffer_count: a_count <= a_buffer.count
		local
			i: INTEGER
		do
			if attached hash_state as l_state then
				i := c_crypto_generichash_update (l_state.item, a_buffer.item, a_count)
			end
		end



	hash_final
		local
			i: INTEGER
		do
			if attached hash_state as l_state then
				i := c_crypto_generichash_final (l_state.item, item, count)
			end
		end

feature -- Conversion

	as_hex_string: STRING
		local
			p: POINTER
		do
			create Result.make_filled ('+', (count * 2) + 1)
			p := sodium_bin2hex (Result.area.base_address, Result.capacity,
				item, count)
		end

end
