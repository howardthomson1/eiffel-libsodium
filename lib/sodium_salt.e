note
	description: "Summary description for {SODIUM_SALT}."
	author: "Howard Thomson"
	date: "$Date$"
	revision: "$Revision$"

class
	SODIUM_SALT

inherit
	LIBSODIUM
		undefine is_equal, copy, default_create end
	MANAGED_POINTER
		export {MANAGED_POINTER} all
		undefine default_create
		end
create
	default_create
feature
	default_create
		do
			make (crypto_pwhash_saltbytes)
			randombytes_buf (item, crypto_pwhash_saltbytes)
		end

end
