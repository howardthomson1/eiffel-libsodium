note
	description: "Summary description for {LIBSODIUM_RANDOMBYTES}."
	author: "Howard Thomson"
	date: "$Date$"
	revision: "$Revision$"

class
	LIBSODIUM_RANDOMBYTES

inherit
	LIBSODIUM_API
	--redefine
	--	default_create

create
	make

feature -- Creation

	item: MANAGED_POINTER

	make (n: INTEGER)
		do
			create item.make (n)
		end

end
