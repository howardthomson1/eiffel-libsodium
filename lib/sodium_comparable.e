note
	description: "Summary description for {SODIUM_COMPARABLE}."
	author: "Howard Thomson"
	date: "24-Nov-2020"

	TODO: "[
		Reconsider the appropriateness of this class ...
	]"

deferred class
	SODIUM_COMPARABLE

inherit
	LIBSODIUM_API

feature {SODIUM_COMPARABLE}

	sodium_is_equal (other: like Current): BOOLEAN
		require
			same_size: count = other.count
		local
			i: INTEGER
		do
			i := sodium_compare (item, other.item, count)
			if i = 0 then
				Result := True
			end
		end

feature {SODIUM_COMPARABLE}

	item: POINTER
		deferred
		end

	count: INTEGER_32
		deferred
		end

end
