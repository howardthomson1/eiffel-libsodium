# 1 "/usr/include/sodium.h"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 32 "<command-line>" 2
# 1 "/usr/include/sodium.h"




# 1 "/usr/include/sodium/version.h" 1




# 1 "/usr/include/sodium/export.h" 1
# 6 "/usr/include/sodium/version.h" 2
# 17 "/usr/include/sodium/version.h"
__attribute__ ((visibility ("default")))
const char *sodium_version_string(void);

__attribute__ ((visibility ("default")))
int sodium_library_version_major(void);

__attribute__ ((visibility ("default")))
int sodium_library_version_minor(void);

__attribute__ ((visibility ("default")))
int sodium_library_minimal(void);
# 6 "/usr/include/sodium.h" 2

# 1 "/usr/include/sodium/core.h" 1
# 11 "/usr/include/sodium/core.h"
__attribute__ ((visibility ("default")))
int sodium_init(void)
            __attribute__ ((warn_unused_result));



__attribute__ ((visibility ("default")))
int sodium_set_misuse_handler(void (*handler)(void));

__attribute__ ((visibility ("default")))
void sodium_misuse(void)
            __attribute__ ((noreturn));
# 8 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_aead_aes256gcm.h" 1
# 24 "/usr/include/sodium/crypto_aead_aes256gcm.h"
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 149 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 3 4

# 149 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 3 4
typedef long int ptrdiff_t;
# 216 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 3 4
typedef long unsigned int size_t;
# 328 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 3 4
typedef int wchar_t;
# 426 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 3 4
typedef struct {
  long long __max_align_ll __attribute__((__aligned__(__alignof__(long long))));
  long double __max_align_ld __attribute__((__aligned__(__alignof__(long double))));
# 437 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 3 4
} max_align_t;
# 25 "/usr/include/sodium/crypto_aead_aes256gcm.h" 2
# 34 "/usr/include/sodium/crypto_aead_aes256gcm.h"

# 34 "/usr/include/sodium/crypto_aead_aes256gcm.h"
__attribute__ ((visibility ("default")))
int crypto_aead_aes256gcm_is_available(void);


__attribute__ ((visibility ("default")))
size_t crypto_aead_aes256gcm_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_aead_aes256gcm_nsecbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_aead_aes256gcm_npubbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_aead_aes256gcm_abytes(void);




__attribute__ ((visibility ("default")))
size_t crypto_aead_aes256gcm_messagebytes_max(void);

typedef __attribute__ ((aligned(16))) unsigned char crypto_aead_aes256gcm_state[512];

__attribute__ ((visibility ("default")))
size_t crypto_aead_aes256gcm_statebytes(void);

__attribute__ ((visibility ("default")))
int crypto_aead_aes256gcm_encrypt(unsigned char *c,
                                  unsigned long long *clen_p,
                                  const unsigned char *m,
                                  unsigned long long mlen,
                                  const unsigned char *ad,
                                  unsigned long long adlen,
                                  const unsigned char *nsec,
                                  const unsigned char *npub,
                                  const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_aead_aes256gcm_decrypt(unsigned char *m,
                                  unsigned long long *mlen_p,
                                  unsigned char *nsec,
                                  const unsigned char *c,
                                  unsigned long long clen,
                                  const unsigned char *ad,
                                  unsigned long long adlen,
                                  const unsigned char *npub,
                                  const unsigned char *k)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_aead_aes256gcm_encrypt_detached(unsigned char *c,
                                           unsigned char *mac,
                                           unsigned long long *maclen_p,
                                           const unsigned char *m,
                                           unsigned long long mlen,
                                           const unsigned char *ad,
                                           unsigned long long adlen,
                                           const unsigned char *nsec,
                                           const unsigned char *npub,
                                           const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_aead_aes256gcm_decrypt_detached(unsigned char *m,
                                           unsigned char *nsec,
                                           const unsigned char *c,
                                           unsigned long long clen,
                                           const unsigned char *mac,
                                           const unsigned char *ad,
                                           unsigned long long adlen,
                                           const unsigned char *npub,
                                           const unsigned char *k)
        __attribute__ ((warn_unused_result));



__attribute__ ((visibility ("default")))
int crypto_aead_aes256gcm_beforenm(crypto_aead_aes256gcm_state *ctx_,
                                   const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_aead_aes256gcm_encrypt_afternm(unsigned char *c,
                                          unsigned long long *clen_p,
                                          const unsigned char *m,
                                          unsigned long long mlen,
                                          const unsigned char *ad,
                                          unsigned long long adlen,
                                          const unsigned char *nsec,
                                          const unsigned char *npub,
                                          const crypto_aead_aes256gcm_state *ctx_);

__attribute__ ((visibility ("default")))
int crypto_aead_aes256gcm_decrypt_afternm(unsigned char *m,
                                          unsigned long long *mlen_p,
                                          unsigned char *nsec,
                                          const unsigned char *c,
                                          unsigned long long clen,
                                          const unsigned char *ad,
                                          unsigned long long adlen,
                                          const unsigned char *npub,
                                          const crypto_aead_aes256gcm_state *ctx_)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_aead_aes256gcm_encrypt_detached_afternm(unsigned char *c,
                                                   unsigned char *mac,
                                                   unsigned long long *maclen_p,
                                                   const unsigned char *m,
                                                   unsigned long long mlen,
                                                   const unsigned char *ad,
                                                   unsigned long long adlen,
                                                   const unsigned char *nsec,
                                                   const unsigned char *npub,
                                                   const crypto_aead_aes256gcm_state *ctx_);

__attribute__ ((visibility ("default")))
int crypto_aead_aes256gcm_decrypt_detached_afternm(unsigned char *m,
                                                   unsigned char *nsec,
                                                   const unsigned char *c,
                                                   unsigned long long clen,
                                                   const unsigned char *mac,
                                                   const unsigned char *ad,
                                                   unsigned long long adlen,
                                                   const unsigned char *npub,
                                                   const crypto_aead_aes256gcm_state *ctx_)
        __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
void crypto_aead_aes256gcm_keygen(unsigned char k[32U]);
# 9 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_aead_chacha20poly1305.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_aead_chacha20poly1305.h" 2
# 17 "/usr/include/sodium/crypto_aead_chacha20poly1305.h"
__attribute__ ((visibility ("default")))
size_t crypto_aead_chacha20poly1305_ietf_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_aead_chacha20poly1305_ietf_nsecbytes(void);



__attribute__ ((visibility ("default")))
size_t crypto_aead_chacha20poly1305_ietf_npubbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_aead_chacha20poly1305_ietf_abytes(void);




__attribute__ ((visibility ("default")))
size_t crypto_aead_chacha20poly1305_ietf_messagebytes_max(void);

__attribute__ ((visibility ("default")))
int crypto_aead_chacha20poly1305_ietf_encrypt(unsigned char *c,
                                              unsigned long long *clen_p,
                                              const unsigned char *m,
                                              unsigned long long mlen,
                                              const unsigned char *ad,
                                              unsigned long long adlen,
                                              const unsigned char *nsec,
                                              const unsigned char *npub,
                                              const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_aead_chacha20poly1305_ietf_decrypt(unsigned char *m,
                                              unsigned long long *mlen_p,
                                              unsigned char *nsec,
                                              const unsigned char *c,
                                              unsigned long long clen,
                                              const unsigned char *ad,
                                              unsigned long long adlen,
                                              const unsigned char *npub,
                                              const unsigned char *k)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_aead_chacha20poly1305_ietf_encrypt_detached(unsigned char *c,
                                                       unsigned char *mac,
                                                       unsigned long long *maclen_p,
                                                       const unsigned char *m,
                                                       unsigned long long mlen,
                                                       const unsigned char *ad,
                                                       unsigned long long adlen,
                                                       const unsigned char *nsec,
                                                       const unsigned char *npub,
                                                       const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_aead_chacha20poly1305_ietf_decrypt_detached(unsigned char *m,
                                                       unsigned char *nsec,
                                                       const unsigned char *c,
                                                       unsigned long long clen,
                                                       const unsigned char *mac,
                                                       const unsigned char *ad,
                                                       unsigned long long adlen,
                                                       const unsigned char *npub,
                                                       const unsigned char *k)
        __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
void crypto_aead_chacha20poly1305_ietf_keygen(unsigned char k[32U]);




__attribute__ ((visibility ("default")))
size_t crypto_aead_chacha20poly1305_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_aead_chacha20poly1305_nsecbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_aead_chacha20poly1305_npubbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_aead_chacha20poly1305_abytes(void);



__attribute__ ((visibility ("default")))
size_t crypto_aead_chacha20poly1305_messagebytes_max(void);

__attribute__ ((visibility ("default")))
int crypto_aead_chacha20poly1305_encrypt(unsigned char *c,
                                         unsigned long long *clen_p,
                                         const unsigned char *m,
                                         unsigned long long mlen,
                                         const unsigned char *ad,
                                         unsigned long long adlen,
                                         const unsigned char *nsec,
                                         const unsigned char *npub,
                                         const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_aead_chacha20poly1305_decrypt(unsigned char *m,
                                         unsigned long long *mlen_p,
                                         unsigned char *nsec,
                                         const unsigned char *c,
                                         unsigned long long clen,
                                         const unsigned char *ad,
                                         unsigned long long adlen,
                                         const unsigned char *npub,
                                         const unsigned char *k)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_aead_chacha20poly1305_encrypt_detached(unsigned char *c,
                                                  unsigned char *mac,
                                                  unsigned long long *maclen_p,
                                                  const unsigned char *m,
                                                  unsigned long long mlen,
                                                  const unsigned char *ad,
                                                  unsigned long long adlen,
                                                  const unsigned char *nsec,
                                                  const unsigned char *npub,
                                                  const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_aead_chacha20poly1305_decrypt_detached(unsigned char *m,
                                                  unsigned char *nsec,
                                                  const unsigned char *c,
                                                  unsigned long long clen,
                                                  const unsigned char *mac,
                                                  const unsigned char *ad,
                                                  unsigned long long adlen,
                                                  const unsigned char *npub,
                                                  const unsigned char *k)
        __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
void crypto_aead_chacha20poly1305_keygen(unsigned char k[32U]);
# 10 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_aead_xchacha20poly1305.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_aead_xchacha20poly1305.h" 2
# 15 "/usr/include/sodium/crypto_aead_xchacha20poly1305.h"
__attribute__ ((visibility ("default")))
size_t crypto_aead_xchacha20poly1305_ietf_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_aead_xchacha20poly1305_ietf_nsecbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_aead_xchacha20poly1305_ietf_npubbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_aead_xchacha20poly1305_ietf_abytes(void);



__attribute__ ((visibility ("default")))
size_t crypto_aead_xchacha20poly1305_ietf_messagebytes_max(void);

__attribute__ ((visibility ("default")))
int crypto_aead_xchacha20poly1305_ietf_encrypt(unsigned char *c,
                                               unsigned long long *clen_p,
                                               const unsigned char *m,
                                               unsigned long long mlen,
                                               const unsigned char *ad,
                                               unsigned long long adlen,
                                               const unsigned char *nsec,
                                               const unsigned char *npub,
                                               const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_aead_xchacha20poly1305_ietf_decrypt(unsigned char *m,
                                               unsigned long long *mlen_p,
                                               unsigned char *nsec,
                                               const unsigned char *c,
                                               unsigned long long clen,
                                               const unsigned char *ad,
                                               unsigned long long adlen,
                                               const unsigned char *npub,
                                               const unsigned char *k)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_aead_xchacha20poly1305_ietf_encrypt_detached(unsigned char *c,
                                                        unsigned char *mac,
                                                        unsigned long long *maclen_p,
                                                        const unsigned char *m,
                                                        unsigned long long mlen,
                                                        const unsigned char *ad,
                                                        unsigned long long adlen,
                                                        const unsigned char *nsec,
                                                        const unsigned char *npub,
                                                        const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_aead_xchacha20poly1305_ietf_decrypt_detached(unsigned char *m,
                                                        unsigned char *nsec,
                                                        const unsigned char *c,
                                                        unsigned long long clen,
                                                        const unsigned char *mac,
                                                        const unsigned char *ad,
                                                        unsigned long long adlen,
                                                        const unsigned char *npub,
                                                        const unsigned char *k)
        __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
void crypto_aead_xchacha20poly1305_ietf_keygen(unsigned char k[32U]);
# 11 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_auth.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_auth.h" 2

# 1 "/usr/include/sodium/crypto_auth_hmacsha512256.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_auth_hmacsha512256.h" 2
# 1 "/usr/include/sodium/crypto_auth_hmacsha512.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_auth_hmacsha512.h" 2
# 1 "/usr/include/sodium/crypto_hash_sha512.h" 1
# 11 "/usr/include/sodium/crypto_hash_sha512.h"
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 12 "/usr/include/sodium/crypto_hash_sha512.h" 2
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stdint.h" 1 3 4
# 9 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stdint.h" 3 4
# 1 "/usr/include/stdint.h" 1 3 4
# 26 "/usr/include/stdint.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/libc-header-start.h" 1 3 4
# 33 "/usr/include/x86_64-linux-gnu/bits/libc-header-start.h" 3 4
# 1 "/usr/include/features.h" 1 3 4
# 424 "/usr/include/features.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/sys/cdefs.h" 1 3 4
# 427 "/usr/include/x86_64-linux-gnu/sys/cdefs.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/wordsize.h" 1 3 4
# 428 "/usr/include/x86_64-linux-gnu/sys/cdefs.h" 2 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/long-double.h" 1 3 4
# 429 "/usr/include/x86_64-linux-gnu/sys/cdefs.h" 2 3 4
# 425 "/usr/include/features.h" 2 3 4
# 448 "/usr/include/features.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/gnu/stubs.h" 1 3 4
# 10 "/usr/include/x86_64-linux-gnu/gnu/stubs.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/gnu/stubs-64.h" 1 3 4
# 11 "/usr/include/x86_64-linux-gnu/gnu/stubs.h" 2 3 4
# 449 "/usr/include/features.h" 2 3 4
# 34 "/usr/include/x86_64-linux-gnu/bits/libc-header-start.h" 2 3 4
# 27 "/usr/include/stdint.h" 2 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/types.h" 1 3 4
# 27 "/usr/include/x86_64-linux-gnu/bits/types.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/wordsize.h" 1 3 4
# 28 "/usr/include/x86_64-linux-gnu/bits/types.h" 2 3 4



# 30 "/usr/include/x86_64-linux-gnu/bits/types.h" 3 4
typedef unsigned char __u_char;
typedef unsigned short int __u_short;
typedef unsigned int __u_int;
typedef unsigned long int __u_long;


typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef signed short int __int16_t;
typedef unsigned short int __uint16_t;
typedef signed int __int32_t;
typedef unsigned int __uint32_t;

typedef signed long int __int64_t;
typedef unsigned long int __uint64_t;







typedef long int __quad_t;
typedef unsigned long int __u_quad_t;







typedef long int __intmax_t;
typedef unsigned long int __uintmax_t;
# 130 "/usr/include/x86_64-linux-gnu/bits/types.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/typesizes.h" 1 3 4
# 131 "/usr/include/x86_64-linux-gnu/bits/types.h" 2 3 4


typedef unsigned long int __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long int __ino_t;
typedef unsigned long int __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long int __nlink_t;
typedef long int __off_t;
typedef long int __off64_t;
typedef int __pid_t;
typedef struct { int __val[2]; } __fsid_t;
typedef long int __clock_t;
typedef unsigned long int __rlim_t;
typedef unsigned long int __rlim64_t;
typedef unsigned int __id_t;
typedef long int __time_t;
typedef unsigned int __useconds_t;
typedef long int __suseconds_t;

typedef int __daddr_t;
typedef int __key_t;


typedef int __clockid_t;


typedef void * __timer_t;


typedef long int __blksize_t;




typedef long int __blkcnt_t;
typedef long int __blkcnt64_t;


typedef unsigned long int __fsblkcnt_t;
typedef unsigned long int __fsblkcnt64_t;


typedef unsigned long int __fsfilcnt_t;
typedef unsigned long int __fsfilcnt64_t;


typedef long int __fsword_t;

typedef long int __ssize_t;


typedef long int __syscall_slong_t;

typedef unsigned long int __syscall_ulong_t;



typedef __off64_t __loff_t;
typedef char *__caddr_t;


typedef long int __intptr_t;


typedef unsigned int __socklen_t;




typedef int __sig_atomic_t;
# 28 "/usr/include/stdint.h" 2 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/wchar.h" 1 3 4
# 29 "/usr/include/stdint.h" 2 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/wordsize.h" 1 3 4
# 30 "/usr/include/stdint.h" 2 3 4




# 1 "/usr/include/x86_64-linux-gnu/bits/stdint-intn.h" 1 3 4
# 24 "/usr/include/x86_64-linux-gnu/bits/stdint-intn.h" 3 4
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
# 35 "/usr/include/stdint.h" 2 3 4


# 1 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h" 1 3 4
# 24 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h" 3 4
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
# 38 "/usr/include/stdint.h" 2 3 4





typedef signed char int_least8_t;
typedef short int int_least16_t;
typedef int int_least32_t;

typedef long int int_least64_t;






typedef unsigned char uint_least8_t;
typedef unsigned short int uint_least16_t;
typedef unsigned int uint_least32_t;

typedef unsigned long int uint_least64_t;
# 68 "/usr/include/stdint.h" 3 4
typedef signed char int_fast8_t;

typedef long int int_fast16_t;
typedef long int int_fast32_t;
typedef long int int_fast64_t;
# 81 "/usr/include/stdint.h" 3 4
typedef unsigned char uint_fast8_t;

typedef unsigned long int uint_fast16_t;
typedef unsigned long int uint_fast32_t;
typedef unsigned long int uint_fast64_t;
# 97 "/usr/include/stdint.h" 3 4
typedef long int intptr_t;


typedef unsigned long int uintptr_t;
# 111 "/usr/include/stdint.h" 3 4
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
# 10 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stdint.h" 2 3 4
# 13 "/usr/include/sodium/crypto_hash_sha512.h" 2
# 1 "/usr/include/stdlib.h" 1 3 4
# 25 "/usr/include/stdlib.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/libc-header-start.h" 1 3 4
# 26 "/usr/include/stdlib.h" 2 3 4





# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 32 "/usr/include/stdlib.h" 2 3 4







# 1 "/usr/include/x86_64-linux-gnu/bits/waitflags.h" 1 3 4
# 52 "/usr/include/x86_64-linux-gnu/bits/waitflags.h" 3 4
typedef enum
{
  P_ALL,
  P_PID,
  P_PGID
} idtype_t;
# 40 "/usr/include/stdlib.h" 2 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/waitstatus.h" 1 3 4
# 41 "/usr/include/stdlib.h" 2 3 4
# 55 "/usr/include/stdlib.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/floatn.h" 1 3 4
# 120 "/usr/include/x86_64-linux-gnu/bits/floatn.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/floatn-common.h" 1 3 4
# 24 "/usr/include/x86_64-linux-gnu/bits/floatn-common.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/long-double.h" 1 3 4
# 25 "/usr/include/x86_64-linux-gnu/bits/floatn-common.h" 2 3 4
# 121 "/usr/include/x86_64-linux-gnu/bits/floatn.h" 2 3 4
# 56 "/usr/include/stdlib.h" 2 3 4


typedef struct
  {
    int quot;
    int rem;
  } div_t;



typedef struct
  {
    long int quot;
    long int rem;
  } ldiv_t;





__extension__ typedef struct
  {
    long long int quot;
    long long int rem;
  } lldiv_t;
# 97 "/usr/include/stdlib.h" 3 4
extern size_t __ctype_get_mb_cur_max (void) __attribute__ ((__nothrow__ , __leaf__)) ;



extern double atof (const char *__nptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1))) ;

extern int atoi (const char *__nptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1))) ;

extern long int atol (const char *__nptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1))) ;



__extension__ extern long long int atoll (const char *__nptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1))) ;



extern double strtod (const char *__restrict __nptr,
        char **__restrict __endptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));



extern float strtof (const char *__restrict __nptr,
       char **__restrict __endptr) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));

extern long double strtold (const char *__restrict __nptr,
       char **__restrict __endptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
# 176 "/usr/include/stdlib.h" 3 4
extern long int strtol (const char *__restrict __nptr,
   char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));

extern unsigned long int strtoul (const char *__restrict __nptr,
      char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));



__extension__
extern long long int strtoq (const char *__restrict __nptr,
        char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));

__extension__
extern unsigned long long int strtouq (const char *__restrict __nptr,
           char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));




__extension__
extern long long int strtoll (const char *__restrict __nptr,
         char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));

__extension__
extern unsigned long long int strtoull (const char *__restrict __nptr,
     char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
# 385 "/usr/include/stdlib.h" 3 4
extern char *l64a (long int __n) __attribute__ ((__nothrow__ , __leaf__)) ;


extern long int a64l (const char *__s)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1))) ;




# 1 "/usr/include/x86_64-linux-gnu/sys/types.h" 1 3 4
# 27 "/usr/include/x86_64-linux-gnu/sys/types.h" 3 4






typedef __u_char u_char;
typedef __u_short u_short;
typedef __u_int u_int;
typedef __u_long u_long;
typedef __quad_t quad_t;
typedef __u_quad_t u_quad_t;
typedef __fsid_t fsid_t;




typedef __loff_t loff_t;



typedef __ino_t ino_t;
# 60 "/usr/include/x86_64-linux-gnu/sys/types.h" 3 4
typedef __dev_t dev_t;




typedef __gid_t gid_t;




typedef __mode_t mode_t;




typedef __nlink_t nlink_t;




typedef __uid_t uid_t;





typedef __off_t off_t;
# 98 "/usr/include/x86_64-linux-gnu/sys/types.h" 3 4
typedef __pid_t pid_t;





typedef __id_t id_t;




typedef __ssize_t ssize_t;





typedef __daddr_t daddr_t;
typedef __caddr_t caddr_t;





typedef __key_t key_t;




# 1 "/usr/include/x86_64-linux-gnu/bits/types/clock_t.h" 1 3 4






typedef __clock_t clock_t;
# 128 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4

# 1 "/usr/include/x86_64-linux-gnu/bits/types/clockid_t.h" 1 3 4






typedef __clockid_t clockid_t;
# 130 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/types/time_t.h" 1 3 4






typedef __time_t time_t;
# 131 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/types/timer_t.h" 1 3 4






typedef __timer_t timer_t;
# 132 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4
# 145 "/usr/include/x86_64-linux-gnu/sys/types.h" 3 4
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 146 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4



typedef unsigned long int ulong;
typedef unsigned short int ushort;
typedef unsigned int uint;
# 178 "/usr/include/x86_64-linux-gnu/sys/types.h" 3 4
typedef unsigned int u_int8_t __attribute__ ((__mode__ (__QI__)));
typedef unsigned int u_int16_t __attribute__ ((__mode__ (__HI__)));
typedef unsigned int u_int32_t __attribute__ ((__mode__ (__SI__)));
typedef unsigned int u_int64_t __attribute__ ((__mode__ (__DI__)));

typedef int register_t __attribute__ ((__mode__ (__word__)));
# 194 "/usr/include/x86_64-linux-gnu/sys/types.h" 3 4
# 1 "/usr/include/endian.h" 1 3 4
# 36 "/usr/include/endian.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/endian.h" 1 3 4
# 37 "/usr/include/endian.h" 2 3 4
# 60 "/usr/include/endian.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/byteswap.h" 1 3 4
# 28 "/usr/include/x86_64-linux-gnu/bits/byteswap.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/wordsize.h" 1 3 4
# 29 "/usr/include/x86_64-linux-gnu/bits/byteswap.h" 2 3 4






# 1 "/usr/include/x86_64-linux-gnu/bits/byteswap-16.h" 1 3 4
# 36 "/usr/include/x86_64-linux-gnu/bits/byteswap.h" 2 3 4
# 44 "/usr/include/x86_64-linux-gnu/bits/byteswap.h" 3 4
static __inline unsigned int
__bswap_32 (unsigned int __bsx)
{
  return __builtin_bswap32 (__bsx);
}
# 108 "/usr/include/x86_64-linux-gnu/bits/byteswap.h" 3 4
static __inline __uint64_t
__bswap_64 (__uint64_t __bsx)
{
  return __builtin_bswap64 (__bsx);
}
# 61 "/usr/include/endian.h" 2 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/uintn-identity.h" 1 3 4
# 32 "/usr/include/x86_64-linux-gnu/bits/uintn-identity.h" 3 4
static __inline __uint16_t
__uint16_identity (__uint16_t __x)
{
  return __x;
}

static __inline __uint32_t
__uint32_identity (__uint32_t __x)
{
  return __x;
}

static __inline __uint64_t
__uint64_identity (__uint64_t __x)
{
  return __x;
}
# 62 "/usr/include/endian.h" 2 3 4
# 195 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4


# 1 "/usr/include/x86_64-linux-gnu/sys/select.h" 1 3 4
# 30 "/usr/include/x86_64-linux-gnu/sys/select.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/select.h" 1 3 4
# 22 "/usr/include/x86_64-linux-gnu/bits/select.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/wordsize.h" 1 3 4
# 23 "/usr/include/x86_64-linux-gnu/bits/select.h" 2 3 4
# 31 "/usr/include/x86_64-linux-gnu/sys/select.h" 2 3 4


# 1 "/usr/include/x86_64-linux-gnu/bits/types/sigset_t.h" 1 3 4



# 1 "/usr/include/x86_64-linux-gnu/bits/types/__sigset_t.h" 1 3 4




typedef struct
{
  unsigned long int __val[(1024 / (8 * sizeof (unsigned long int)))];
} __sigset_t;
# 5 "/usr/include/x86_64-linux-gnu/bits/types/sigset_t.h" 2 3 4


typedef __sigset_t sigset_t;
# 34 "/usr/include/x86_64-linux-gnu/sys/select.h" 2 3 4



# 1 "/usr/include/x86_64-linux-gnu/bits/types/struct_timeval.h" 1 3 4







struct timeval
{
  __time_t tv_sec;
  __suseconds_t tv_usec;
};
# 38 "/usr/include/x86_64-linux-gnu/sys/select.h" 2 3 4

# 1 "/usr/include/x86_64-linux-gnu/bits/types/struct_timespec.h" 1 3 4
# 9 "/usr/include/x86_64-linux-gnu/bits/types/struct_timespec.h" 3 4
struct timespec
{
  __time_t tv_sec;
  __syscall_slong_t tv_nsec;
};
# 40 "/usr/include/x86_64-linux-gnu/sys/select.h" 2 3 4



typedef __suseconds_t suseconds_t;





typedef long int __fd_mask;
# 59 "/usr/include/x86_64-linux-gnu/sys/select.h" 3 4
typedef struct
  {






    __fd_mask __fds_bits[1024 / (8 * (int) sizeof (__fd_mask))];


  } fd_set;






typedef __fd_mask fd_mask;
# 91 "/usr/include/x86_64-linux-gnu/sys/select.h" 3 4

# 101 "/usr/include/x86_64-linux-gnu/sys/select.h" 3 4
extern int select (int __nfds, fd_set *__restrict __readfds,
     fd_set *__restrict __writefds,
     fd_set *__restrict __exceptfds,
     struct timeval *__restrict __timeout);
# 113 "/usr/include/x86_64-linux-gnu/sys/select.h" 3 4
extern int pselect (int __nfds, fd_set *__restrict __readfds,
      fd_set *__restrict __writefds,
      fd_set *__restrict __exceptfds,
      const struct timespec *__restrict __timeout,
      const __sigset_t *__restrict __sigmask);
# 126 "/usr/include/x86_64-linux-gnu/sys/select.h" 3 4

# 198 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4







# 1 "/usr/include/x86_64-linux-gnu/sys/sysmacros.h" 1 3 4
# 41 "/usr/include/x86_64-linux-gnu/sys/sysmacros.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/sysmacros.h" 1 3 4
# 42 "/usr/include/x86_64-linux-gnu/sys/sysmacros.h" 2 3 4
# 71 "/usr/include/x86_64-linux-gnu/sys/sysmacros.h" 3 4


extern unsigned int gnu_dev_major (__dev_t __dev) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__));
extern unsigned int gnu_dev_minor (__dev_t __dev) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__));
extern __dev_t gnu_dev_makedev (unsigned int __major, unsigned int __minor) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__));
# 85 "/usr/include/x86_64-linux-gnu/sys/sysmacros.h" 3 4

# 206 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4






typedef __blksize_t blksize_t;






typedef __blkcnt_t blkcnt_t;



typedef __fsblkcnt_t fsblkcnt_t;



typedef __fsfilcnt_t fsfilcnt_t;
# 254 "/usr/include/x86_64-linux-gnu/sys/types.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes.h" 1 3 4
# 23 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/thread-shared-types.h" 1 3 4
# 77 "/usr/include/x86_64-linux-gnu/bits/thread-shared-types.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes-arch.h" 1 3 4
# 21 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes-arch.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/wordsize.h" 1 3 4
# 22 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes-arch.h" 2 3 4
# 65 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes-arch.h" 3 4
struct __pthread_rwlock_arch_t
{
  unsigned int __readers;
  unsigned int __writers;
  unsigned int __wrphase_futex;
  unsigned int __writers_futex;
  unsigned int __pad3;
  unsigned int __pad4;

  int __cur_writer;
  int __shared;
  signed char __rwelision;




  unsigned char __pad1[7];


  unsigned long int __pad2;


  unsigned int __flags;
# 99 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes-arch.h" 3 4
};
# 78 "/usr/include/x86_64-linux-gnu/bits/thread-shared-types.h" 2 3 4




typedef struct __pthread_internal_list
{
  struct __pthread_internal_list *__prev;
  struct __pthread_internal_list *__next;
} __pthread_list_t;
# 118 "/usr/include/x86_64-linux-gnu/bits/thread-shared-types.h" 3 4
struct __pthread_mutex_s
{
  int __lock ;
  unsigned int __count;
  int __owner;

  unsigned int __nusers;
# 148 "/usr/include/x86_64-linux-gnu/bits/thread-shared-types.h" 3 4
  int __kind;
 




  short __spins; short __elision;
  __pthread_list_t __list;
# 165 "/usr/include/x86_64-linux-gnu/bits/thread-shared-types.h" 3 4
 
};




struct __pthread_cond_s
{
  __extension__ union
  {
    __extension__ unsigned long long int __wseq;
    struct
    {
      unsigned int __low;
      unsigned int __high;
    } __wseq32;
  };
  __extension__ union
  {
    __extension__ unsigned long long int __g1_start;
    struct
    {
      unsigned int __low;
      unsigned int __high;
    } __g1_start32;
  };
  unsigned int __g_refs[2] ;
  unsigned int __g_size[2];
  unsigned int __g1_orig_size;
  unsigned int __wrefs;
  unsigned int __g_signals[2];
};
# 24 "/usr/include/x86_64-linux-gnu/bits/pthreadtypes.h" 2 3 4



typedef unsigned long int pthread_t;




typedef union
{
  char __size[4];
  int __align;
} pthread_mutexattr_t;




typedef union
{
  char __size[4];
  int __align;
} pthread_condattr_t;



typedef unsigned int pthread_key_t;



typedef int pthread_once_t;


union pthread_attr_t
{
  char __size[56];
  long int __align;
};

typedef union pthread_attr_t pthread_attr_t;




typedef union
{
  struct __pthread_mutex_s __data;
  char __size[40];
  long int __align;
} pthread_mutex_t;


typedef union
{
  struct __pthread_cond_s __data;
  char __size[48];
  __extension__ long long int __align;
} pthread_cond_t;





typedef union
{
  struct __pthread_rwlock_arch_t __data;
  char __size[56];
  long int __align;
} pthread_rwlock_t;

typedef union
{
  char __size[8];
  long int __align;
} pthread_rwlockattr_t;





typedef volatile int pthread_spinlock_t;




typedef union
{
  char __size[32];
  long int __align;
} pthread_barrier_t;

typedef union
{
  char __size[4];
  int __align;
} pthread_barrierattr_t;
# 255 "/usr/include/x86_64-linux-gnu/sys/types.h" 2 3 4



# 395 "/usr/include/stdlib.h" 2 3 4






extern long int random (void) __attribute__ ((__nothrow__ , __leaf__));


extern void srandom (unsigned int __seed) __attribute__ ((__nothrow__ , __leaf__));





extern char *initstate (unsigned int __seed, char *__statebuf,
   size_t __statelen) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));



extern char *setstate (char *__statebuf) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));







struct random_data
  {
    int32_t *fptr;
    int32_t *rptr;
    int32_t *state;
    int rand_type;
    int rand_deg;
    int rand_sep;
    int32_t *end_ptr;
  };

extern int random_r (struct random_data *__restrict __buf,
       int32_t *__restrict __result) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));

extern int srandom_r (unsigned int __seed, struct random_data *__buf)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));

extern int initstate_r (unsigned int __seed, char *__restrict __statebuf,
   size_t __statelen,
   struct random_data *__restrict __buf)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2, 4)));

extern int setstate_r (char *__restrict __statebuf,
         struct random_data *__restrict __buf)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));





extern int rand (void) __attribute__ ((__nothrow__ , __leaf__));

extern void srand (unsigned int __seed) __attribute__ ((__nothrow__ , __leaf__));



extern int rand_r (unsigned int *__seed) __attribute__ ((__nothrow__ , __leaf__));







extern double drand48 (void) __attribute__ ((__nothrow__ , __leaf__));
extern double erand48 (unsigned short int __xsubi[3]) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));


extern long int lrand48 (void) __attribute__ ((__nothrow__ , __leaf__));
extern long int nrand48 (unsigned short int __xsubi[3])
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));


extern long int mrand48 (void) __attribute__ ((__nothrow__ , __leaf__));
extern long int jrand48 (unsigned short int __xsubi[3])
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));


extern void srand48 (long int __seedval) __attribute__ ((__nothrow__ , __leaf__));
extern unsigned short int *seed48 (unsigned short int __seed16v[3])
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern void lcong48 (unsigned short int __param[7]) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));





struct drand48_data
  {
    unsigned short int __x[3];
    unsigned short int __old_x[3];
    unsigned short int __c;
    unsigned short int __init;
    __extension__ unsigned long long int __a;

  };


extern int drand48_r (struct drand48_data *__restrict __buffer,
        double *__restrict __result) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int erand48_r (unsigned short int __xsubi[3],
        struct drand48_data *__restrict __buffer,
        double *__restrict __result) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));


extern int lrand48_r (struct drand48_data *__restrict __buffer,
        long int *__restrict __result)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int nrand48_r (unsigned short int __xsubi[3],
        struct drand48_data *__restrict __buffer,
        long int *__restrict __result)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));


extern int mrand48_r (struct drand48_data *__restrict __buffer,
        long int *__restrict __result)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int jrand48_r (unsigned short int __xsubi[3],
        struct drand48_data *__restrict __buffer,
        long int *__restrict __result)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));


extern int srand48_r (long int __seedval, struct drand48_data *__buffer)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));

extern int seed48_r (unsigned short int __seed16v[3],
       struct drand48_data *__buffer) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));

extern int lcong48_r (unsigned short int __param[7],
        struct drand48_data *__buffer)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));




extern void *malloc (size_t __size) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__)) ;

extern void *calloc (size_t __nmemb, size_t __size)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__)) ;






extern void *realloc (void *__ptr, size_t __size)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__warn_unused_result__));
# 563 "/usr/include/stdlib.h" 3 4
extern void free (void *__ptr) __attribute__ ((__nothrow__ , __leaf__));


# 1 "/usr/include/alloca.h" 1 3 4
# 24 "/usr/include/alloca.h" 3 4
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 25 "/usr/include/alloca.h" 2 3 4







extern void *alloca (size_t __size) __attribute__ ((__nothrow__ , __leaf__));






# 567 "/usr/include/stdlib.h" 2 3 4





extern void *valloc (size_t __size) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__)) ;




extern int posix_memalign (void **__memptr, size_t __alignment, size_t __size)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) ;




extern void *aligned_alloc (size_t __alignment, size_t __size)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__)) __attribute__ ((__alloc_size__ (2))) ;



extern void abort (void) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));



extern int atexit (void (*__func) (void)) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));







extern int at_quick_exit (void (*__func) (void)) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));






extern int on_exit (void (*__func) (int __status, void *__arg), void *__arg)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));





extern void exit (int __status) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));





extern void quick_exit (int __status) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));





extern void _Exit (int __status) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));




extern char *getenv (const char *__name) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) ;
# 644 "/usr/include/stdlib.h" 3 4
extern int putenv (char *__string) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));





extern int setenv (const char *__name, const char *__value, int __replace)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));


extern int unsetenv (const char *__name) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));






extern int clearenv (void) __attribute__ ((__nothrow__ , __leaf__));
# 672 "/usr/include/stdlib.h" 3 4
extern char *mktemp (char *__template) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
# 685 "/usr/include/stdlib.h" 3 4
extern int mkstemp (char *__template) __attribute__ ((__nonnull__ (1))) ;
# 707 "/usr/include/stdlib.h" 3 4
extern int mkstemps (char *__template, int __suffixlen) __attribute__ ((__nonnull__ (1))) ;
# 728 "/usr/include/stdlib.h" 3 4
extern char *mkdtemp (char *__template) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) ;
# 781 "/usr/include/stdlib.h" 3 4
extern int system (const char *__command) ;
# 797 "/usr/include/stdlib.h" 3 4
extern char *realpath (const char *__restrict __name,
         char *__restrict __resolved) __attribute__ ((__nothrow__ , __leaf__)) ;






typedef int (*__compar_fn_t) (const void *, const void *);
# 817 "/usr/include/stdlib.h" 3 4
extern void *bsearch (const void *__key, const void *__base,
        size_t __nmemb, size_t __size, __compar_fn_t __compar)
     __attribute__ ((__nonnull__ (1, 2, 5))) ;







extern void qsort (void *__base, size_t __nmemb, size_t __size,
     __compar_fn_t __compar) __attribute__ ((__nonnull__ (1, 4)));
# 837 "/usr/include/stdlib.h" 3 4
extern int abs (int __x) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__)) ;
extern long int labs (long int __x) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__)) ;


__extension__ extern long long int llabs (long long int __x)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__)) ;






extern div_t div (int __numer, int __denom)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__)) ;
extern ldiv_t ldiv (long int __numer, long int __denom)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__)) ;


__extension__ extern lldiv_t lldiv (long long int __numer,
        long long int __denom)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__)) ;
# 869 "/usr/include/stdlib.h" 3 4
extern char *ecvt (double __value, int __ndigit, int *__restrict __decpt,
     int *__restrict __sign) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4))) ;




extern char *fcvt (double __value, int __ndigit, int *__restrict __decpt,
     int *__restrict __sign) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4))) ;




extern char *gcvt (double __value, int __ndigit, char *__buf)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3))) ;




extern char *qecvt (long double __value, int __ndigit,
      int *__restrict __decpt, int *__restrict __sign)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4))) ;
extern char *qfcvt (long double __value, int __ndigit,
      int *__restrict __decpt, int *__restrict __sign)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4))) ;
extern char *qgcvt (long double __value, int __ndigit, char *__buf)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3))) ;




extern int ecvt_r (double __value, int __ndigit, int *__restrict __decpt,
     int *__restrict __sign, char *__restrict __buf,
     size_t __len) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4, 5)));
extern int fcvt_r (double __value, int __ndigit, int *__restrict __decpt,
     int *__restrict __sign, char *__restrict __buf,
     size_t __len) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4, 5)));

extern int qecvt_r (long double __value, int __ndigit,
      int *__restrict __decpt, int *__restrict __sign,
      char *__restrict __buf, size_t __len)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4, 5)));
extern int qfcvt_r (long double __value, int __ndigit,
      int *__restrict __decpt, int *__restrict __sign,
      char *__restrict __buf, size_t __len)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4, 5)));





extern int mblen (const char *__s, size_t __n) __attribute__ ((__nothrow__ , __leaf__));


extern int mbtowc (wchar_t *__restrict __pwc,
     const char *__restrict __s, size_t __n) __attribute__ ((__nothrow__ , __leaf__));


extern int wctomb (char *__s, wchar_t __wchar) __attribute__ ((__nothrow__ , __leaf__));



extern size_t mbstowcs (wchar_t *__restrict __pwcs,
   const char *__restrict __s, size_t __n) __attribute__ ((__nothrow__ , __leaf__));

extern size_t wcstombs (char *__restrict __s,
   const wchar_t *__restrict __pwcs, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__));







extern int rpmatch (const char *__response) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) ;
# 954 "/usr/include/stdlib.h" 3 4
extern int getsubopt (char **__restrict __optionp,
        char *const *__restrict __tokens,
        char **__restrict __valuep)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2, 3))) ;
# 1006 "/usr/include/stdlib.h" 3 4
extern int getloadavg (double __loadavg[], int __nelem)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
# 1016 "/usr/include/stdlib.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/stdlib-float.h" 1 3 4
# 1017 "/usr/include/stdlib.h" 2 3 4
# 1026 "/usr/include/stdlib.h" 3 4

# 14 "/usr/include/sodium/crypto_hash_sha512.h" 2
# 24 "/usr/include/sodium/crypto_hash_sha512.h"

# 24 "/usr/include/sodium/crypto_hash_sha512.h"
typedef struct crypto_hash_sha512_state {
    uint64_t state[8];
    uint64_t count[2];
    uint8_t buf[128];
} crypto_hash_sha512_state;

__attribute__ ((visibility ("default")))
size_t crypto_hash_sha512_statebytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_hash_sha512_bytes(void);

__attribute__ ((visibility ("default")))
int crypto_hash_sha512(unsigned char *out, const unsigned char *in,
                       unsigned long long inlen);

__attribute__ ((visibility ("default")))
int crypto_hash_sha512_init(crypto_hash_sha512_state *state);

__attribute__ ((visibility ("default")))
int crypto_hash_sha512_update(crypto_hash_sha512_state *state,
                              const unsigned char *in,
                              unsigned long long inlen);

__attribute__ ((visibility ("default")))
int crypto_hash_sha512_final(crypto_hash_sha512_state *state,
                             unsigned char *out);
# 6 "/usr/include/sodium/crypto_auth_hmacsha512.h" 2
# 16 "/usr/include/sodium/crypto_auth_hmacsha512.h"
__attribute__ ((visibility ("default")))
size_t crypto_auth_hmacsha512_bytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_auth_hmacsha512_keybytes(void);

__attribute__ ((visibility ("default")))
int crypto_auth_hmacsha512(unsigned char *out,
                           const unsigned char *in,
                           unsigned long long inlen,
                           const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_auth_hmacsha512_verify(const unsigned char *h,
                                  const unsigned char *in,
                                  unsigned long long inlen,
                                  const unsigned char *k)
            __attribute__ ((warn_unused_result));



typedef struct crypto_auth_hmacsha512_state {
    crypto_hash_sha512_state ictx;
    crypto_hash_sha512_state octx;
} crypto_auth_hmacsha512_state;

__attribute__ ((visibility ("default")))
size_t crypto_auth_hmacsha512_statebytes(void);

__attribute__ ((visibility ("default")))
int crypto_auth_hmacsha512_init(crypto_auth_hmacsha512_state *state,
                                const unsigned char *key,
                                size_t keylen);

__attribute__ ((visibility ("default")))
int crypto_auth_hmacsha512_update(crypto_auth_hmacsha512_state *state,
                                  const unsigned char *in,
                                  unsigned long long inlen);

__attribute__ ((visibility ("default")))
int crypto_auth_hmacsha512_final(crypto_auth_hmacsha512_state *state,
                                 unsigned char *out);

__attribute__ ((visibility ("default")))
void crypto_auth_hmacsha512_keygen(unsigned char k[32U]);
# 6 "/usr/include/sodium/crypto_auth_hmacsha512256.h" 2
# 16 "/usr/include/sodium/crypto_auth_hmacsha512256.h"
__attribute__ ((visibility ("default")))
size_t crypto_auth_hmacsha512256_bytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_auth_hmacsha512256_keybytes(void);

__attribute__ ((visibility ("default")))
int crypto_auth_hmacsha512256(unsigned char *out, const unsigned char *in,
                              unsigned long long inlen,const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_auth_hmacsha512256_verify(const unsigned char *h,
                                     const unsigned char *in,
                                     unsigned long long inlen,
                                     const unsigned char *k)
            __attribute__ ((warn_unused_result));



typedef crypto_auth_hmacsha512_state crypto_auth_hmacsha512256_state;

__attribute__ ((visibility ("default")))
size_t crypto_auth_hmacsha512256_statebytes(void);

__attribute__ ((visibility ("default")))
int crypto_auth_hmacsha512256_init(crypto_auth_hmacsha512256_state *state,
                                   const unsigned char *key,
                                   size_t keylen);

__attribute__ ((visibility ("default")))
int crypto_auth_hmacsha512256_update(crypto_auth_hmacsha512256_state *state,
                                     const unsigned char *in,
                                     unsigned long long inlen);

__attribute__ ((visibility ("default")))
int crypto_auth_hmacsha512256_final(crypto_auth_hmacsha512256_state *state,
                                    unsigned char *out);

__attribute__ ((visibility ("default")))
void crypto_auth_hmacsha512256_keygen(unsigned char k[32U]);
# 7 "/usr/include/sodium/crypto_auth.h" 2
# 17 "/usr/include/sodium/crypto_auth.h"
__attribute__ ((visibility ("default")))
size_t crypto_auth_bytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_auth_keybytes(void);


__attribute__ ((visibility ("default")))
const char *crypto_auth_primitive(void);

__attribute__ ((visibility ("default")))
int crypto_auth(unsigned char *out, const unsigned char *in,
                unsigned long long inlen, const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_auth_verify(const unsigned char *h, const unsigned char *in,
                       unsigned long long inlen, const unsigned char *k)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
void crypto_auth_keygen(unsigned char k[32U]);
# 12 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_auth_hmacsha256.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_auth_hmacsha256.h" 2
# 1 "/usr/include/sodium/crypto_hash_sha256.h" 1
# 11 "/usr/include/sodium/crypto_hash_sha256.h"
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 12 "/usr/include/sodium/crypto_hash_sha256.h" 2
# 24 "/usr/include/sodium/crypto_hash_sha256.h"
typedef struct crypto_hash_sha256_state {
    uint32_t state[8];
    uint64_t count;
    uint8_t buf[64];
} crypto_hash_sha256_state;

__attribute__ ((visibility ("default")))
size_t crypto_hash_sha256_statebytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_hash_sha256_bytes(void);

__attribute__ ((visibility ("default")))
int crypto_hash_sha256(unsigned char *out, const unsigned char *in,
                       unsigned long long inlen);

__attribute__ ((visibility ("default")))
int crypto_hash_sha256_init(crypto_hash_sha256_state *state);

__attribute__ ((visibility ("default")))
int crypto_hash_sha256_update(crypto_hash_sha256_state *state,
                              const unsigned char *in,
                              unsigned long long inlen);

__attribute__ ((visibility ("default")))
int crypto_hash_sha256_final(crypto_hash_sha256_state *state,
                             unsigned char *out);
# 6 "/usr/include/sodium/crypto_auth_hmacsha256.h" 2
# 16 "/usr/include/sodium/crypto_auth_hmacsha256.h"
__attribute__ ((visibility ("default")))
size_t crypto_auth_hmacsha256_bytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_auth_hmacsha256_keybytes(void);

__attribute__ ((visibility ("default")))
int crypto_auth_hmacsha256(unsigned char *out,
                           const unsigned char *in,
                           unsigned long long inlen,
                           const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_auth_hmacsha256_verify(const unsigned char *h,
                                  const unsigned char *in,
                                  unsigned long long inlen,
                                  const unsigned char *k)
            __attribute__ ((warn_unused_result));



typedef struct crypto_auth_hmacsha256_state {
    crypto_hash_sha256_state ictx;
    crypto_hash_sha256_state octx;
} crypto_auth_hmacsha256_state;

__attribute__ ((visibility ("default")))
size_t crypto_auth_hmacsha256_statebytes(void);

__attribute__ ((visibility ("default")))
int crypto_auth_hmacsha256_init(crypto_auth_hmacsha256_state *state,
                                const unsigned char *key,
                                size_t keylen);

__attribute__ ((visibility ("default")))
int crypto_auth_hmacsha256_update(crypto_auth_hmacsha256_state *state,
                                  const unsigned char *in,
                                  unsigned long long inlen);

__attribute__ ((visibility ("default")))
int crypto_auth_hmacsha256_final(crypto_auth_hmacsha256_state *state,
                                 unsigned char *out);


__attribute__ ((visibility ("default")))
void crypto_auth_hmacsha256_keygen(unsigned char k[32U]);
# 13 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_auth_hmacsha512.h" 1
# 14 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_auth_hmacsha512256.h" 1
# 15 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_box.h" 1
# 11 "/usr/include/sodium/crypto_box.h"
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 12 "/usr/include/sodium/crypto_box.h" 2

# 1 "/usr/include/sodium/crypto_box_curve25519xsalsa20poly1305.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_box_curve25519xsalsa20poly1305.h" 2
# 1 "/usr/include/sodium/crypto_stream_xsalsa20.h" 1
# 12 "/usr/include/sodium/crypto_stream_xsalsa20.h"
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 13 "/usr/include/sodium/crypto_stream_xsalsa20.h" 2
# 24 "/usr/include/sodium/crypto_stream_xsalsa20.h"
__attribute__ ((visibility ("default")))
size_t crypto_stream_xsalsa20_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_stream_xsalsa20_noncebytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_stream_xsalsa20_messagebytes_max(void);

__attribute__ ((visibility ("default")))
int crypto_stream_xsalsa20(unsigned char *c, unsigned long long clen,
                           const unsigned char *n, const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_stream_xsalsa20_xor(unsigned char *c, const unsigned char *m,
                               unsigned long long mlen, const unsigned char *n,
                               const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_stream_xsalsa20_xor_ic(unsigned char *c, const unsigned char *m,
                                  unsigned long long mlen,
                                  const unsigned char *n, uint64_t ic,
                                  const unsigned char *k);

__attribute__ ((visibility ("default")))
void crypto_stream_xsalsa20_keygen(unsigned char k[32U]);
# 6 "/usr/include/sodium/crypto_box_curve25519xsalsa20poly1305.h" 2
# 16 "/usr/include/sodium/crypto_box_curve25519xsalsa20poly1305.h"
__attribute__ ((visibility ("default")))
size_t crypto_box_curve25519xsalsa20poly1305_seedbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_box_curve25519xsalsa20poly1305_publickeybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_box_curve25519xsalsa20poly1305_secretkeybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_box_curve25519xsalsa20poly1305_beforenmbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_box_curve25519xsalsa20poly1305_noncebytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_box_curve25519xsalsa20poly1305_macbytes(void);




__attribute__ ((visibility ("default")))
size_t crypto_box_curve25519xsalsa20poly1305_messagebytes_max(void);

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xsalsa20poly1305_seed_keypair(unsigned char *pk,
                                                       unsigned char *sk,
                                                       const unsigned char *seed);

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xsalsa20poly1305_keypair(unsigned char *pk,
                                                  unsigned char *sk);

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xsalsa20poly1305_beforenm(unsigned char *k,
                                                   const unsigned char *pk,
                                                   const unsigned char *sk)
            __attribute__ ((warn_unused_result));




__attribute__ ((visibility ("default")))
size_t crypto_box_curve25519xsalsa20poly1305_boxzerobytes(void);




__attribute__ ((visibility ("default")))
size_t crypto_box_curve25519xsalsa20poly1305_zerobytes(void);

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xsalsa20poly1305(unsigned char *c,
                                          const unsigned char *m,
                                          unsigned long long mlen,
                                          const unsigned char *n,
                                          const unsigned char *pk,
                                          const unsigned char *sk)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xsalsa20poly1305_open(unsigned char *m,
                                               const unsigned char *c,
                                               unsigned long long clen,
                                               const unsigned char *n,
                                               const unsigned char *pk,
                                               const unsigned char *sk)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xsalsa20poly1305_afternm(unsigned char *c,
                                                  const unsigned char *m,
                                                  unsigned long long mlen,
                                                  const unsigned char *n,
                                                  const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xsalsa20poly1305_open_afternm(unsigned char *m,
                                                       const unsigned char *c,
                                                       unsigned long long clen,
                                                       const unsigned char *n,
                                                       const unsigned char *k)
            __attribute__ ((warn_unused_result));
# 14 "/usr/include/sodium/crypto_box.h" 2
# 24 "/usr/include/sodium/crypto_box.h"
__attribute__ ((visibility ("default")))
size_t crypto_box_seedbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_box_publickeybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_box_secretkeybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_box_noncebytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_box_macbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_box_messagebytes_max(void);


__attribute__ ((visibility ("default")))
const char *crypto_box_primitive(void);

__attribute__ ((visibility ("default")))
int crypto_box_seed_keypair(unsigned char *pk, unsigned char *sk,
                            const unsigned char *seed);

__attribute__ ((visibility ("default")))
int crypto_box_keypair(unsigned char *pk, unsigned char *sk);

__attribute__ ((visibility ("default")))
int crypto_box_easy(unsigned char *c, const unsigned char *m,
                    unsigned long long mlen, const unsigned char *n,
                    const unsigned char *pk, const unsigned char *sk)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_box_open_easy(unsigned char *m, const unsigned char *c,
                         unsigned long long clen, const unsigned char *n,
                         const unsigned char *pk, const unsigned char *sk)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_box_detached(unsigned char *c, unsigned char *mac,
                        const unsigned char *m, unsigned long long mlen,
                        const unsigned char *n, const unsigned char *pk,
                        const unsigned char *sk)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_box_open_detached(unsigned char *m, const unsigned char *c,
                             const unsigned char *mac,
                             unsigned long long clen,
                             const unsigned char *n,
                             const unsigned char *pk,
                             const unsigned char *sk)
            __attribute__ ((warn_unused_result));




__attribute__ ((visibility ("default")))
size_t crypto_box_beforenmbytes(void);

__attribute__ ((visibility ("default")))
int crypto_box_beforenm(unsigned char *k, const unsigned char *pk,
                        const unsigned char *sk)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_box_easy_afternm(unsigned char *c, const unsigned char *m,
                            unsigned long long mlen, const unsigned char *n,
                            const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_box_open_easy_afternm(unsigned char *m, const unsigned char *c,
                                 unsigned long long clen, const unsigned char *n,
                                 const unsigned char *k)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_box_detached_afternm(unsigned char *c, unsigned char *mac,
                                const unsigned char *m, unsigned long long mlen,
                                const unsigned char *n, const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_box_open_detached_afternm(unsigned char *m, const unsigned char *c,
                                     const unsigned char *mac,
                                     unsigned long long clen, const unsigned char *n,
                                     const unsigned char *k)
            __attribute__ ((warn_unused_result));




__attribute__ ((visibility ("default")))
size_t crypto_box_sealbytes(void);

__attribute__ ((visibility ("default")))
int crypto_box_seal(unsigned char *c, const unsigned char *m,
                    unsigned long long mlen, const unsigned char *pk);

__attribute__ ((visibility ("default")))
int crypto_box_seal_open(unsigned char *m, const unsigned char *c,
                         unsigned long long clen,
                         const unsigned char *pk, const unsigned char *sk)
            __attribute__ ((warn_unused_result));




__attribute__ ((visibility ("default")))
size_t crypto_box_zerobytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_box_boxzerobytes(void);

__attribute__ ((visibility ("default")))
int crypto_box(unsigned char *c, const unsigned char *m,
               unsigned long long mlen, const unsigned char *n,
               const unsigned char *pk, const unsigned char *sk)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_box_open(unsigned char *m, const unsigned char *c,
                    unsigned long long clen, const unsigned char *n,
                    const unsigned char *pk, const unsigned char *sk)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_box_afternm(unsigned char *c, const unsigned char *m,
                       unsigned long long mlen, const unsigned char *n,
                       const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_box_open_afternm(unsigned char *m, const unsigned char *c,
                            unsigned long long clen, const unsigned char *n,
                            const unsigned char *k)
            __attribute__ ((warn_unused_result));
# 16 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_box_curve25519xsalsa20poly1305.h" 1
# 17 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_core_hsalsa20.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_core_hsalsa20.h" 2







__attribute__ ((visibility ("default")))
size_t crypto_core_hsalsa20_outputbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_core_hsalsa20_inputbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_core_hsalsa20_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_core_hsalsa20_constbytes(void);

__attribute__ ((visibility ("default")))
int crypto_core_hsalsa20(unsigned char *out, const unsigned char *in,
                         const unsigned char *k, const unsigned char *c);
# 18 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_core_hchacha20.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_core_hchacha20.h" 2







__attribute__ ((visibility ("default")))
size_t crypto_core_hchacha20_outputbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_core_hchacha20_inputbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_core_hchacha20_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_core_hchacha20_constbytes(void);

__attribute__ ((visibility ("default")))
int crypto_core_hchacha20(unsigned char *out, const unsigned char *in,
                          const unsigned char *k, const unsigned char *c);
# 19 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_core_salsa20.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_core_salsa20.h" 2







__attribute__ ((visibility ("default")))
size_t crypto_core_salsa20_outputbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_core_salsa20_inputbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_core_salsa20_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_core_salsa20_constbytes(void);

__attribute__ ((visibility ("default")))
int crypto_core_salsa20(unsigned char *out, const unsigned char *in,
                        const unsigned char *k, const unsigned char *c);
# 20 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_core_salsa2012.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_core_salsa2012.h" 2







__attribute__ ((visibility ("default")))
size_t crypto_core_salsa2012_outputbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_core_salsa2012_inputbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_core_salsa2012_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_core_salsa2012_constbytes(void);

__attribute__ ((visibility ("default")))
int crypto_core_salsa2012(unsigned char *out, const unsigned char *in,
                          const unsigned char *k, const unsigned char *c);
# 21 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_core_salsa208.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_core_salsa208.h" 2







__attribute__ ((visibility ("default")))
size_t crypto_core_salsa208_outputbytes(void)
            __attribute__ ((deprecated));


__attribute__ ((visibility ("default")))
size_t crypto_core_salsa208_inputbytes(void)
            __attribute__ ((deprecated));


__attribute__ ((visibility ("default")))
size_t crypto_core_salsa208_keybytes(void)
            __attribute__ ((deprecated));


__attribute__ ((visibility ("default")))
size_t crypto_core_salsa208_constbytes(void)
            __attribute__ ((deprecated));

__attribute__ ((visibility ("default")))
int crypto_core_salsa208(unsigned char *out, const unsigned char *in,
                         const unsigned char *k, const unsigned char *c);
# 22 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_generichash.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_generichash.h" 2

# 1 "/usr/include/sodium/crypto_generichash_blake2b.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_generichash_blake2b.h" 2
# 20 "/usr/include/sodium/crypto_generichash_blake2b.h"
#pragma pack(push, 1)


typedef struct __attribute__ ((aligned(64))) crypto_generichash_blake2b_state {
    uint64_t h[8];
    uint64_t t[2];
    uint64_t f[2];
    uint8_t buf[2 * 128];
    size_t buflen;
    uint8_t last_node;
} crypto_generichash_blake2b_state;




#pragma pack(pop)



__attribute__ ((visibility ("default")))
size_t crypto_generichash_blake2b_bytes_min(void);


__attribute__ ((visibility ("default")))
size_t crypto_generichash_blake2b_bytes_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_generichash_blake2b_bytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_generichash_blake2b_keybytes_min(void);


__attribute__ ((visibility ("default")))
size_t crypto_generichash_blake2b_keybytes_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_generichash_blake2b_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_generichash_blake2b_saltbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_generichash_blake2b_personalbytes(void);

__attribute__ ((visibility ("default")))
size_t crypto_generichash_blake2b_statebytes(void);

__attribute__ ((visibility ("default")))
int crypto_generichash_blake2b(unsigned char *out, size_t outlen,
                               const unsigned char *in,
                               unsigned long long inlen,
                               const unsigned char *key, size_t keylen);

__attribute__ ((visibility ("default")))
int crypto_generichash_blake2b_salt_personal(unsigned char *out, size_t outlen,
                                             const unsigned char *in,
                                             unsigned long long inlen,
                                             const unsigned char *key,
                                             size_t keylen,
                                             const unsigned char *salt,
                                             const unsigned char *personal);

__attribute__ ((visibility ("default")))
int crypto_generichash_blake2b_init(crypto_generichash_blake2b_state *state,
                                    const unsigned char *key,
                                    const size_t keylen, const size_t outlen);

__attribute__ ((visibility ("default")))
int crypto_generichash_blake2b_init_salt_personal(crypto_generichash_blake2b_state *state,
                                                  const unsigned char *key,
                                                  const size_t keylen, const size_t outlen,
                                                  const unsigned char *salt,
                                                  const unsigned char *personal);

__attribute__ ((visibility ("default")))
int crypto_generichash_blake2b_update(crypto_generichash_blake2b_state *state,
                                      const unsigned char *in,
                                      unsigned long long inlen);

__attribute__ ((visibility ("default")))
int crypto_generichash_blake2b_final(crypto_generichash_blake2b_state *state,
                                     unsigned char *out,
                                     const size_t outlen);

__attribute__ ((visibility ("default")))
void crypto_generichash_blake2b_keygen(unsigned char k[32U]);
# 7 "/usr/include/sodium/crypto_generichash.h" 2
# 17 "/usr/include/sodium/crypto_generichash.h"
__attribute__ ((visibility ("default")))
size_t crypto_generichash_bytes_min(void);


__attribute__ ((visibility ("default")))
size_t crypto_generichash_bytes_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_generichash_bytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_generichash_keybytes_min(void);


__attribute__ ((visibility ("default")))
size_t crypto_generichash_keybytes_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_generichash_keybytes(void);


__attribute__ ((visibility ("default")))
const char *crypto_generichash_primitive(void);

typedef crypto_generichash_blake2b_state crypto_generichash_state;

__attribute__ ((visibility ("default")))
size_t crypto_generichash_statebytes(void);

__attribute__ ((visibility ("default")))
int crypto_generichash(unsigned char *out, size_t outlen,
                       const unsigned char *in, unsigned long long inlen,
                       const unsigned char *key, size_t keylen);

__attribute__ ((visibility ("default")))
int crypto_generichash_init(crypto_generichash_state *state,
                            const unsigned char *key,
                            const size_t keylen, const size_t outlen);

__attribute__ ((visibility ("default")))
int crypto_generichash_update(crypto_generichash_state *state,
                              const unsigned char *in,
                              unsigned long long inlen);

__attribute__ ((visibility ("default")))
int crypto_generichash_final(crypto_generichash_state *state,
                             unsigned char *out, const size_t outlen);

__attribute__ ((visibility ("default")))
void crypto_generichash_keygen(unsigned char k[32U]);
# 23 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_generichash_blake2b.h" 1
# 24 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_hash.h" 1
# 11 "/usr/include/sodium/crypto_hash.h"
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 12 "/usr/include/sodium/crypto_hash.h" 2
# 24 "/usr/include/sodium/crypto_hash.h"
__attribute__ ((visibility ("default")))
size_t crypto_hash_bytes(void);

__attribute__ ((visibility ("default")))
int crypto_hash(unsigned char *out, const unsigned char *in,
                unsigned long long inlen);


__attribute__ ((visibility ("default")))
const char *crypto_hash_primitive(void)
            __attribute__ ((warn_unused_result));
# 25 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_hash_sha256.h" 1
# 26 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_hash_sha512.h" 1
# 27 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_kdf.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_kdf.h" 2


# 1 "/usr/include/sodium/crypto_kdf_blake2b.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_kdf_blake2b.h" 2


# 1 "/usr/include/sodium/crypto_kdf_blake2b.h" 1
# 8 "/usr/include/sodium/crypto_kdf_blake2b.h" 2
# 18 "/usr/include/sodium/crypto_kdf_blake2b.h"
__attribute__ ((visibility ("default")))
size_t crypto_kdf_blake2b_bytes_min(void);


__attribute__ ((visibility ("default")))
size_t crypto_kdf_blake2b_bytes_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_kdf_blake2b_contextbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_kdf_blake2b_keybytes(void);

__attribute__ ((visibility ("default")))
int crypto_kdf_blake2b_derive_from_key(unsigned char *subkey, size_t subkey_len,
                                       uint64_t subkey_id,
                                       const char ctx[8],
                                       const unsigned char key[32]);
# 8 "/usr/include/sodium/crypto_kdf.h" 2
# 18 "/usr/include/sodium/crypto_kdf.h"
__attribute__ ((visibility ("default")))
size_t crypto_kdf_bytes_min(void);


__attribute__ ((visibility ("default")))
size_t crypto_kdf_bytes_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_kdf_contextbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_kdf_keybytes(void);


__attribute__ ((visibility ("default")))
const char *crypto_kdf_primitive(void)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_kdf_derive_from_key(unsigned char *subkey, size_t subkey_len,
                               uint64_t subkey_id,
                               const char ctx[8],
                               const unsigned char key[32]);

__attribute__ ((visibility ("default")))
void crypto_kdf_keygen(unsigned char k[32]);
# 28 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_kdf_blake2b.h" 1
# 29 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_kx.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_kx.h" 2
# 16 "/usr/include/sodium/crypto_kx.h"
__attribute__ ((visibility ("default")))
size_t crypto_kx_publickeybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_kx_secretkeybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_kx_seedbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_kx_sessionkeybytes(void);


__attribute__ ((visibility ("default")))
const char *crypto_kx_primitive(void);

__attribute__ ((visibility ("default")))
int crypto_kx_seed_keypair(unsigned char pk[32],
                           unsigned char sk[32],
                           const unsigned char seed[32]);

__attribute__ ((visibility ("default")))
int crypto_kx_keypair(unsigned char pk[32],
                      unsigned char sk[32]);

__attribute__ ((visibility ("default")))
int crypto_kx_client_session_keys(unsigned char rx[32],
                                  unsigned char tx[32],
                                  const unsigned char client_pk[32],
                                  const unsigned char client_sk[32],
                                  const unsigned char server_pk[32])
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_kx_server_session_keys(unsigned char rx[32],
                                  unsigned char tx[32],
                                  const unsigned char server_pk[32],
                                  const unsigned char server_sk[32],
                                  const unsigned char client_pk[32])
            __attribute__ ((warn_unused_result));
# 30 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_onetimeauth.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_onetimeauth.h" 2

# 1 "/usr/include/sodium/crypto_onetimeauth_poly1305.h" 1
# 12 "/usr/include/sodium/crypto_onetimeauth_poly1305.h"
# 1 "/usr/include/stdio.h" 1 3 4
# 27 "/usr/include/stdio.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/libc-header-start.h" 1 3 4
# 28 "/usr/include/stdio.h" 2 3 4





# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 34 "/usr/include/stdio.h" 2 3 4


# 1 "/usr/include/x86_64-linux-gnu/bits/types/__FILE.h" 1 3 4




# 4 "/usr/include/x86_64-linux-gnu/bits/types/__FILE.h" 3 4
struct _IO_FILE;
typedef struct _IO_FILE __FILE;
# 37 "/usr/include/stdio.h" 2 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h" 1 3 4



struct _IO_FILE;


typedef struct _IO_FILE FILE;
# 38 "/usr/include/stdio.h" 2 3 4



# 1 "/usr/include/x86_64-linux-gnu/bits/libio.h" 1 3 4
# 35 "/usr/include/x86_64-linux-gnu/bits/libio.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/_G_config.h" 1 3 4
# 19 "/usr/include/x86_64-linux-gnu/bits/_G_config.h" 3 4
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 20 "/usr/include/x86_64-linux-gnu/bits/_G_config.h" 2 3 4

# 1 "/usr/include/x86_64-linux-gnu/bits/types/__mbstate_t.h" 1 3 4
# 13 "/usr/include/x86_64-linux-gnu/bits/types/__mbstate_t.h" 3 4
typedef struct
{
  int __count;
  union
  {
    unsigned int __wch;
    char __wchb[4];
  } __value;
} __mbstate_t;
# 22 "/usr/include/x86_64-linux-gnu/bits/_G_config.h" 2 3 4




typedef struct
{
  __off_t __pos;
  __mbstate_t __state;
} _G_fpos_t;
typedef struct
{
  __off64_t __pos;
  __mbstate_t __state;
} _G_fpos64_t;
# 36 "/usr/include/x86_64-linux-gnu/bits/libio.h" 2 3 4
# 53 "/usr/include/x86_64-linux-gnu/bits/libio.h" 3 4
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stdarg.h" 1 3 4
# 40 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stdarg.h" 3 4
typedef __builtin_va_list __gnuc_va_list;
# 54 "/usr/include/x86_64-linux-gnu/bits/libio.h" 2 3 4
# 149 "/usr/include/x86_64-linux-gnu/bits/libio.h" 3 4
struct _IO_jump_t; struct _IO_FILE;




typedef void _IO_lock_t;





struct _IO_marker {
  struct _IO_marker *_next;
  struct _IO_FILE *_sbuf;



  int _pos;
# 177 "/usr/include/x86_64-linux-gnu/bits/libio.h" 3 4
};


enum __codecvt_result
{
  __codecvt_ok,
  __codecvt_partial,
  __codecvt_error,
  __codecvt_noconv
};
# 245 "/usr/include/x86_64-linux-gnu/bits/libio.h" 3 4
struct _IO_FILE {
  int _flags;




  char* _IO_read_ptr;
  char* _IO_read_end;
  char* _IO_read_base;
  char* _IO_write_base;
  char* _IO_write_ptr;
  char* _IO_write_end;
  char* _IO_buf_base;
  char* _IO_buf_end;

  char *_IO_save_base;
  char *_IO_backup_base;
  char *_IO_save_end;

  struct _IO_marker *_markers;

  struct _IO_FILE *_chain;

  int _fileno;



  int _flags2;

  __off_t _old_offset;



  unsigned short _cur_column;
  signed char _vtable_offset;
  char _shortbuf[1];



  _IO_lock_t *_lock;
# 293 "/usr/include/x86_64-linux-gnu/bits/libio.h" 3 4
  __off64_t _offset;







  void *__pad1;
  void *__pad2;
  void *__pad3;
  void *__pad4;

  size_t __pad5;
  int _mode;

  char _unused2[15 * sizeof (int) - 4 * sizeof (void *) - sizeof (size_t)];

};


typedef struct _IO_FILE _IO_FILE;


struct _IO_FILE_plus;

extern struct _IO_FILE_plus _IO_2_1_stdin_;
extern struct _IO_FILE_plus _IO_2_1_stdout_;
extern struct _IO_FILE_plus _IO_2_1_stderr_;
# 337 "/usr/include/x86_64-linux-gnu/bits/libio.h" 3 4
typedef __ssize_t __io_read_fn (void *__cookie, char *__buf, size_t __nbytes);







typedef __ssize_t __io_write_fn (void *__cookie, const char *__buf,
     size_t __n);







typedef int __io_seek_fn (void *__cookie, __off64_t *__pos, int __w);


typedef int __io_close_fn (void *__cookie);
# 389 "/usr/include/x86_64-linux-gnu/bits/libio.h" 3 4
extern int __underflow (_IO_FILE *);
extern int __uflow (_IO_FILE *);
extern int __overflow (_IO_FILE *, int);
# 433 "/usr/include/x86_64-linux-gnu/bits/libio.h" 3 4
extern int _IO_getc (_IO_FILE *__fp);
extern int _IO_putc (int __c, _IO_FILE *__fp);
extern int _IO_feof (_IO_FILE *__fp) __attribute__ ((__nothrow__ , __leaf__));
extern int _IO_ferror (_IO_FILE *__fp) __attribute__ ((__nothrow__ , __leaf__));

extern int _IO_peekc_locked (_IO_FILE *__fp);





extern void _IO_flockfile (_IO_FILE *) __attribute__ ((__nothrow__ , __leaf__));
extern void _IO_funlockfile (_IO_FILE *) __attribute__ ((__nothrow__ , __leaf__));
extern int _IO_ftrylockfile (_IO_FILE *) __attribute__ ((__nothrow__ , __leaf__));
# 462 "/usr/include/x86_64-linux-gnu/bits/libio.h" 3 4
extern int _IO_vfscanf (_IO_FILE * __restrict, const char * __restrict,
   __gnuc_va_list, int *__restrict);
extern int _IO_vfprintf (_IO_FILE *__restrict, const char *__restrict,
    __gnuc_va_list);
extern __ssize_t _IO_padn (_IO_FILE *, int, __ssize_t);
extern size_t _IO_sgetn (_IO_FILE *, void *, size_t);

extern __off64_t _IO_seekoff (_IO_FILE *, __off64_t, int, int);
extern __off64_t _IO_seekpos (_IO_FILE *, __off64_t, int);

extern void _IO_free_backup_area (_IO_FILE *) __attribute__ ((__nothrow__ , __leaf__));
# 42 "/usr/include/stdio.h" 2 3 4




typedef __gnuc_va_list va_list;
# 78 "/usr/include/stdio.h" 3 4
typedef _G_fpos_t fpos_t;
# 131 "/usr/include/stdio.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/stdio_lim.h" 1 3 4
# 132 "/usr/include/stdio.h" 2 3 4



extern struct _IO_FILE *stdin;
extern struct _IO_FILE *stdout;
extern struct _IO_FILE *stderr;






extern int remove (const char *__filename) __attribute__ ((__nothrow__ , __leaf__));

extern int rename (const char *__old, const char *__new) __attribute__ ((__nothrow__ , __leaf__));



extern int renameat (int __oldfd, const char *__old, int __newfd,
       const char *__new) __attribute__ ((__nothrow__ , __leaf__));







extern FILE *tmpfile (void) ;
# 173 "/usr/include/stdio.h" 3 4
extern char *tmpnam (char *__s) __attribute__ ((__nothrow__ , __leaf__)) ;




extern char *tmpnam_r (char *__s) __attribute__ ((__nothrow__ , __leaf__)) ;
# 190 "/usr/include/stdio.h" 3 4
extern char *tempnam (const char *__dir, const char *__pfx)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__)) ;







extern int fclose (FILE *__stream);




extern int fflush (FILE *__stream);
# 213 "/usr/include/stdio.h" 3 4
extern int fflush_unlocked (FILE *__stream);
# 232 "/usr/include/stdio.h" 3 4
extern FILE *fopen (const char *__restrict __filename,
      const char *__restrict __modes) ;




extern FILE *freopen (const char *__restrict __filename,
        const char *__restrict __modes,
        FILE *__restrict __stream) ;
# 265 "/usr/include/stdio.h" 3 4
extern FILE *fdopen (int __fd, const char *__modes) __attribute__ ((__nothrow__ , __leaf__)) ;
# 278 "/usr/include/stdio.h" 3 4
extern FILE *fmemopen (void *__s, size_t __len, const char *__modes)
  __attribute__ ((__nothrow__ , __leaf__)) ;




extern FILE *open_memstream (char **__bufloc, size_t *__sizeloc) __attribute__ ((__nothrow__ , __leaf__)) ;





extern void setbuf (FILE *__restrict __stream, char *__restrict __buf) __attribute__ ((__nothrow__ , __leaf__));



extern int setvbuf (FILE *__restrict __stream, char *__restrict __buf,
      int __modes, size_t __n) __attribute__ ((__nothrow__ , __leaf__));




extern void setbuffer (FILE *__restrict __stream, char *__restrict __buf,
         size_t __size) __attribute__ ((__nothrow__ , __leaf__));


extern void setlinebuf (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__));







extern int fprintf (FILE *__restrict __stream,
      const char *__restrict __format, ...);




extern int printf (const char *__restrict __format, ...);

extern int sprintf (char *__restrict __s,
      const char *__restrict __format, ...) __attribute__ ((__nothrow__));





extern int vfprintf (FILE *__restrict __s, const char *__restrict __format,
       __gnuc_va_list __arg);




extern int vprintf (const char *__restrict __format, __gnuc_va_list __arg);

extern int vsprintf (char *__restrict __s, const char *__restrict __format,
       __gnuc_va_list __arg) __attribute__ ((__nothrow__));



extern int snprintf (char *__restrict __s, size_t __maxlen,
       const char *__restrict __format, ...)
     __attribute__ ((__nothrow__)) __attribute__ ((__format__ (__printf__, 3, 4)));

extern int vsnprintf (char *__restrict __s, size_t __maxlen,
        const char *__restrict __format, __gnuc_va_list __arg)
     __attribute__ ((__nothrow__)) __attribute__ ((__format__ (__printf__, 3, 0)));
# 365 "/usr/include/stdio.h" 3 4
extern int vdprintf (int __fd, const char *__restrict __fmt,
       __gnuc_va_list __arg)
     __attribute__ ((__format__ (__printf__, 2, 0)));
extern int dprintf (int __fd, const char *__restrict __fmt, ...)
     __attribute__ ((__format__ (__printf__, 2, 3)));







extern int fscanf (FILE *__restrict __stream,
     const char *__restrict __format, ...) ;




extern int scanf (const char *__restrict __format, ...) ;

extern int sscanf (const char *__restrict __s,
     const char *__restrict __format, ...) __attribute__ ((__nothrow__ , __leaf__));
# 395 "/usr/include/stdio.h" 3 4
extern int fscanf (FILE *__restrict __stream, const char *__restrict __format, ...) __asm__ ("" "__isoc99_fscanf")

                               ;
extern int scanf (const char *__restrict __format, ...) __asm__ ("" "__isoc99_scanf")
                              ;
extern int sscanf (const char *__restrict __s, const char *__restrict __format, ...) __asm__ ("" "__isoc99_sscanf") __attribute__ ((__nothrow__ , __leaf__))

                      ;
# 420 "/usr/include/stdio.h" 3 4
extern int vfscanf (FILE *__restrict __s, const char *__restrict __format,
      __gnuc_va_list __arg)
     __attribute__ ((__format__ (__scanf__, 2, 0))) ;





extern int vscanf (const char *__restrict __format, __gnuc_va_list __arg)
     __attribute__ ((__format__ (__scanf__, 1, 0))) ;


extern int vsscanf (const char *__restrict __s,
      const char *__restrict __format, __gnuc_va_list __arg)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__format__ (__scanf__, 2, 0)));
# 443 "/usr/include/stdio.h" 3 4
extern int vfscanf (FILE *__restrict __s, const char *__restrict __format, __gnuc_va_list __arg) __asm__ ("" "__isoc99_vfscanf")



     __attribute__ ((__format__ (__scanf__, 2, 0))) ;
extern int vscanf (const char *__restrict __format, __gnuc_va_list __arg) __asm__ ("" "__isoc99_vscanf")

     __attribute__ ((__format__ (__scanf__, 1, 0))) ;
extern int vsscanf (const char *__restrict __s, const char *__restrict __format, __gnuc_va_list __arg) __asm__ ("" "__isoc99_vsscanf") __attribute__ ((__nothrow__ , __leaf__))



     __attribute__ ((__format__ (__scanf__, 2, 0)));
# 477 "/usr/include/stdio.h" 3 4
extern int fgetc (FILE *__stream);
extern int getc (FILE *__stream);





extern int getchar (void);
# 495 "/usr/include/stdio.h" 3 4
extern int getc_unlocked (FILE *__stream);
extern int getchar_unlocked (void);
# 506 "/usr/include/stdio.h" 3 4
extern int fgetc_unlocked (FILE *__stream);
# 517 "/usr/include/stdio.h" 3 4
extern int fputc (int __c, FILE *__stream);
extern int putc (int __c, FILE *__stream);





extern int putchar (int __c);
# 537 "/usr/include/stdio.h" 3 4
extern int fputc_unlocked (int __c, FILE *__stream);







extern int putc_unlocked (int __c, FILE *__stream);
extern int putchar_unlocked (int __c);






extern int getw (FILE *__stream);


extern int putw (int __w, FILE *__stream);







extern char *fgets (char *__restrict __s, int __n, FILE *__restrict __stream)
     ;
# 603 "/usr/include/stdio.h" 3 4
extern __ssize_t __getdelim (char **__restrict __lineptr,
          size_t *__restrict __n, int __delimiter,
          FILE *__restrict __stream) ;
extern __ssize_t getdelim (char **__restrict __lineptr,
        size_t *__restrict __n, int __delimiter,
        FILE *__restrict __stream) ;







extern __ssize_t getline (char **__restrict __lineptr,
       size_t *__restrict __n,
       FILE *__restrict __stream) ;







extern int fputs (const char *__restrict __s, FILE *__restrict __stream);





extern int puts (const char *__s);






extern int ungetc (int __c, FILE *__stream);






extern size_t fread (void *__restrict __ptr, size_t __size,
       size_t __n, FILE *__restrict __stream) ;




extern size_t fwrite (const void *__restrict __ptr, size_t __size,
        size_t __n, FILE *__restrict __s);
# 673 "/usr/include/stdio.h" 3 4
extern size_t fread_unlocked (void *__restrict __ptr, size_t __size,
         size_t __n, FILE *__restrict __stream) ;
extern size_t fwrite_unlocked (const void *__restrict __ptr, size_t __size,
          size_t __n, FILE *__restrict __stream);







extern int fseek (FILE *__stream, long int __off, int __whence);




extern long int ftell (FILE *__stream) ;




extern void rewind (FILE *__stream);
# 707 "/usr/include/stdio.h" 3 4
extern int fseeko (FILE *__stream, __off_t __off, int __whence);




extern __off_t ftello (FILE *__stream) ;
# 731 "/usr/include/stdio.h" 3 4
extern int fgetpos (FILE *__restrict __stream, fpos_t *__restrict __pos);




extern int fsetpos (FILE *__stream, const fpos_t *__pos);
# 757 "/usr/include/stdio.h" 3 4
extern void clearerr (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__));

extern int feof (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;

extern int ferror (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;



extern void clearerr_unlocked (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__));
extern int feof_unlocked (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;
extern int ferror_unlocked (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;







extern void perror (const char *__s);





# 1 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h" 1 3 4
# 26 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h" 3 4
extern int sys_nerr;
extern const char *const sys_errlist[];
# 782 "/usr/include/stdio.h" 2 3 4




extern int fileno (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;




extern int fileno_unlocked (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;
# 800 "/usr/include/stdio.h" 3 4
extern FILE *popen (const char *__command, const char *__modes) ;





extern int pclose (FILE *__stream);





extern char *ctermid (char *__s) __attribute__ ((__nothrow__ , __leaf__));
# 840 "/usr/include/stdio.h" 3 4
extern void flockfile (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__));



extern int ftrylockfile (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;


extern void funlockfile (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__));
# 868 "/usr/include/stdio.h" 3 4

# 13 "/usr/include/sodium/crypto_onetimeauth_poly1305.h" 2







# 19 "/usr/include/sodium/crypto_onetimeauth_poly1305.h"
typedef struct __attribute__ ((aligned(16))) crypto_onetimeauth_poly1305_state {
    unsigned char opaque[256];
} crypto_onetimeauth_poly1305_state;

__attribute__ ((visibility ("default")))
size_t crypto_onetimeauth_poly1305_statebytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_onetimeauth_poly1305_bytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_onetimeauth_poly1305_keybytes(void);

__attribute__ ((visibility ("default")))
int crypto_onetimeauth_poly1305(unsigned char *out,
                                const unsigned char *in,
                                unsigned long long inlen,
                                const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_onetimeauth_poly1305_verify(const unsigned char *h,
                                       const unsigned char *in,
                                       unsigned long long inlen,
                                       const unsigned char *k)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_onetimeauth_poly1305_init(crypto_onetimeauth_poly1305_state *state,
                                     const unsigned char *key);

__attribute__ ((visibility ("default")))
int crypto_onetimeauth_poly1305_update(crypto_onetimeauth_poly1305_state *state,
                                       const unsigned char *in,
                                       unsigned long long inlen);

__attribute__ ((visibility ("default")))
int crypto_onetimeauth_poly1305_final(crypto_onetimeauth_poly1305_state *state,
                                      unsigned char *out);

__attribute__ ((visibility ("default")))
void crypto_onetimeauth_poly1305_keygen(unsigned char k[32U]);
# 7 "/usr/include/sodium/crypto_onetimeauth.h" 2
# 16 "/usr/include/sodium/crypto_onetimeauth.h"
typedef crypto_onetimeauth_poly1305_state crypto_onetimeauth_state;

__attribute__ ((visibility ("default")))
size_t crypto_onetimeauth_statebytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_onetimeauth_bytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_onetimeauth_keybytes(void);


__attribute__ ((visibility ("default")))
const char *crypto_onetimeauth_primitive(void);

__attribute__ ((visibility ("default")))
int crypto_onetimeauth(unsigned char *out, const unsigned char *in,
                       unsigned long long inlen, const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_onetimeauth_verify(const unsigned char *h, const unsigned char *in,
                              unsigned long long inlen, const unsigned char *k)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_onetimeauth_init(crypto_onetimeauth_state *state,
                            const unsigned char *key);

__attribute__ ((visibility ("default")))
int crypto_onetimeauth_update(crypto_onetimeauth_state *state,
                              const unsigned char *in,
                              unsigned long long inlen);

__attribute__ ((visibility ("default")))
int crypto_onetimeauth_final(crypto_onetimeauth_state *state,
                             unsigned char *out);

__attribute__ ((visibility ("default")))
void crypto_onetimeauth_keygen(unsigned char k[32U]);
# 31 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_onetimeauth_poly1305.h" 1
# 32 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_pwhash.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_pwhash.h" 2

# 1 "/usr/include/sodium/crypto_pwhash_argon2i.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include-fixed/limits.h" 1 3 4
# 34 "/usr/lib/gcc/x86_64-linux-gnu/7/include-fixed/limits.h" 3 4
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include-fixed/syslimits.h" 1 3 4






# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include-fixed/limits.h" 1 3 4
# 194 "/usr/lib/gcc/x86_64-linux-gnu/7/include-fixed/limits.h" 3 4
# 1 "/usr/include/limits.h" 1 3 4
# 26 "/usr/include/limits.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/libc-header-start.h" 1 3 4
# 27 "/usr/include/limits.h" 2 3 4
# 183 "/usr/include/limits.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/posix1_lim.h" 1 3 4
# 160 "/usr/include/x86_64-linux-gnu/bits/posix1_lim.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/local_lim.h" 1 3 4
# 38 "/usr/include/x86_64-linux-gnu/bits/local_lim.h" 3 4
# 1 "/usr/include/linux/limits.h" 1 3 4
# 39 "/usr/include/x86_64-linux-gnu/bits/local_lim.h" 2 3 4
# 161 "/usr/include/x86_64-linux-gnu/bits/posix1_lim.h" 2 3 4
# 184 "/usr/include/limits.h" 2 3 4



# 1 "/usr/include/x86_64-linux-gnu/bits/posix2_lim.h" 1 3 4
# 188 "/usr/include/limits.h" 2 3 4
# 195 "/usr/lib/gcc/x86_64-linux-gnu/7/include-fixed/limits.h" 2 3 4
# 8 "/usr/lib/gcc/x86_64-linux-gnu/7/include-fixed/syslimits.h" 2 3 4
# 35 "/usr/lib/gcc/x86_64-linux-gnu/7/include-fixed/limits.h" 2 3 4
# 5 "/usr/include/sodium/crypto_pwhash_argon2i.h" 2
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 6 "/usr/include/sodium/crypto_pwhash_argon2i.h" 2
# 18 "/usr/include/sodium/crypto_pwhash_argon2i.h"
__attribute__ ((visibility ("default")))
int crypto_pwhash_argon2i_alg_argon2i13(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2i_bytes_min(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2i_bytes_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2i_passwd_min(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2i_passwd_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2i_saltbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2i_strbytes(void);


__attribute__ ((visibility ("default")))
const char *crypto_pwhash_argon2i_strprefix(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2i_opslimit_min(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2i_opslimit_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2i_memlimit_min(void);



__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2i_memlimit_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2i_opslimit_interactive(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2i_memlimit_interactive(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2i_opslimit_moderate(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2i_memlimit_moderate(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2i_opslimit_sensitive(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2i_memlimit_sensitive(void);

__attribute__ ((visibility ("default")))
int crypto_pwhash_argon2i(unsigned char * const out,
                          unsigned long long outlen,
                          const char * const passwd,
                          unsigned long long passwdlen,
                          const unsigned char * const salt,
                          unsigned long long opslimit, size_t memlimit,
                          int alg)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_pwhash_argon2i_str(char out[128U],
                              const char * const passwd,
                              unsigned long long passwdlen,
                              unsigned long long opslimit, size_t memlimit)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_pwhash_argon2i_str_verify(const char str[128U],
                                     const char * const passwd,
                                     unsigned long long passwdlen)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_pwhash_argon2i_str_needs_rehash(const char str[128U],
                                           unsigned long long opslimit, size_t memlimit)
            __attribute__ ((warn_unused_result));
# 7 "/usr/include/sodium/crypto_pwhash.h" 2
# 1 "/usr/include/sodium/crypto_pwhash_argon2id.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include-fixed/limits.h" 1 3 4
# 5 "/usr/include/sodium/crypto_pwhash_argon2id.h" 2
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 6 "/usr/include/sodium/crypto_pwhash_argon2id.h" 2
# 18 "/usr/include/sodium/crypto_pwhash_argon2id.h"
__attribute__ ((visibility ("default")))
int crypto_pwhash_argon2id_alg_argon2id13(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2id_bytes_min(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2id_bytes_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2id_passwd_min(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2id_passwd_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2id_saltbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2id_strbytes(void);


__attribute__ ((visibility ("default")))
const char *crypto_pwhash_argon2id_strprefix(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2id_opslimit_min(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2id_opslimit_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2id_memlimit_min(void);



__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2id_memlimit_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2id_opslimit_interactive(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2id_memlimit_interactive(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2id_opslimit_moderate(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2id_memlimit_moderate(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2id_opslimit_sensitive(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_argon2id_memlimit_sensitive(void);

__attribute__ ((visibility ("default")))
int crypto_pwhash_argon2id(unsigned char * const out,
                           unsigned long long outlen,
                           const char * const passwd,
                           unsigned long long passwdlen,
                           const unsigned char * const salt,
                           unsigned long long opslimit, size_t memlimit,
                           int alg)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_pwhash_argon2id_str(char out[128U],
                               const char * const passwd,
                               unsigned long long passwdlen,
                               unsigned long long opslimit, size_t memlimit)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_pwhash_argon2id_str_verify(const char str[128U],
                                      const char * const passwd,
                                      unsigned long long passwdlen)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_pwhash_argon2id_str_needs_rehash(const char str[128U],
                                            unsigned long long opslimit, size_t memlimit)
            __attribute__ ((warn_unused_result));
# 8 "/usr/include/sodium/crypto_pwhash.h" 2
# 18 "/usr/include/sodium/crypto_pwhash.h"
__attribute__ ((visibility ("default")))
int crypto_pwhash_alg_argon2i13(void);


__attribute__ ((visibility ("default")))
int crypto_pwhash_alg_argon2id13(void);


__attribute__ ((visibility ("default")))
int crypto_pwhash_alg_default(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_bytes_min(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_bytes_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_passwd_min(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_passwd_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_saltbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_strbytes(void);


__attribute__ ((visibility ("default")))
const char *crypto_pwhash_strprefix(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_opslimit_min(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_opslimit_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_memlimit_min(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_memlimit_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_opslimit_interactive(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_memlimit_interactive(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_opslimit_moderate(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_memlimit_moderate(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_opslimit_sensitive(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_memlimit_sensitive(void);







__attribute__ ((visibility ("default")))
int crypto_pwhash(unsigned char * const out, unsigned long long outlen,
                  const char * const passwd, unsigned long long passwdlen,
                  const unsigned char * const salt,
                  unsigned long long opslimit, size_t memlimit, int alg)
            __attribute__ ((warn_unused_result));






__attribute__ ((visibility ("default")))
int crypto_pwhash_str(char out[128U],
                      const char * const passwd, unsigned long long passwdlen,
                      unsigned long long opslimit, size_t memlimit)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_pwhash_str_alg(char out[128U],
                          const char * const passwd, unsigned long long passwdlen,
                          unsigned long long opslimit, size_t memlimit, int alg)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_pwhash_str_verify(const char str[128U],
                             const char * const passwd,
                             unsigned long long passwdlen)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_pwhash_str_needs_rehash(const char str[128U],
                                   unsigned long long opslimit, size_t memlimit)
            __attribute__ ((warn_unused_result));


__attribute__ ((visibility ("default")))
const char *crypto_pwhash_primitive(void)
            __attribute__ ((warn_unused_result));
# 33 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_pwhash_argon2i.h" 1
# 34 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_scalarmult.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_scalarmult.h" 2

# 1 "/usr/include/sodium/crypto_scalarmult_curve25519.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_scalarmult_curve25519.h" 2
# 13 "/usr/include/sodium/crypto_scalarmult_curve25519.h"
__attribute__ ((visibility ("default")))
size_t crypto_scalarmult_curve25519_bytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_scalarmult_curve25519_scalarbytes(void);
# 28 "/usr/include/sodium/crypto_scalarmult_curve25519.h"
__attribute__ ((visibility ("default")))
int crypto_scalarmult_curve25519(unsigned char *q, const unsigned char *n,
                                 const unsigned char *p)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_scalarmult_curve25519_base(unsigned char *q, const unsigned char *n);
# 7 "/usr/include/sodium/crypto_scalarmult.h" 2







__attribute__ ((visibility ("default")))
size_t crypto_scalarmult_bytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_scalarmult_scalarbytes(void);


__attribute__ ((visibility ("default")))
const char *crypto_scalarmult_primitive(void);

__attribute__ ((visibility ("default")))
int crypto_scalarmult_base(unsigned char *q, const unsigned char *n);
# 36 "/usr/include/sodium/crypto_scalarmult.h"
__attribute__ ((visibility ("default")))
int crypto_scalarmult(unsigned char *q, const unsigned char *n,
                      const unsigned char *p)
            __attribute__ ((warn_unused_result));
# 35 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_scalarmult_curve25519.h" 1
# 36 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_secretbox.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_secretbox.h" 2

# 1 "/usr/include/sodium/crypto_secretbox_xsalsa20poly1305.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_secretbox_xsalsa20poly1305.h" 2
# 16 "/usr/include/sodium/crypto_secretbox_xsalsa20poly1305.h"
__attribute__ ((visibility ("default")))
size_t crypto_secretbox_xsalsa20poly1305_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_secretbox_xsalsa20poly1305_noncebytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_secretbox_xsalsa20poly1305_macbytes(void);




__attribute__ ((visibility ("default")))
size_t crypto_secretbox_xsalsa20poly1305_messagebytes_max(void);

__attribute__ ((visibility ("default")))
int crypto_secretbox_xsalsa20poly1305(unsigned char *c,
                                      const unsigned char *m,
                                      unsigned long long mlen,
                                      const unsigned char *n,
                                      const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_secretbox_xsalsa20poly1305_open(unsigned char *m,
                                           const unsigned char *c,
                                           unsigned long long clen,
                                           const unsigned char *n,
                                           const unsigned char *k)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
void crypto_secretbox_xsalsa20poly1305_keygen(unsigned char k[32U]);




__attribute__ ((visibility ("default")))
size_t crypto_secretbox_xsalsa20poly1305_boxzerobytes(void);




__attribute__ ((visibility ("default")))
size_t crypto_secretbox_xsalsa20poly1305_zerobytes(void);
# 7 "/usr/include/sodium/crypto_secretbox.h" 2
# 17 "/usr/include/sodium/crypto_secretbox.h"
__attribute__ ((visibility ("default")))
size_t crypto_secretbox_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_secretbox_noncebytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_secretbox_macbytes(void);


__attribute__ ((visibility ("default")))
const char *crypto_secretbox_primitive(void);


__attribute__ ((visibility ("default")))
size_t crypto_secretbox_messagebytes_max(void);

__attribute__ ((visibility ("default")))
int crypto_secretbox_easy(unsigned char *c, const unsigned char *m,
                          unsigned long long mlen, const unsigned char *n,
                          const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_secretbox_open_easy(unsigned char *m, const unsigned char *c,
                               unsigned long long clen, const unsigned char *n,
                               const unsigned char *k)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_secretbox_detached(unsigned char *c, unsigned char *mac,
                              const unsigned char *m,
                              unsigned long long mlen,
                              const unsigned char *n,
                              const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_secretbox_open_detached(unsigned char *m,
                                   const unsigned char *c,
                                   const unsigned char *mac,
                                   unsigned long long clen,
                                   const unsigned char *n,
                                   const unsigned char *k)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
void crypto_secretbox_keygen(unsigned char k[32U]);




__attribute__ ((visibility ("default")))
size_t crypto_secretbox_zerobytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_secretbox_boxzerobytes(void);

__attribute__ ((visibility ("default")))
int crypto_secretbox(unsigned char *c, const unsigned char *m,
                     unsigned long long mlen, const unsigned char *n,
                     const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_secretbox_open(unsigned char *m, const unsigned char *c,
                          unsigned long long clen, const unsigned char *n,
                          const unsigned char *k)
            __attribute__ ((warn_unused_result));
# 37 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_secretbox_xsalsa20poly1305.h" 1
# 38 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_secretstream_xchacha20poly1305.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_secretstream_xchacha20poly1305.h" 2

# 1 "/usr/include/sodium/crypto_aead_xchacha20poly1305.h" 1
# 7 "/usr/include/sodium/crypto_secretstream_xchacha20poly1305.h" 2
# 1 "/usr/include/sodium/crypto_stream_chacha20.h" 1
# 12 "/usr/include/sodium/crypto_stream_chacha20.h"
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 13 "/usr/include/sodium/crypto_stream_chacha20.h" 2
# 24 "/usr/include/sodium/crypto_stream_chacha20.h"
__attribute__ ((visibility ("default")))
size_t crypto_stream_chacha20_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_stream_chacha20_noncebytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_stream_chacha20_messagebytes_max(void);



__attribute__ ((visibility ("default")))
int crypto_stream_chacha20(unsigned char *c, unsigned long long clen,
                           const unsigned char *n, const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_stream_chacha20_xor(unsigned char *c, const unsigned char *m,
                               unsigned long long mlen, const unsigned char *n,
                               const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_stream_chacha20_xor_ic(unsigned char *c, const unsigned char *m,
                                  unsigned long long mlen,
                                  const unsigned char *n, uint64_t ic,
                                  const unsigned char *k);

__attribute__ ((visibility ("default")))
void crypto_stream_chacha20_keygen(unsigned char k[32U]);




__attribute__ ((visibility ("default")))
size_t crypto_stream_chacha20_ietf_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_stream_chacha20_ietf_noncebytes(void);



__attribute__ ((visibility ("default")))
size_t crypto_stream_chacha20_ietf_messagebytes_max(void);

__attribute__ ((visibility ("default")))
int crypto_stream_chacha20_ietf(unsigned char *c, unsigned long long clen,
                                const unsigned char *n, const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_stream_chacha20_ietf_xor(unsigned char *c, const unsigned char *m,
                                    unsigned long long mlen, const unsigned char *n,
                                    const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_stream_chacha20_ietf_xor_ic(unsigned char *c, const unsigned char *m,
                                       unsigned long long mlen,
                                       const unsigned char *n, uint32_t ic,
                                       const unsigned char *k);

__attribute__ ((visibility ("default")))
void crypto_stream_chacha20_ietf_keygen(unsigned char k[32U]);
# 8 "/usr/include/sodium/crypto_secretstream_xchacha20poly1305.h" 2
# 19 "/usr/include/sodium/crypto_secretstream_xchacha20poly1305.h"
__attribute__ ((visibility ("default")))
size_t crypto_secretstream_xchacha20poly1305_abytes(void);



__attribute__ ((visibility ("default")))
size_t crypto_secretstream_xchacha20poly1305_headerbytes(void);



__attribute__ ((visibility ("default")))
size_t crypto_secretstream_xchacha20poly1305_keybytes(void);



__attribute__ ((visibility ("default")))
size_t crypto_secretstream_xchacha20poly1305_messagebytes_max(void);


__attribute__ ((visibility ("default")))
unsigned char crypto_secretstream_xchacha20poly1305_tag_message(void);


__attribute__ ((visibility ("default")))
unsigned char crypto_secretstream_xchacha20poly1305_tag_push(void);


__attribute__ ((visibility ("default")))
unsigned char crypto_secretstream_xchacha20poly1305_tag_rekey(void);




__attribute__ ((visibility ("default")))
unsigned char crypto_secretstream_xchacha20poly1305_tag_final(void);

typedef struct crypto_secretstream_xchacha20poly1305_state {
    unsigned char k[32U];
    unsigned char nonce[12U];
    unsigned char _pad[8];
} crypto_secretstream_xchacha20poly1305_state;

__attribute__ ((visibility ("default")))
size_t crypto_secretstream_xchacha20poly1305_statebytes(void);

__attribute__ ((visibility ("default")))
void crypto_secretstream_xchacha20poly1305_keygen
   (unsigned char k[32U]);

__attribute__ ((visibility ("default")))
int crypto_secretstream_xchacha20poly1305_init_push
   (crypto_secretstream_xchacha20poly1305_state *state,
    unsigned char header[24U],
    const unsigned char k[32U]);

__attribute__ ((visibility ("default")))
int crypto_secretstream_xchacha20poly1305_push
   (crypto_secretstream_xchacha20poly1305_state *state,
    unsigned char *c, unsigned long long *clen_p,
    const unsigned char *m, unsigned long long mlen,
    const unsigned char *ad, unsigned long long adlen, unsigned char tag);

__attribute__ ((visibility ("default")))
int crypto_secretstream_xchacha20poly1305_init_pull
   (crypto_secretstream_xchacha20poly1305_state *state,
    const unsigned char header[24U],
    const unsigned char k[32U]);

__attribute__ ((visibility ("default")))
int crypto_secretstream_xchacha20poly1305_pull
   (crypto_secretstream_xchacha20poly1305_state *state,
    unsigned char *m, unsigned long long *mlen_p, unsigned char *tag_p,
    const unsigned char *c, unsigned long long clen,
    const unsigned char *ad, unsigned long long adlen);

__attribute__ ((visibility ("default")))
void crypto_secretstream_xchacha20poly1305_rekey
    (crypto_secretstream_xchacha20poly1305_state *state);
# 39 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_shorthash.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_shorthash.h" 2

# 1 "/usr/include/sodium/crypto_shorthash_siphash24.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_shorthash_siphash24.h" 2
# 17 "/usr/include/sodium/crypto_shorthash_siphash24.h"
__attribute__ ((visibility ("default")))
size_t crypto_shorthash_siphash24_bytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_shorthash_siphash24_keybytes(void);

__attribute__ ((visibility ("default")))
int crypto_shorthash_siphash24(unsigned char *out, const unsigned char *in,
                               unsigned long long inlen, const unsigned char *k);





__attribute__ ((visibility ("default")))
size_t crypto_shorthash_siphashx24_bytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_shorthash_siphashx24_keybytes(void);

__attribute__ ((visibility ("default")))
int crypto_shorthash_siphashx24(unsigned char *out, const unsigned char *in,
                                unsigned long long inlen, const unsigned char *k);
# 7 "/usr/include/sodium/crypto_shorthash.h" 2
# 17 "/usr/include/sodium/crypto_shorthash.h"
__attribute__ ((visibility ("default")))
size_t crypto_shorthash_bytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_shorthash_keybytes(void);


__attribute__ ((visibility ("default")))
const char *crypto_shorthash_primitive(void);

__attribute__ ((visibility ("default")))
int crypto_shorthash(unsigned char *out, const unsigned char *in,
                     unsigned long long inlen, const unsigned char *k);

__attribute__ ((visibility ("default")))
void crypto_shorthash_keygen(unsigned char k[16U]);
# 40 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_shorthash_siphash24.h" 1
# 41 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_sign.h" 1
# 11 "/usr/include/sodium/crypto_sign.h"
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 12 "/usr/include/sodium/crypto_sign.h" 2

# 1 "/usr/include/sodium/crypto_sign_ed25519.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_sign_ed25519.h" 2
# 15 "/usr/include/sodium/crypto_sign_ed25519.h"
typedef struct crypto_sign_ed25519ph_state {
    crypto_hash_sha512_state hs;
} crypto_sign_ed25519ph_state;

__attribute__ ((visibility ("default")))
size_t crypto_sign_ed25519ph_statebytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_sign_ed25519_bytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_sign_ed25519_seedbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_sign_ed25519_publickeybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_sign_ed25519_secretkeybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_sign_ed25519_messagebytes_max(void);

__attribute__ ((visibility ("default")))
int crypto_sign_ed25519(unsigned char *sm, unsigned long long *smlen_p,
                        const unsigned char *m, unsigned long long mlen,
                        const unsigned char *sk);

__attribute__ ((visibility ("default")))
int crypto_sign_ed25519_open(unsigned char *m, unsigned long long *mlen_p,
                             const unsigned char *sm, unsigned long long smlen,
                             const unsigned char *pk)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_sign_ed25519_detached(unsigned char *sig,
                                 unsigned long long *siglen_p,
                                 const unsigned char *m,
                                 unsigned long long mlen,
                                 const unsigned char *sk);

__attribute__ ((visibility ("default")))
int crypto_sign_ed25519_verify_detached(const unsigned char *sig,
                                        const unsigned char *m,
                                        unsigned long long mlen,
                                        const unsigned char *pk)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_sign_ed25519_keypair(unsigned char *pk, unsigned char *sk);

__attribute__ ((visibility ("default")))
int crypto_sign_ed25519_seed_keypair(unsigned char *pk, unsigned char *sk,
                                     const unsigned char *seed);

__attribute__ ((visibility ("default")))
int crypto_sign_ed25519_pk_to_curve25519(unsigned char *curve25519_pk,
                                         const unsigned char *ed25519_pk)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_sign_ed25519_sk_to_curve25519(unsigned char *curve25519_sk,
                                         const unsigned char *ed25519_sk);

__attribute__ ((visibility ("default")))
int crypto_sign_ed25519_sk_to_seed(unsigned char *seed,
                                   const unsigned char *sk);

__attribute__ ((visibility ("default")))
int crypto_sign_ed25519_sk_to_pk(unsigned char *pk, const unsigned char *sk);

__attribute__ ((visibility ("default")))
int crypto_sign_ed25519ph_init(crypto_sign_ed25519ph_state *state);

__attribute__ ((visibility ("default")))
int crypto_sign_ed25519ph_update(crypto_sign_ed25519ph_state *state,
                                 const unsigned char *m,
                                 unsigned long long mlen);

__attribute__ ((visibility ("default")))
int crypto_sign_ed25519ph_final_create(crypto_sign_ed25519ph_state *state,
                                       unsigned char *sig,
                                       unsigned long long *siglen_p,
                                       const unsigned char *sk);

__attribute__ ((visibility ("default")))
int crypto_sign_ed25519ph_final_verify(crypto_sign_ed25519ph_state *state,
                                       unsigned char *sig,
                                       const unsigned char *pk)
            __attribute__ ((warn_unused_result));
# 14 "/usr/include/sodium/crypto_sign.h" 2
# 23 "/usr/include/sodium/crypto_sign.h"
typedef crypto_sign_ed25519ph_state crypto_sign_state;

__attribute__ ((visibility ("default")))
size_t crypto_sign_statebytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_sign_bytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_sign_seedbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_sign_publickeybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_sign_secretkeybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_sign_messagebytes_max(void);


__attribute__ ((visibility ("default")))
const char *crypto_sign_primitive(void);

__attribute__ ((visibility ("default")))
int crypto_sign_seed_keypair(unsigned char *pk, unsigned char *sk,
                             const unsigned char *seed);

__attribute__ ((visibility ("default")))
int crypto_sign_keypair(unsigned char *pk, unsigned char *sk);

__attribute__ ((visibility ("default")))
int crypto_sign(unsigned char *sm, unsigned long long *smlen_p,
                const unsigned char *m, unsigned long long mlen,
                const unsigned char *sk);

__attribute__ ((visibility ("default")))
int crypto_sign_open(unsigned char *m, unsigned long long *mlen_p,
                     const unsigned char *sm, unsigned long long smlen,
                     const unsigned char *pk)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_sign_detached(unsigned char *sig, unsigned long long *siglen_p,
                         const unsigned char *m, unsigned long long mlen,
                         const unsigned char *sk);

__attribute__ ((visibility ("default")))
int crypto_sign_verify_detached(const unsigned char *sig,
                                const unsigned char *m,
                                unsigned long long mlen,
                                const unsigned char *pk)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_sign_init(crypto_sign_state *state);

__attribute__ ((visibility ("default")))
int crypto_sign_update(crypto_sign_state *state,
                       const unsigned char *m, unsigned long long mlen);

__attribute__ ((visibility ("default")))
int crypto_sign_final_create(crypto_sign_state *state, unsigned char *sig,
                             unsigned long long *siglen_p,
                             const unsigned char *sk);

__attribute__ ((visibility ("default")))
int crypto_sign_final_verify(crypto_sign_state *state, unsigned char *sig,
                             const unsigned char *pk)
            __attribute__ ((warn_unused_result));
# 42 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_sign_ed25519.h" 1
# 43 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_stream.h" 1
# 12 "/usr/include/sodium/crypto_stream.h"
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 13 "/usr/include/sodium/crypto_stream.h" 2
# 25 "/usr/include/sodium/crypto_stream.h"
__attribute__ ((visibility ("default")))
size_t crypto_stream_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_stream_noncebytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_stream_messagebytes_max(void);


__attribute__ ((visibility ("default")))
const char *crypto_stream_primitive(void);

__attribute__ ((visibility ("default")))
int crypto_stream(unsigned char *c, unsigned long long clen,
                  const unsigned char *n, const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_stream_xor(unsigned char *c, const unsigned char *m,
                      unsigned long long mlen, const unsigned char *n,
                      const unsigned char *k);

__attribute__ ((visibility ("default")))
void crypto_stream_keygen(unsigned char k[32U]);
# 44 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_stream_chacha20.h" 1
# 45 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_stream_salsa20.h" 1
# 12 "/usr/include/sodium/crypto_stream_salsa20.h"
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 13 "/usr/include/sodium/crypto_stream_salsa20.h" 2
# 24 "/usr/include/sodium/crypto_stream_salsa20.h"
__attribute__ ((visibility ("default")))
size_t crypto_stream_salsa20_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_stream_salsa20_noncebytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_stream_salsa20_messagebytes_max(void);

__attribute__ ((visibility ("default")))
int crypto_stream_salsa20(unsigned char *c, unsigned long long clen,
                          const unsigned char *n, const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_stream_salsa20_xor(unsigned char *c, const unsigned char *m,
                              unsigned long long mlen, const unsigned char *n,
                              const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_stream_salsa20_xor_ic(unsigned char *c, const unsigned char *m,
                                 unsigned long long mlen,
                                 const unsigned char *n, uint64_t ic,
                                 const unsigned char *k);

__attribute__ ((visibility ("default")))
void crypto_stream_salsa20_keygen(unsigned char k[32U]);
# 46 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_stream_xsalsa20.h" 1
# 47 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_verify_16.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_verify_16.h" 2







__attribute__ ((visibility ("default")))
size_t crypto_verify_16_bytes(void);

__attribute__ ((visibility ("default")))
int crypto_verify_16(const unsigned char *x, const unsigned char *y)
            __attribute__ ((warn_unused_result));
# 48 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_verify_32.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_verify_32.h" 2







__attribute__ ((visibility ("default")))
size_t crypto_verify_32_bytes(void);

__attribute__ ((visibility ("default")))
int crypto_verify_32(const unsigned char *x, const unsigned char *y)
            __attribute__ ((warn_unused_result));
# 49 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_verify_64.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_verify_64.h" 2







__attribute__ ((visibility ("default")))
size_t crypto_verify_64_bytes(void);

__attribute__ ((visibility ("default")))
int crypto_verify_64(const unsigned char *x, const unsigned char *y)
            __attribute__ ((warn_unused_result));
# 50 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/randombytes.h" 1




# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 6 "/usr/include/sodium/randombytes.h" 2
# 19 "/usr/include/sodium/randombytes.h"
typedef struct randombytes_implementation {
    const char *(*implementation_name)(void);
    uint32_t (*random)(void);
    void (*stir)(void);
    uint32_t (*uniform)(const uint32_t upper_bound);
    void (*buf)(void * const buf, const size_t size);
    int (*close)(void);
} randombytes_implementation;




__attribute__ ((visibility ("default")))
size_t randombytes_seedbytes(void);

__attribute__ ((visibility ("default")))
void randombytes_buf(void * const buf, const size_t size);

__attribute__ ((visibility ("default")))
void randombytes_buf_deterministic(void * const buf, const size_t size,
                                   const unsigned char seed[32U]);

__attribute__ ((visibility ("default")))
uint32_t randombytes_random(void);

__attribute__ ((visibility ("default")))
uint32_t randombytes_uniform(const uint32_t upper_bound);

__attribute__ ((visibility ("default")))
void randombytes_stir(void);

__attribute__ ((visibility ("default")))
int randombytes_close(void);

__attribute__ ((visibility ("default")))
int randombytes_set_implementation(randombytes_implementation *impl);

__attribute__ ((visibility ("default")))
const char *randombytes_implementation_name(void);



__attribute__ ((visibility ("default")))
void randombytes(unsigned char * const buf, const unsigned long long buf_len);
# 51 "/usr/include/sodium.h" 2



# 1 "/usr/include/sodium/randombytes_salsa20_random.h" 1





# 1 "/usr/include/sodium/randombytes.h" 1
# 7 "/usr/include/sodium/randombytes_salsa20_random.h" 2





__attribute__ ((visibility ("default")))
extern struct randombytes_implementation randombytes_salsa20_implementation;
# 55 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/randombytes_sysrandom.h" 1
# 12 "/usr/include/sodium/randombytes_sysrandom.h"
__attribute__ ((visibility ("default")))
extern struct randombytes_implementation randombytes_sysrandom_implementation;
# 56 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/runtime.h" 1
# 11 "/usr/include/sodium/runtime.h"
__attribute__ ((visibility ("default"))) __attribute__((weak))
int sodium_runtime_has_neon(void);

__attribute__ ((visibility ("default"))) __attribute__((weak))
int sodium_runtime_has_sse2(void);

__attribute__ ((visibility ("default"))) __attribute__((weak))
int sodium_runtime_has_sse3(void);

__attribute__ ((visibility ("default"))) __attribute__((weak))
int sodium_runtime_has_ssse3(void);

__attribute__ ((visibility ("default"))) __attribute__((weak))
int sodium_runtime_has_sse41(void);

__attribute__ ((visibility ("default"))) __attribute__((weak))
int sodium_runtime_has_avx(void);

__attribute__ ((visibility ("default"))) __attribute__((weak))
int sodium_runtime_has_avx2(void);

__attribute__ ((visibility ("default"))) __attribute__((weak))
int sodium_runtime_has_avx512f(void);

__attribute__ ((visibility ("default"))) __attribute__((weak))
int sodium_runtime_has_pclmul(void);

__attribute__ ((visibility ("default"))) __attribute__((weak))
int sodium_runtime_has_aesni(void);

__attribute__ ((visibility ("default"))) __attribute__((weak))
int sodium_runtime_has_rdrand(void);



int _sodium_runtime_get_cpu_features(void);
# 57 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/utils.h" 1




# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 6 "/usr/include/sodium/utils.h" 2
# 21 "/usr/include/sodium/utils.h"
__attribute__ ((visibility ("default")))
void sodium_memzero(void * const pnt, const size_t len);

__attribute__ ((visibility ("default")))
void sodium_stackzero(const size_t len);







__attribute__ ((visibility ("default")))
int sodium_memcmp(const void * const b1_, const void * const b2_, size_t len)
            __attribute__ ((warn_unused_result));







__attribute__ ((visibility ("default")))
int sodium_compare(const unsigned char *b1_, const unsigned char *b2_,
                   size_t len)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int sodium_is_zero(const unsigned char *n, const size_t nlen);

__attribute__ ((visibility ("default")))
void sodium_increment(unsigned char *n, const size_t nlen);

__attribute__ ((visibility ("default")))
void sodium_add(unsigned char *a, const unsigned char *b, const size_t len);

__attribute__ ((visibility ("default")))
char *sodium_bin2hex(char * const hex, const size_t hex_maxlen,
                     const unsigned char * const bin, const size_t bin_len);

__attribute__ ((visibility ("default")))
int sodium_hex2bin(unsigned char * const bin, const size_t bin_maxlen,
                   const char * const hex, const size_t hex_len,
                   const char * const ignore, size_t * const bin_len,
                   const char ** const hex_end);
# 81 "/usr/include/sodium/utils.h"
__attribute__ ((visibility ("default")))
size_t sodium_base64_encoded_len(const size_t bin_len, const int variant);

__attribute__ ((visibility ("default")))
char *sodium_bin2base64(char * const b64, const size_t b64_maxlen,
                        const unsigned char * const bin, const size_t bin_len,
                        const int variant);

__attribute__ ((visibility ("default")))
int sodium_base642bin(unsigned char * const bin, const size_t bin_maxlen,
                      const char * const b64, const size_t b64_len,
                      const char * const ignore, size_t * const bin_len,
                      const char ** const b64_end, const int variant);

__attribute__ ((visibility ("default")))
int sodium_mlock(void * const addr, const size_t len);

__attribute__ ((visibility ("default")))
int sodium_munlock(void * const addr, const size_t len);
# 134 "/usr/include/sodium/utils.h"
__attribute__ ((visibility ("default")))
void *sodium_malloc(const size_t size)
            __attribute__ ((malloc));

__attribute__ ((visibility ("default")))
void *sodium_allocarray(size_t count, size_t size)
            __attribute__ ((malloc));

__attribute__ ((visibility ("default")))
void sodium_free(void *ptr);

__attribute__ ((visibility ("default")))
int sodium_mprotect_noaccess(void *ptr);

__attribute__ ((visibility ("default")))
int sodium_mprotect_readonly(void *ptr);

__attribute__ ((visibility ("default")))
int sodium_mprotect_readwrite(void *ptr);

__attribute__ ((visibility ("default")))
int sodium_pad(size_t *padded_buflen_p, unsigned char *buf,
               size_t unpadded_buflen, size_t blocksize, size_t max_buflen);

__attribute__ ((visibility ("default")))
int sodium_unpad(size_t *unpadded_buflen_p, const unsigned char *buf,
                 size_t padded_buflen, size_t blocksize);



int _sodium_alloc_init(void);
# 58 "/usr/include/sodium.h" 2


# 1 "/usr/include/sodium/crypto_box_curve25519xchacha20poly1305.h" 1




# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 6 "/usr/include/sodium/crypto_box_curve25519xchacha20poly1305.h" 2
# 1 "/usr/include/sodium/crypto_stream_xchacha20.h" 1
# 12 "/usr/include/sodium/crypto_stream_xchacha20.h"
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 13 "/usr/include/sodium/crypto_stream_xchacha20.h" 2
# 24 "/usr/include/sodium/crypto_stream_xchacha20.h"
__attribute__ ((visibility ("default")))
size_t crypto_stream_xchacha20_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_stream_xchacha20_noncebytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_stream_xchacha20_messagebytes_max(void);

__attribute__ ((visibility ("default")))
int crypto_stream_xchacha20(unsigned char *c, unsigned long long clen,
                            const unsigned char *n, const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_stream_xchacha20_xor(unsigned char *c, const unsigned char *m,
                                unsigned long long mlen, const unsigned char *n,
                                const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_stream_xchacha20_xor_ic(unsigned char *c, const unsigned char *m,
                                   unsigned long long mlen,
                                   const unsigned char *n, uint64_t ic,
                                   const unsigned char *k);

__attribute__ ((visibility ("default")))
void crypto_stream_xchacha20_keygen(unsigned char k[32U]);
# 7 "/usr/include/sodium/crypto_box_curve25519xchacha20poly1305.h" 2
# 17 "/usr/include/sodium/crypto_box_curve25519xchacha20poly1305.h"
__attribute__ ((visibility ("default")))
size_t crypto_box_curve25519xchacha20poly1305_seedbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_box_curve25519xchacha20poly1305_publickeybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_box_curve25519xchacha20poly1305_secretkeybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_box_curve25519xchacha20poly1305_beforenmbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_box_curve25519xchacha20poly1305_noncebytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_box_curve25519xchacha20poly1305_macbytes(void);



__attribute__ ((visibility ("default")))
size_t crypto_box_curve25519xchacha20poly1305_messagebytes_max(void);

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xchacha20poly1305_seed_keypair(unsigned char *pk,
                                                        unsigned char *sk,
                                                        const unsigned char *seed);

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xchacha20poly1305_keypair(unsigned char *pk,
                                                   unsigned char *sk);

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xchacha20poly1305_easy(unsigned char *c,
                                                const unsigned char *m,
                                                unsigned long long mlen,
                                                const unsigned char *n,
                                                const unsigned char *pk,
                                                const unsigned char *sk)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xchacha20poly1305_open_easy(unsigned char *m,
                                                     const unsigned char *c,
                                                     unsigned long long clen,
                                                     const unsigned char *n,
                                                     const unsigned char *pk,
                                                     const unsigned char *sk)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xchacha20poly1305_detached(unsigned char *c,
                                                    unsigned char *mac,
                                                    const unsigned char *m,
                                                    unsigned long long mlen,
                                                    const unsigned char *n,
                                                    const unsigned char *pk,
                                                    const unsigned char *sk)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xchacha20poly1305_open_detached(unsigned char *m,
                                                         const unsigned char *c,
                                                         const unsigned char *mac,
                                                         unsigned long long clen,
                                                         const unsigned char *n,
                                                         const unsigned char *pk,
                                                         const unsigned char *sk)
            __attribute__ ((warn_unused_result));



__attribute__ ((visibility ("default")))
int crypto_box_curve25519xchacha20poly1305_beforenm(unsigned char *k,
                                                    const unsigned char *pk,
                                                    const unsigned char *sk)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xchacha20poly1305_easy_afternm(unsigned char *c,
                                                        const unsigned char *m,
                                                        unsigned long long mlen,
                                                        const unsigned char *n,
                                                        const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xchacha20poly1305_open_easy_afternm(unsigned char *m,
                                                             const unsigned char *c,
                                                             unsigned long long clen,
                                                             const unsigned char *n,
                                                             const unsigned char *k)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xchacha20poly1305_detached_afternm(unsigned char *c,
                                                            unsigned char *mac,
                                                            const unsigned char *m,
                                                            unsigned long long mlen,
                                                            const unsigned char *n,
                                                            const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xchacha20poly1305_open_detached_afternm(unsigned char *m,
                                                                 const unsigned char *c,
                                                                 const unsigned char *mac,
                                                                 unsigned long long clen,
                                                                 const unsigned char *n,
                                                                 const unsigned char *k)
            __attribute__ ((warn_unused_result));







__attribute__ ((visibility ("default")))
size_t crypto_box_curve25519xchacha20poly1305_sealbytes(void);

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xchacha20poly1305_seal(unsigned char *c,
                                                const unsigned char *m,
                                                unsigned long long mlen,
                                                const unsigned char *pk);

__attribute__ ((visibility ("default")))
int crypto_box_curve25519xchacha20poly1305_seal_open(unsigned char *m,
                                                     const unsigned char *c,
                                                     unsigned long long clen,
                                                     const unsigned char *pk,
                                                     const unsigned char *sk)
            __attribute__ ((warn_unused_result));
# 61 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_core_ed25519.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_core_ed25519.h" 2







__attribute__ ((visibility ("default")))
size_t crypto_core_ed25519_bytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_core_ed25519_uniformbytes(void);

__attribute__ ((visibility ("default")))
int crypto_core_ed25519_is_valid_point(const unsigned char *p);

__attribute__ ((visibility ("default")))
int crypto_core_ed25519_add(unsigned char *r,
                            const unsigned char *p, const unsigned char *q);

__attribute__ ((visibility ("default")))
int crypto_core_ed25519_sub(unsigned char *r,
                            const unsigned char *p, const unsigned char *q);

__attribute__ ((visibility ("default")))
int crypto_core_ed25519_from_uniform(unsigned char *p, const unsigned char *r);
# 62 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_scalarmult_ed25519.h" 1




# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 6 "/usr/include/sodium/crypto_scalarmult_ed25519.h" 2
# 14 "/usr/include/sodium/crypto_scalarmult_ed25519.h"
__attribute__ ((visibility ("default")))
size_t crypto_scalarmult_ed25519_bytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_scalarmult_ed25519_scalarbytes(void);
# 29 "/usr/include/sodium/crypto_scalarmult_ed25519.h"
__attribute__ ((visibility ("default")))
int crypto_scalarmult_ed25519(unsigned char *q, const unsigned char *n,
                              const unsigned char *p)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_scalarmult_ed25519_base(unsigned char *q, const unsigned char *n);
# 63 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_secretbox_xchacha20poly1305.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 5 "/usr/include/sodium/crypto_secretbox_xchacha20poly1305.h" 2
# 16 "/usr/include/sodium/crypto_secretbox_xchacha20poly1305.h"
__attribute__ ((visibility ("default")))
size_t crypto_secretbox_xchacha20poly1305_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_secretbox_xchacha20poly1305_noncebytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_secretbox_xchacha20poly1305_macbytes(void);



__attribute__ ((visibility ("default")))
size_t crypto_secretbox_xchacha20poly1305_messagebytes_max(void);

__attribute__ ((visibility ("default")))
int crypto_secretbox_xchacha20poly1305_easy(unsigned char *c,
                                            const unsigned char *m,
                                            unsigned long long mlen,
                                            const unsigned char *n,
                                            const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_secretbox_xchacha20poly1305_open_easy(unsigned char *m,
                                                 const unsigned char *c,
                                                 unsigned long long clen,
                                                 const unsigned char *n,
                                                 const unsigned char *k)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_secretbox_xchacha20poly1305_detached(unsigned char *c,
                                                unsigned char *mac,
                                                const unsigned char *m,
                                                unsigned long long mlen,
                                                const unsigned char *n,
                                                const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_secretbox_xchacha20poly1305_open_detached(unsigned char *m,
                                                     const unsigned char *c,
                                                     const unsigned char *mac,
                                                     unsigned long long clen,
                                                     const unsigned char *n,
                                                     const unsigned char *k)
            __attribute__ ((warn_unused_result));
# 64 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_pwhash_scryptsalsa208sha256.h" 1



# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include-fixed/limits.h" 1 3 4
# 5 "/usr/include/sodium/crypto_pwhash_scryptsalsa208sha256.h" 2
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 6 "/usr/include/sodium/crypto_pwhash_scryptsalsa208sha256.h" 2
# 18 "/usr/include/sodium/crypto_pwhash_scryptsalsa208sha256.h"
__attribute__ ((visibility ("default")))
size_t crypto_pwhash_scryptsalsa208sha256_bytes_min(void);



__attribute__ ((visibility ("default")))
size_t crypto_pwhash_scryptsalsa208sha256_bytes_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_scryptsalsa208sha256_passwd_min(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_scryptsalsa208sha256_passwd_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_scryptsalsa208sha256_saltbytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_scryptsalsa208sha256_strbytes(void);


__attribute__ ((visibility ("default")))
const char *crypto_pwhash_scryptsalsa208sha256_strprefix(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_scryptsalsa208sha256_opslimit_min(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_scryptsalsa208sha256_opslimit_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_scryptsalsa208sha256_memlimit_min(void);



__attribute__ ((visibility ("default")))
size_t crypto_pwhash_scryptsalsa208sha256_memlimit_max(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_scryptsalsa208sha256_opslimit_interactive(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_scryptsalsa208sha256_memlimit_interactive(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_scryptsalsa208sha256_opslimit_sensitive(void);


__attribute__ ((visibility ("default")))
size_t crypto_pwhash_scryptsalsa208sha256_memlimit_sensitive(void);

__attribute__ ((visibility ("default")))
int crypto_pwhash_scryptsalsa208sha256(unsigned char * const out,
                                       unsigned long long outlen,
                                       const char * const passwd,
                                       unsigned long long passwdlen,
                                       const unsigned char * const salt,
                                       unsigned long long opslimit,
                                       size_t memlimit)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_pwhash_scryptsalsa208sha256_str(char out[102U],
                                           const char * const passwd,
                                           unsigned long long passwdlen,
                                           unsigned long long opslimit,
                                           size_t memlimit)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_pwhash_scryptsalsa208sha256_str_verify(const char str[102U],
                                                  const char * const passwd,
                                                  unsigned long long passwdlen)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_pwhash_scryptsalsa208sha256_ll(const uint8_t * passwd, size_t passwdlen,
                                          const uint8_t * salt, size_t saltlen,
                                          uint64_t N, uint32_t r, uint32_t p,
                                          uint8_t * buf, size_t buflen)
            __attribute__ ((warn_unused_result));

__attribute__ ((visibility ("default")))
int crypto_pwhash_scryptsalsa208sha256_str_needs_rehash(const char str[102U],
                                                        unsigned long long opslimit,
                                                        size_t memlimit)
            __attribute__ ((warn_unused_result));
# 65 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_stream_salsa2012.h" 1
# 12 "/usr/include/sodium/crypto_stream_salsa2012.h"
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 13 "/usr/include/sodium/crypto_stream_salsa2012.h" 2
# 23 "/usr/include/sodium/crypto_stream_salsa2012.h"
__attribute__ ((visibility ("default")))
size_t crypto_stream_salsa2012_keybytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_stream_salsa2012_noncebytes(void);


__attribute__ ((visibility ("default")))
size_t crypto_stream_salsa2012_messagebytes_max(void);

__attribute__ ((visibility ("default")))
int crypto_stream_salsa2012(unsigned char *c, unsigned long long clen,
                            const unsigned char *n, const unsigned char *k);

__attribute__ ((visibility ("default")))
int crypto_stream_salsa2012_xor(unsigned char *c, const unsigned char *m,
                                unsigned long long mlen, const unsigned char *n,
                                const unsigned char *k);

__attribute__ ((visibility ("default")))
void crypto_stream_salsa2012_keygen(unsigned char k[32U]);
# 66 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_stream_salsa208.h" 1
# 12 "/usr/include/sodium/crypto_stream_salsa208.h"
# 1 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h" 1 3 4
# 13 "/usr/include/sodium/crypto_stream_salsa208.h" 2
# 23 "/usr/include/sodium/crypto_stream_salsa208.h"
__attribute__ ((visibility ("default")))
size_t crypto_stream_salsa208_keybytes(void)
            __attribute__ ((deprecated));


__attribute__ ((visibility ("default")))
size_t crypto_stream_salsa208_noncebytes(void)
            __attribute__ ((deprecated));


    __attribute__ ((visibility ("default")))
size_t crypto_stream_salsa208_messagebytes_max(void)
            __attribute__ ((deprecated));

__attribute__ ((visibility ("default")))
int crypto_stream_salsa208(unsigned char *c, unsigned long long clen,
                           const unsigned char *n, const unsigned char *k)
            __attribute__ ((deprecated));

__attribute__ ((visibility ("default")))
int crypto_stream_salsa208_xor(unsigned char *c, const unsigned char *m,
                               unsigned long long mlen, const unsigned char *n,
                               const unsigned char *k)
            __attribute__ ((deprecated));

__attribute__ ((visibility ("default")))
void crypto_stream_salsa208_keygen(unsigned char k[32U])
            __attribute__ ((deprecated));
# 67 "/usr/include/sodium.h" 2
# 1 "/usr/include/sodium/crypto_stream_xchacha20.h" 1
# 68 "/usr/include/sodium.h" 2
