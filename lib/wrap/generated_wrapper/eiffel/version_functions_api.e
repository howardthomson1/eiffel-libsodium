note

	description: "This file has been generated by EWG. Do not edit. Changes will be lost!"

	generator: "Eiffel Wrapper Generator"
-- functions wrapper
class VERSION_FUNCTIONS_API


feature -- Access

	sodium_version_string: POINTER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return sodium_version_string ();
			]"
		end

	sodium_library_version_major: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return sodium_library_version_major ();
			]"
		end

	sodium_library_version_minor: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return sodium_library_version_minor ();
			]"
		end

	sodium_library_minimal: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return sodium_library_minimal ();
			]"
		end

feature -- Externals

feature -- Externals Address

end
