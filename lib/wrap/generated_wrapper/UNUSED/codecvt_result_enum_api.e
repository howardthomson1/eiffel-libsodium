-- enum wrapper
class CODECVT_RESULT_ENUM_API

feature {ANY}

	is_valid_enum (a_value: INTEGER): BOOLEAN 
			-- Is `a_value' a valid integer code for this enum ?
		do
			Result := a_value = codecvt_ok or a_value = codecvt_partial or a_value = codecvt_error or a_value = codecvt_noconv
		end

	codecvt_ok: INTEGER 
		external
			"C inline use <sodium.h>"
		alias
			"__codecvt_ok"
		end

	codecvt_partial: INTEGER 
		external
			"C inline use <sodium.h>"
		alias
			"__codecvt_partial"
		end

	codecvt_error: INTEGER 
		external
			"C inline use <sodium.h>"
		alias
			"__codecvt_error"
		end

	codecvt_noconv: INTEGER 
		external
			"C inline use <sodium.h>"
		alias
			"__codecvt_noconv"
		end

end
