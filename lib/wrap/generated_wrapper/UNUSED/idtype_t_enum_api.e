-- enum wrapper
class IDTYPE_T_ENUM_API

feature {ANY}

	is_valid_enum (a_value: INTEGER): BOOLEAN 
			-- Is `a_value' a valid integer code for this enum ?
		do
			Result := a_value = p_all or a_value = p_pid or a_value = p_pgid
		end

	p_all: INTEGER 
		external
			"C inline use <sodium.h>"
		alias
			"P_ALL"
		end

	p_pid: INTEGER 
		external
			"C inline use <sodium.h>"
		alias
			"P_PID"
		end

	p_pgid: INTEGER 
		external
			"C inline use <sodium.h>"
		alias
			"P_PGID"
		end

end
