note

	description: "This file has been generated by EWG. Do not edit. Changes will be lost!"

	generator: "Eiffel Wrapper Generator"

class RANDOM_DATA_STRUCT_API

inherit

	MEMORY_STRUCTURE

	
create

	make,
	make_by_pointer

feature -- Measurement

	structure_size: INTEGER 
		do
			Result := sizeof_external
		end

feature {ANY} -- Member Access

	fptr: POINTER
			-- Access member `fptr`
		require
			exists: exists
		do
			Result := c_fptr (item)
		ensure
			result_correct: Result = c_fptr (item)
		end

	set_fptr (a_value: POINTER) 
			-- Change the value of member `fptr` to `a_value`.
		require
			exists: exists
		do
			set_c_fptr (item, a_value)
		ensure
			fptr_set: a_value = fptr
		end

	rptr: POINTER
			-- Access member `rptr`
		require
			exists: exists
		do
			Result := c_rptr (item)
		ensure
			result_correct: Result = c_rptr (item)
		end

	set_rptr (a_value: POINTER) 
			-- Change the value of member `rptr` to `a_value`.
		require
			exists: exists
		do
			set_c_rptr (item, a_value)
		ensure
			rptr_set: a_value = rptr
		end

	state: POINTER
			-- Access member `state`
		require
			exists: exists
		do
			Result := c_state (item)
		ensure
			result_correct: Result = c_state (item)
		end

	set_state (a_value: POINTER) 
			-- Change the value of member `state` to `a_value`.
		require
			exists: exists
		do
			set_c_state (item, a_value)
		ensure
			state_set: a_value = state
		end

	rand_type: INTEGER
			-- Access member `rand_type`
		require
			exists: exists
		do
			Result := c_rand_type (item)
		ensure
			result_correct: Result = c_rand_type (item)
		end

	set_rand_type (a_value: INTEGER) 
			-- Change the value of member `rand_type` to `a_value`.
		require
			exists: exists
		do
			set_c_rand_type (item, a_value)
		ensure
			rand_type_set: a_value = rand_type
		end

	rand_deg: INTEGER
			-- Access member `rand_deg`
		require
			exists: exists
		do
			Result := c_rand_deg (item)
		ensure
			result_correct: Result = c_rand_deg (item)
		end

	set_rand_deg (a_value: INTEGER) 
			-- Change the value of member `rand_deg` to `a_value`.
		require
			exists: exists
		do
			set_c_rand_deg (item, a_value)
		ensure
			rand_deg_set: a_value = rand_deg
		end

	rand_sep: INTEGER
			-- Access member `rand_sep`
		require
			exists: exists
		do
			Result := c_rand_sep (item)
		ensure
			result_correct: Result = c_rand_sep (item)
		end

	set_rand_sep (a_value: INTEGER) 
			-- Change the value of member `rand_sep` to `a_value`.
		require
			exists: exists
		do
			set_c_rand_sep (item, a_value)
		ensure
			rand_sep_set: a_value = rand_sep
		end

	end_ptr: POINTER
			-- Access member `end_ptr`
		require
			exists: exists
		do
			Result := c_end_ptr (item)
		ensure
			result_correct: Result = c_end_ptr (item)
		end

	set_end_ptr (a_value: POINTER) 
			-- Change the value of member `end_ptr` to `a_value`.
		require
			exists: exists
		do
			set_c_end_ptr (item, a_value)
		ensure
			end_ptr_set: a_value = end_ptr
		end

feature {NONE} -- Implementation wrapper for struct struct random_data

	sizeof_external: INTEGER 
		external
			"C inline use <sodium.h>"
		alias
			"sizeof(struct random_data)"
		end

	c_fptr (an_item: POINTER): POINTER
		require
			an_item_not_null: an_item /= default_pointer
		external
			"C inline use <sodium.h>"
		alias
			"[
				((struct random_data*)$an_item)->fptr
			]"
		end

	set_c_fptr (an_item: POINTER; a_value: POINTER) 
		require
			an_item_not_null: an_item /= default_pointer
		external
			"C inline use <sodium.h>"
		alias
			"[
				((struct random_data*)$an_item)->fptr =  (int32_t*)$a_value
			]"
		ensure
			fptr_set: a_value = c_fptr (an_item)
		end

	c_rptr (an_item: POINTER): POINTER
		require
			an_item_not_null: an_item /= default_pointer
		external
			"C inline use <sodium.h>"
		alias
			"[
				((struct random_data*)$an_item)->rptr
			]"
		end

	set_c_rptr (an_item: POINTER; a_value: POINTER) 
		require
			an_item_not_null: an_item /= default_pointer
		external
			"C inline use <sodium.h>"
		alias
			"[
				((struct random_data*)$an_item)->rptr =  (int32_t*)$a_value
			]"
		ensure
			rptr_set: a_value = c_rptr (an_item)
		end

	c_state (an_item: POINTER): POINTER
		require
			an_item_not_null: an_item /= default_pointer
		external
			"C inline use <sodium.h>"
		alias
			"[
				((struct random_data*)$an_item)->state
			]"
		end

	set_c_state (an_item: POINTER; a_value: POINTER) 
		require
			an_item_not_null: an_item /= default_pointer
		external
			"C inline use <sodium.h>"
		alias
			"[
				((struct random_data*)$an_item)->state =  (int32_t*)$a_value
			]"
		ensure
			state_set: a_value = c_state (an_item)
		end

	c_rand_type (an_item: POINTER): INTEGER
		require
			an_item_not_null: an_item /= default_pointer
		external
			"C inline use <sodium.h>"
		alias
			"[
				((struct random_data*)$an_item)->rand_type
			]"
		end

	set_c_rand_type (an_item: POINTER; a_value: INTEGER) 
		require
			an_item_not_null: an_item /= default_pointer
		external
			"C inline use <sodium.h>"
		alias
			"[
				((struct random_data*)$an_item)->rand_type =  (int)$a_value
			]"
		ensure
			rand_type_set: a_value = c_rand_type (an_item)
		end

	c_rand_deg (an_item: POINTER): INTEGER
		require
			an_item_not_null: an_item /= default_pointer
		external
			"C inline use <sodium.h>"
		alias
			"[
				((struct random_data*)$an_item)->rand_deg
			]"
		end

	set_c_rand_deg (an_item: POINTER; a_value: INTEGER) 
		require
			an_item_not_null: an_item /= default_pointer
		external
			"C inline use <sodium.h>"
		alias
			"[
				((struct random_data*)$an_item)->rand_deg =  (int)$a_value
			]"
		ensure
			rand_deg_set: a_value = c_rand_deg (an_item)
		end

	c_rand_sep (an_item: POINTER): INTEGER
		require
			an_item_not_null: an_item /= default_pointer
		external
			"C inline use <sodium.h>"
		alias
			"[
				((struct random_data*)$an_item)->rand_sep
			]"
		end

	set_c_rand_sep (an_item: POINTER; a_value: INTEGER) 
		require
			an_item_not_null: an_item /= default_pointer
		external
			"C inline use <sodium.h>"
		alias
			"[
				((struct random_data*)$an_item)->rand_sep =  (int)$a_value
			]"
		ensure
			rand_sep_set: a_value = c_rand_sep (an_item)
		end

	c_end_ptr (an_item: POINTER): POINTER
		require
			an_item_not_null: an_item /= default_pointer
		external
			"C inline use <sodium.h>"
		alias
			"[
				((struct random_data*)$an_item)->end_ptr
			]"
		end

	set_c_end_ptr (an_item: POINTER; a_value: POINTER) 
		require
			an_item_not_null: an_item /= default_pointer
		external
			"C inline use <sodium.h>"
		alias
			"[
				((struct random_data*)$an_item)->end_ptr =  (int32_t*)$a_value
			]"
		ensure
			end_ptr_set: a_value = c_end_ptr (an_item)
		end

end
