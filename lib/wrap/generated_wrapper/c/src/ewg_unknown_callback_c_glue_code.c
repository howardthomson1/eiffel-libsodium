#include <ewg_eiffel.h>
#include <ewg_unknown_callback_c_glue_code.h>

#ifdef _MSC_VER
#pragma warning (disable:4715) // Not all control paths return a value
#endif
void* void_void_anonymous_callback_object =  NULL;
void_void_anonymous_callback_eiffel_feature void_void_anonymous_callback_address_1 = NULL;
void_void_anonymous_callback_eiffel_feature void_void_anonymous_callback_address_2 = NULL;
void_void_anonymous_callback_eiffel_feature void_void_anonymous_callback_address_3 = NULL;

void set_void_void_anonymous_callback_object (void* a_object)
{
	if (a_object) {
		void_void_anonymous_callback_object = eif_protect(a_object);
	} else { 
		void_void_anonymous_callback_object = NULL;
	}
}

void release_void_void_anonymous_callback_object ()
{
	eif_wean (void_void_anonymous_callback_object);
}

void void_void_anonymous_callback_stub_1 ()
{
	if (void_void_anonymous_callback_object != NULL && void_void_anonymous_callback_address_1 != NULL)
	{
		void_void_anonymous_callback_address_1 (eif_access(void_void_anonymous_callback_object));
	}
}

void void_void_anonymous_callback_stub_2 ()
{
	if (void_void_anonymous_callback_object != NULL && void_void_anonymous_callback_address_2 != NULL)
	{
		void_void_anonymous_callback_address_2 (eif_access(void_void_anonymous_callback_object));
	}
}

void void_void_anonymous_callback_stub_3 ()
{
	if (void_void_anonymous_callback_object != NULL && void_void_anonymous_callback_address_3 != NULL)
	{
		void_void_anonymous_callback_address_3 (eif_access(void_void_anonymous_callback_object));
	}
}

void set_void_void_anonymous_callback_entry_1 (void* a_feature){
	void_void_anonymous_callback_address_1 = (void_void_anonymous_callback_eiffel_feature) a_feature;
}

void set_void_void_anonymous_callback_entry_2 (void* a_feature){
	void_void_anonymous_callback_address_2 = (void_void_anonymous_callback_eiffel_feature) a_feature;
}

void set_void_void_anonymous_callback_entry_3 (void* a_feature){
	void_void_anonymous_callback_address_3 = (void_void_anonymous_callback_eiffel_feature) a_feature;
}

void* get_void_void_anonymous_callback_stub_1 (){
	return (void*) void_void_anonymous_callback_stub_1;
}

void* get_void_void_anonymous_callback_stub_2 (){
	return (void*) void_void_anonymous_callback_stub_2;
}

void* get_void_void_anonymous_callback_stub_3 (){
	return (void*) void_void_anonymous_callback_stub_3;
}

void call_void_void_anonymous_callback (void *a_function)
{
	((void (*) ())a_function) ();
}

void* void_int_voidp_anonymous_callback_object =  NULL;
void_int_voidp_anonymous_callback_eiffel_feature void_int_voidp_anonymous_callback_address_1 = NULL;
void_int_voidp_anonymous_callback_eiffel_feature void_int_voidp_anonymous_callback_address_2 = NULL;
void_int_voidp_anonymous_callback_eiffel_feature void_int_voidp_anonymous_callback_address_3 = NULL;

void set_void_int_voidp_anonymous_callback_object (void* a_object)
{
	if (a_object) {
		void_int_voidp_anonymous_callback_object = eif_protect(a_object);
	} else { 
		void_int_voidp_anonymous_callback_object = NULL;
	}
}

void release_void_int_voidp_anonymous_callback_object ()
{
	eif_wean (void_int_voidp_anonymous_callback_object);
}

void void_int_voidp_anonymous_callback_stub_1 (int status, void *arg)
{
	if (void_int_voidp_anonymous_callback_object != NULL && void_int_voidp_anonymous_callback_address_1 != NULL)
	{
		void_int_voidp_anonymous_callback_address_1 (eif_access(void_int_voidp_anonymous_callback_object), status, arg);
	}
}

void void_int_voidp_anonymous_callback_stub_2 (int status, void *arg)
{
	if (void_int_voidp_anonymous_callback_object != NULL && void_int_voidp_anonymous_callback_address_2 != NULL)
	{
		void_int_voidp_anonymous_callback_address_2 (eif_access(void_int_voidp_anonymous_callback_object), status, arg);
	}
}

void void_int_voidp_anonymous_callback_stub_3 (int status, void *arg)
{
	if (void_int_voidp_anonymous_callback_object != NULL && void_int_voidp_anonymous_callback_address_3 != NULL)
	{
		void_int_voidp_anonymous_callback_address_3 (eif_access(void_int_voidp_anonymous_callback_object), status, arg);
	}
}

void set_void_int_voidp_anonymous_callback_entry_1 (void* a_feature){
	void_int_voidp_anonymous_callback_address_1 = (void_int_voidp_anonymous_callback_eiffel_feature) a_feature;
}

void set_void_int_voidp_anonymous_callback_entry_2 (void* a_feature){
	void_int_voidp_anonymous_callback_address_2 = (void_int_voidp_anonymous_callback_eiffel_feature) a_feature;
}

void set_void_int_voidp_anonymous_callback_entry_3 (void* a_feature){
	void_int_voidp_anonymous_callback_address_3 = (void_int_voidp_anonymous_callback_eiffel_feature) a_feature;
}

void* get_void_int_voidp_anonymous_callback_stub_1 (){
	return (void*) void_int_voidp_anonymous_callback_stub_1;
}

void* get_void_int_voidp_anonymous_callback_stub_2 (){
	return (void*) void_int_voidp_anonymous_callback_stub_2;
}

void* get_void_int_voidp_anonymous_callback_stub_3 (){
	return (void*) void_int_voidp_anonymous_callback_stub_3;
}

void call_void_int_voidp_anonymous_callback (void *a_function, int status, void *arg)
{
	((void (*) (int status, void *arg))a_function) (status, arg);
}

void* compar_fn_t_object =  NULL;
compar_fn_t_eiffel_feature compar_fn_t_address_1 = NULL;
compar_fn_t_eiffel_feature compar_fn_t_address_2 = NULL;
compar_fn_t_eiffel_feature compar_fn_t_address_3 = NULL;

void set_compar_fn_t_object (void* a_object)
{
	if (a_object) {
		compar_fn_t_object = eif_protect(a_object);
	} else { 
		compar_fn_t_object = NULL;
	}
}

void release_compar_fn_t_object ()
{
	eif_wean (compar_fn_t_object);
}

int compar_fn_t_stub_1 (void const *anonymous_1, void const *anonymous_2)
{
	if (compar_fn_t_object != NULL && compar_fn_t_address_1 != NULL)
	{
		return compar_fn_t_address_1 (eif_access(compar_fn_t_object), anonymous_1, anonymous_2);
	}
}

int compar_fn_t_stub_2 (void const *anonymous_1, void const *anonymous_2)
{
	if (compar_fn_t_object != NULL && compar_fn_t_address_2 != NULL)
	{
		return compar_fn_t_address_2 (eif_access(compar_fn_t_object), anonymous_1, anonymous_2);
	}
}

int compar_fn_t_stub_3 (void const *anonymous_1, void const *anonymous_2)
{
	if (compar_fn_t_object != NULL && compar_fn_t_address_3 != NULL)
	{
		return compar_fn_t_address_3 (eif_access(compar_fn_t_object), anonymous_1, anonymous_2);
	}
}

void set_compar_fn_t_entry_1 (void* a_feature){
	compar_fn_t_address_1 = (compar_fn_t_eiffel_feature) a_feature;
}

void set_compar_fn_t_entry_2 (void* a_feature){
	compar_fn_t_address_2 = (compar_fn_t_eiffel_feature) a_feature;
}

void set_compar_fn_t_entry_3 (void* a_feature){
	compar_fn_t_address_3 = (compar_fn_t_eiffel_feature) a_feature;
}

void* get_compar_fn_t_stub_1 (){
	return (void*) compar_fn_t_stub_1;
}

void* get_compar_fn_t_stub_2 (){
	return (void*) compar_fn_t_stub_2;
}

void* get_compar_fn_t_stub_3 (){
	return (void*) compar_fn_t_stub_3;
}

int call_compar_fn_t (void *a_function, void const *anonymous_1, void const *anonymous_2)
{
	return ((int (*) (void const *anonymous_1, void const *anonymous_2))a_function) (anonymous_1, anonymous_2);
}

void* charconstp_void_anonymous_callback_object =  NULL;
charconstp_void_anonymous_callback_eiffel_feature charconstp_void_anonymous_callback_address_1 = NULL;
charconstp_void_anonymous_callback_eiffel_feature charconstp_void_anonymous_callback_address_2 = NULL;
charconstp_void_anonymous_callback_eiffel_feature charconstp_void_anonymous_callback_address_3 = NULL;

void set_charconstp_void_anonymous_callback_object (void* a_object)
{
	if (a_object) {
		charconstp_void_anonymous_callback_object = eif_protect(a_object);
	} else { 
		charconstp_void_anonymous_callback_object = NULL;
	}
}

void release_charconstp_void_anonymous_callback_object ()
{
	eif_wean (charconstp_void_anonymous_callback_object);
}

char const *charconstp_void_anonymous_callback_stub_1 ()
{
	if (charconstp_void_anonymous_callback_object != NULL && charconstp_void_anonymous_callback_address_1 != NULL)
	{
		return charconstp_void_anonymous_callback_address_1 (eif_access(charconstp_void_anonymous_callback_object));
	}
}

char const *charconstp_void_anonymous_callback_stub_2 ()
{
	if (charconstp_void_anonymous_callback_object != NULL && charconstp_void_anonymous_callback_address_2 != NULL)
	{
		return charconstp_void_anonymous_callback_address_2 (eif_access(charconstp_void_anonymous_callback_object));
	}
}

char const *charconstp_void_anonymous_callback_stub_3 ()
{
	if (charconstp_void_anonymous_callback_object != NULL && charconstp_void_anonymous_callback_address_3 != NULL)
	{
		return charconstp_void_anonymous_callback_address_3 (eif_access(charconstp_void_anonymous_callback_object));
	}
}

void set_charconstp_void_anonymous_callback_entry_1 (void* a_feature){
	charconstp_void_anonymous_callback_address_1 = (charconstp_void_anonymous_callback_eiffel_feature) a_feature;
}

void set_charconstp_void_anonymous_callback_entry_2 (void* a_feature){
	charconstp_void_anonymous_callback_address_2 = (charconstp_void_anonymous_callback_eiffel_feature) a_feature;
}

void set_charconstp_void_anonymous_callback_entry_3 (void* a_feature){
	charconstp_void_anonymous_callback_address_3 = (charconstp_void_anonymous_callback_eiffel_feature) a_feature;
}

void* get_charconstp_void_anonymous_callback_stub_1 (){
	return (void*) charconstp_void_anonymous_callback_stub_1;
}

void* get_charconstp_void_anonymous_callback_stub_2 (){
	return (void*) charconstp_void_anonymous_callback_stub_2;
}

void* get_charconstp_void_anonymous_callback_stub_3 (){
	return (void*) charconstp_void_anonymous_callback_stub_3;
}

char const *call_charconstp_void_anonymous_callback (void *a_function)
{
	return ((char const *(*) ())a_function) ();
}

void* uint32_t_void_anonymous_callback_object =  NULL;
uint32_t_void_anonymous_callback_eiffel_feature uint32_t_void_anonymous_callback_address_1 = NULL;
uint32_t_void_anonymous_callback_eiffel_feature uint32_t_void_anonymous_callback_address_2 = NULL;
uint32_t_void_anonymous_callback_eiffel_feature uint32_t_void_anonymous_callback_address_3 = NULL;

void set_uint32_t_void_anonymous_callback_object (void* a_object)
{
	if (a_object) {
		uint32_t_void_anonymous_callback_object = eif_protect(a_object);
	} else { 
		uint32_t_void_anonymous_callback_object = NULL;
	}
}

void release_uint32_t_void_anonymous_callback_object ()
{
	eif_wean (uint32_t_void_anonymous_callback_object);
}

uint32_t uint32_t_void_anonymous_callback_stub_1 ()
{
	if (uint32_t_void_anonymous_callback_object != NULL && uint32_t_void_anonymous_callback_address_1 != NULL)
	{
		return uint32_t_void_anonymous_callback_address_1 (eif_access(uint32_t_void_anonymous_callback_object));
	}
}

uint32_t uint32_t_void_anonymous_callback_stub_2 ()
{
	if (uint32_t_void_anonymous_callback_object != NULL && uint32_t_void_anonymous_callback_address_2 != NULL)
	{
		return uint32_t_void_anonymous_callback_address_2 (eif_access(uint32_t_void_anonymous_callback_object));
	}
}

uint32_t uint32_t_void_anonymous_callback_stub_3 ()
{
	if (uint32_t_void_anonymous_callback_object != NULL && uint32_t_void_anonymous_callback_address_3 != NULL)
	{
		return uint32_t_void_anonymous_callback_address_3 (eif_access(uint32_t_void_anonymous_callback_object));
	}
}

void set_uint32_t_void_anonymous_callback_entry_1 (void* a_feature){
	uint32_t_void_anonymous_callback_address_1 = (uint32_t_void_anonymous_callback_eiffel_feature) a_feature;
}

void set_uint32_t_void_anonymous_callback_entry_2 (void* a_feature){
	uint32_t_void_anonymous_callback_address_2 = (uint32_t_void_anonymous_callback_eiffel_feature) a_feature;
}

void set_uint32_t_void_anonymous_callback_entry_3 (void* a_feature){
	uint32_t_void_anonymous_callback_address_3 = (uint32_t_void_anonymous_callback_eiffel_feature) a_feature;
}

void* get_uint32_t_void_anonymous_callback_stub_1 (){
	return (void*) uint32_t_void_anonymous_callback_stub_1;
}

void* get_uint32_t_void_anonymous_callback_stub_2 (){
	return (void*) uint32_t_void_anonymous_callback_stub_2;
}

void* get_uint32_t_void_anonymous_callback_stub_3 (){
	return (void*) uint32_t_void_anonymous_callback_stub_3;
}

uint32_t call_uint32_t_void_anonymous_callback (void *a_function)
{
	return ((uint32_t (*) ())a_function) ();
}

void* uint32_t_uint32_tconst_anonymous_callback_object =  NULL;
uint32_t_uint32_tconst_anonymous_callback_eiffel_feature uint32_t_uint32_tconst_anonymous_callback_address_1 = NULL;
uint32_t_uint32_tconst_anonymous_callback_eiffel_feature uint32_t_uint32_tconst_anonymous_callback_address_2 = NULL;
uint32_t_uint32_tconst_anonymous_callback_eiffel_feature uint32_t_uint32_tconst_anonymous_callback_address_3 = NULL;

void set_uint32_t_uint32_tconst_anonymous_callback_object (void* a_object)
{
	if (a_object) {
		uint32_t_uint32_tconst_anonymous_callback_object = eif_protect(a_object);
	} else { 
		uint32_t_uint32_tconst_anonymous_callback_object = NULL;
	}
}

void release_uint32_t_uint32_tconst_anonymous_callback_object ()
{
	eif_wean (uint32_t_uint32_tconst_anonymous_callback_object);
}

uint32_t uint32_t_uint32_tconst_anonymous_callback_stub_1 (uint32_t const upper_bound)
{
	if (uint32_t_uint32_tconst_anonymous_callback_object != NULL && uint32_t_uint32_tconst_anonymous_callback_address_1 != NULL)
	{
		return uint32_t_uint32_tconst_anonymous_callback_address_1 (eif_access(uint32_t_uint32_tconst_anonymous_callback_object), upper_bound);
	}
}

uint32_t uint32_t_uint32_tconst_anonymous_callback_stub_2 (uint32_t const upper_bound)
{
	if (uint32_t_uint32_tconst_anonymous_callback_object != NULL && uint32_t_uint32_tconst_anonymous_callback_address_2 != NULL)
	{
		return uint32_t_uint32_tconst_anonymous_callback_address_2 (eif_access(uint32_t_uint32_tconst_anonymous_callback_object), upper_bound);
	}
}

uint32_t uint32_t_uint32_tconst_anonymous_callback_stub_3 (uint32_t const upper_bound)
{
	if (uint32_t_uint32_tconst_anonymous_callback_object != NULL && uint32_t_uint32_tconst_anonymous_callback_address_3 != NULL)
	{
		return uint32_t_uint32_tconst_anonymous_callback_address_3 (eif_access(uint32_t_uint32_tconst_anonymous_callback_object), upper_bound);
	}
}

void set_uint32_t_uint32_tconst_anonymous_callback_entry_1 (void* a_feature){
	uint32_t_uint32_tconst_anonymous_callback_address_1 = (uint32_t_uint32_tconst_anonymous_callback_eiffel_feature) a_feature;
}

void set_uint32_t_uint32_tconst_anonymous_callback_entry_2 (void* a_feature){
	uint32_t_uint32_tconst_anonymous_callback_address_2 = (uint32_t_uint32_tconst_anonymous_callback_eiffel_feature) a_feature;
}

void set_uint32_t_uint32_tconst_anonymous_callback_entry_3 (void* a_feature){
	uint32_t_uint32_tconst_anonymous_callback_address_3 = (uint32_t_uint32_tconst_anonymous_callback_eiffel_feature) a_feature;
}

void* get_uint32_t_uint32_tconst_anonymous_callback_stub_1 (){
	return (void*) uint32_t_uint32_tconst_anonymous_callback_stub_1;
}

void* get_uint32_t_uint32_tconst_anonymous_callback_stub_2 (){
	return (void*) uint32_t_uint32_tconst_anonymous_callback_stub_2;
}

void* get_uint32_t_uint32_tconst_anonymous_callback_stub_3 (){
	return (void*) uint32_t_uint32_tconst_anonymous_callback_stub_3;
}

uint32_t call_uint32_t_uint32_tconst_anonymous_callback (void *a_function, uint32_t const upper_bound)
{
	return ((uint32_t (*) (uint32_t const upper_bound))a_function) (upper_bound);
}

void* void_voidpconst_size_tconst_anonymous_callback_object =  NULL;
void_voidpconst_size_tconst_anonymous_callback_eiffel_feature void_voidpconst_size_tconst_anonymous_callback_address_1 = NULL;
void_voidpconst_size_tconst_anonymous_callback_eiffel_feature void_voidpconst_size_tconst_anonymous_callback_address_2 = NULL;
void_voidpconst_size_tconst_anonymous_callback_eiffel_feature void_voidpconst_size_tconst_anonymous_callback_address_3 = NULL;

void set_void_voidpconst_size_tconst_anonymous_callback_object (void* a_object)
{
	if (a_object) {
		void_voidpconst_size_tconst_anonymous_callback_object = eif_protect(a_object);
	} else { 
		void_voidpconst_size_tconst_anonymous_callback_object = NULL;
	}
}

void release_void_voidpconst_size_tconst_anonymous_callback_object ()
{
	eif_wean (void_voidpconst_size_tconst_anonymous_callback_object);
}

void void_voidpconst_size_tconst_anonymous_callback_stub_1 (void *const buf, size_t const size)
{
	if (void_voidpconst_size_tconst_anonymous_callback_object != NULL && void_voidpconst_size_tconst_anonymous_callback_address_1 != NULL)
	{
		void_voidpconst_size_tconst_anonymous_callback_address_1 (eif_access(void_voidpconst_size_tconst_anonymous_callback_object), buf, size);
	}
}

void void_voidpconst_size_tconst_anonymous_callback_stub_2 (void *const buf, size_t const size)
{
	if (void_voidpconst_size_tconst_anonymous_callback_object != NULL && void_voidpconst_size_tconst_anonymous_callback_address_2 != NULL)
	{
		void_voidpconst_size_tconst_anonymous_callback_address_2 (eif_access(void_voidpconst_size_tconst_anonymous_callback_object), buf, size);
	}
}

void void_voidpconst_size_tconst_anonymous_callback_stub_3 (void *const buf, size_t const size)
{
	if (void_voidpconst_size_tconst_anonymous_callback_object != NULL && void_voidpconst_size_tconst_anonymous_callback_address_3 != NULL)
	{
		void_voidpconst_size_tconst_anonymous_callback_address_3 (eif_access(void_voidpconst_size_tconst_anonymous_callback_object), buf, size);
	}
}

void set_void_voidpconst_size_tconst_anonymous_callback_entry_1 (void* a_feature){
	void_voidpconst_size_tconst_anonymous_callback_address_1 = (void_voidpconst_size_tconst_anonymous_callback_eiffel_feature) a_feature;
}

void set_void_voidpconst_size_tconst_anonymous_callback_entry_2 (void* a_feature){
	void_voidpconst_size_tconst_anonymous_callback_address_2 = (void_voidpconst_size_tconst_anonymous_callback_eiffel_feature) a_feature;
}

void set_void_voidpconst_size_tconst_anonymous_callback_entry_3 (void* a_feature){
	void_voidpconst_size_tconst_anonymous_callback_address_3 = (void_voidpconst_size_tconst_anonymous_callback_eiffel_feature) a_feature;
}

void* get_void_voidpconst_size_tconst_anonymous_callback_stub_1 (){
	return (void*) void_voidpconst_size_tconst_anonymous_callback_stub_1;
}

void* get_void_voidpconst_size_tconst_anonymous_callback_stub_2 (){
	return (void*) void_voidpconst_size_tconst_anonymous_callback_stub_2;
}

void* get_void_voidpconst_size_tconst_anonymous_callback_stub_3 (){
	return (void*) void_voidpconst_size_tconst_anonymous_callback_stub_3;
}

void call_void_voidpconst_size_tconst_anonymous_callback (void *a_function, void *const buf, size_t const size)
{
	((void (*) (void *const buf, size_t const size))a_function) (buf, size);
}

void* int_void_anonymous_callback_object =  NULL;
int_void_anonymous_callback_eiffel_feature int_void_anonymous_callback_address_1 = NULL;
int_void_anonymous_callback_eiffel_feature int_void_anonymous_callback_address_2 = NULL;
int_void_anonymous_callback_eiffel_feature int_void_anonymous_callback_address_3 = NULL;

void set_int_void_anonymous_callback_object (void* a_object)
{
	if (a_object) {
		int_void_anonymous_callback_object = eif_protect(a_object);
	} else { 
		int_void_anonymous_callback_object = NULL;
	}
}

void release_int_void_anonymous_callback_object ()
{
	eif_wean (int_void_anonymous_callback_object);
}

int int_void_anonymous_callback_stub_1 ()
{
	if (int_void_anonymous_callback_object != NULL && int_void_anonymous_callback_address_1 != NULL)
	{
		return int_void_anonymous_callback_address_1 (eif_access(int_void_anonymous_callback_object));
	}
}

int int_void_anonymous_callback_stub_2 ()
{
	if (int_void_anonymous_callback_object != NULL && int_void_anonymous_callback_address_2 != NULL)
	{
		return int_void_anonymous_callback_address_2 (eif_access(int_void_anonymous_callback_object));
	}
}

int int_void_anonymous_callback_stub_3 ()
{
	if (int_void_anonymous_callback_object != NULL && int_void_anonymous_callback_address_3 != NULL)
	{
		return int_void_anonymous_callback_address_3 (eif_access(int_void_anonymous_callback_object));
	}
}

void set_int_void_anonymous_callback_entry_1 (void* a_feature){
	int_void_anonymous_callback_address_1 = (int_void_anonymous_callback_eiffel_feature) a_feature;
}

void set_int_void_anonymous_callback_entry_2 (void* a_feature){
	int_void_anonymous_callback_address_2 = (int_void_anonymous_callback_eiffel_feature) a_feature;
}

void set_int_void_anonymous_callback_entry_3 (void* a_feature){
	int_void_anonymous_callback_address_3 = (int_void_anonymous_callback_eiffel_feature) a_feature;
}

void* get_int_void_anonymous_callback_stub_1 (){
	return (void*) int_void_anonymous_callback_stub_1;
}

void* get_int_void_anonymous_callback_stub_2 (){
	return (void*) int_void_anonymous_callback_stub_2;
}

void* get_int_void_anonymous_callback_stub_3 (){
	return (void*) int_void_anonymous_callback_stub_3;
}

int call_int_void_anonymous_callback (void *a_function)
{
	return ((int (*) ())a_function) ();
}

