#ifndef EWG_CALLBACK_UNKNOWN___
#define EWG_CALLBACK_UNKNOWN___

#include <sodium.h>

typedef void (*void_void_anonymous_callback_eiffel_feature) (void *a_class);

void* void_void_anonymous_callback_object;
void_void_anonymous_callback_eiffel_feature void_void_anonymous_callback_address_1;
void_void_anonymous_callback_eiffel_feature void_void_anonymous_callback_address_2;
void_void_anonymous_callback_eiffel_feature void_void_anonymous_callback_address_3;

void set_void_void_anonymous_callback_object (void* a_class);

void release_void_void_anonymous_callback_object (void);

void* get_void_void_anonymous_callback_stub_1 ();
void* get_void_void_anonymous_callback_stub_2 ();
void* get_void_void_anonymous_callback_stub_3 ();

void set_void_void_anonymous_callback_entry_1 (void* a_feature);
void set_void_void_anonymous_callback_entry_2 (void* a_feature);
void set_void_void_anonymous_callback_entry_3 (void* a_feature);

void call_void_void_anonymous_callback (void *a_function);


#include <sodium.h>

typedef void (*void_int_voidp_anonymous_callback_eiffel_feature) (void *a_class, int status, void *arg);

void* void_int_voidp_anonymous_callback_object;
void_int_voidp_anonymous_callback_eiffel_feature void_int_voidp_anonymous_callback_address_1;
void_int_voidp_anonymous_callback_eiffel_feature void_int_voidp_anonymous_callback_address_2;
void_int_voidp_anonymous_callback_eiffel_feature void_int_voidp_anonymous_callback_address_3;

void set_void_int_voidp_anonymous_callback_object (void* a_class);

void release_void_int_voidp_anonymous_callback_object (void);

void* get_void_int_voidp_anonymous_callback_stub_1 ();
void* get_void_int_voidp_anonymous_callback_stub_2 ();
void* get_void_int_voidp_anonymous_callback_stub_3 ();

void set_void_int_voidp_anonymous_callback_entry_1 (void* a_feature);
void set_void_int_voidp_anonymous_callback_entry_2 (void* a_feature);
void set_void_int_voidp_anonymous_callback_entry_3 (void* a_feature);

void call_void_int_voidp_anonymous_callback (void *a_function, int status, void *arg);


#include <sodium.h>

typedef int (*compar_fn_t_eiffel_feature) (void *a_class, void const *anonymous_1, void const *anonymous_2);

void* compar_fn_t_object;
compar_fn_t_eiffel_feature compar_fn_t_address_1;
compar_fn_t_eiffel_feature compar_fn_t_address_2;
compar_fn_t_eiffel_feature compar_fn_t_address_3;

void set_compar_fn_t_object (void* a_class);

void release_compar_fn_t_object (void);

void* get_compar_fn_t_stub_1 ();
void* get_compar_fn_t_stub_2 ();
void* get_compar_fn_t_stub_3 ();

void set_compar_fn_t_entry_1 (void* a_feature);
void set_compar_fn_t_entry_2 (void* a_feature);
void set_compar_fn_t_entry_3 (void* a_feature);

int call_compar_fn_t (void *a_function, void const *anonymous_1, void const *anonymous_2);


#include <sodium.h>

typedef char const *(*charconstp_void_anonymous_callback_eiffel_feature) (void *a_class);

void* charconstp_void_anonymous_callback_object;
charconstp_void_anonymous_callback_eiffel_feature charconstp_void_anonymous_callback_address_1;
charconstp_void_anonymous_callback_eiffel_feature charconstp_void_anonymous_callback_address_2;
charconstp_void_anonymous_callback_eiffel_feature charconstp_void_anonymous_callback_address_3;

void set_charconstp_void_anonymous_callback_object (void* a_class);

void release_charconstp_void_anonymous_callback_object (void);

void* get_charconstp_void_anonymous_callback_stub_1 ();
void* get_charconstp_void_anonymous_callback_stub_2 ();
void* get_charconstp_void_anonymous_callback_stub_3 ();

void set_charconstp_void_anonymous_callback_entry_1 (void* a_feature);
void set_charconstp_void_anonymous_callback_entry_2 (void* a_feature);
void set_charconstp_void_anonymous_callback_entry_3 (void* a_feature);

char const *call_charconstp_void_anonymous_callback (void *a_function);


#include <sodium.h>

typedef uint32_t (*uint32_t_void_anonymous_callback_eiffel_feature) (void *a_class);

void* uint32_t_void_anonymous_callback_object;
uint32_t_void_anonymous_callback_eiffel_feature uint32_t_void_anonymous_callback_address_1;
uint32_t_void_anonymous_callback_eiffel_feature uint32_t_void_anonymous_callback_address_2;
uint32_t_void_anonymous_callback_eiffel_feature uint32_t_void_anonymous_callback_address_3;

void set_uint32_t_void_anonymous_callback_object (void* a_class);

void release_uint32_t_void_anonymous_callback_object (void);

void* get_uint32_t_void_anonymous_callback_stub_1 ();
void* get_uint32_t_void_anonymous_callback_stub_2 ();
void* get_uint32_t_void_anonymous_callback_stub_3 ();

void set_uint32_t_void_anonymous_callback_entry_1 (void* a_feature);
void set_uint32_t_void_anonymous_callback_entry_2 (void* a_feature);
void set_uint32_t_void_anonymous_callback_entry_3 (void* a_feature);

uint32_t call_uint32_t_void_anonymous_callback (void *a_function);


#include <sodium.h>

typedef uint32_t (*uint32_t_uint32_tconst_anonymous_callback_eiffel_feature) (void *a_class, uint32_t const upper_bound);

void* uint32_t_uint32_tconst_anonymous_callback_object;
uint32_t_uint32_tconst_anonymous_callback_eiffel_feature uint32_t_uint32_tconst_anonymous_callback_address_1;
uint32_t_uint32_tconst_anonymous_callback_eiffel_feature uint32_t_uint32_tconst_anonymous_callback_address_2;
uint32_t_uint32_tconst_anonymous_callback_eiffel_feature uint32_t_uint32_tconst_anonymous_callback_address_3;

void set_uint32_t_uint32_tconst_anonymous_callback_object (void* a_class);

void release_uint32_t_uint32_tconst_anonymous_callback_object (void);

void* get_uint32_t_uint32_tconst_anonymous_callback_stub_1 ();
void* get_uint32_t_uint32_tconst_anonymous_callback_stub_2 ();
void* get_uint32_t_uint32_tconst_anonymous_callback_stub_3 ();

void set_uint32_t_uint32_tconst_anonymous_callback_entry_1 (void* a_feature);
void set_uint32_t_uint32_tconst_anonymous_callback_entry_2 (void* a_feature);
void set_uint32_t_uint32_tconst_anonymous_callback_entry_3 (void* a_feature);

uint32_t call_uint32_t_uint32_tconst_anonymous_callback (void *a_function, uint32_t const upper_bound);


#include <sodium.h>

typedef void (*void_voidpconst_size_tconst_anonymous_callback_eiffel_feature) (void *a_class, void *const buf, size_t const size);

void* void_voidpconst_size_tconst_anonymous_callback_object;
void_voidpconst_size_tconst_anonymous_callback_eiffel_feature void_voidpconst_size_tconst_anonymous_callback_address_1;
void_voidpconst_size_tconst_anonymous_callback_eiffel_feature void_voidpconst_size_tconst_anonymous_callback_address_2;
void_voidpconst_size_tconst_anonymous_callback_eiffel_feature void_voidpconst_size_tconst_anonymous_callback_address_3;

void set_void_voidpconst_size_tconst_anonymous_callback_object (void* a_class);

void release_void_voidpconst_size_tconst_anonymous_callback_object (void);

void* get_void_voidpconst_size_tconst_anonymous_callback_stub_1 ();
void* get_void_voidpconst_size_tconst_anonymous_callback_stub_2 ();
void* get_void_voidpconst_size_tconst_anonymous_callback_stub_3 ();

void set_void_voidpconst_size_tconst_anonymous_callback_entry_1 (void* a_feature);
void set_void_voidpconst_size_tconst_anonymous_callback_entry_2 (void* a_feature);
void set_void_voidpconst_size_tconst_anonymous_callback_entry_3 (void* a_feature);

void call_void_voidpconst_size_tconst_anonymous_callback (void *a_function, void *const buf, size_t const size);


#include <sodium.h>

typedef int (*int_void_anonymous_callback_eiffel_feature) (void *a_class);

void* int_void_anonymous_callback_object;
int_void_anonymous_callback_eiffel_feature int_void_anonymous_callback_address_1;
int_void_anonymous_callback_eiffel_feature int_void_anonymous_callback_address_2;
int_void_anonymous_callback_eiffel_feature int_void_anonymous_callback_address_3;

void set_int_void_anonymous_callback_object (void* a_class);

void release_int_void_anonymous_callback_object (void);

void* get_int_void_anonymous_callback_stub_1 ();
void* get_int_void_anonymous_callback_stub_2 ();
void* get_int_void_anonymous_callback_stub_3 ();

void set_int_void_anonymous_callback_entry_1 (void* a_feature);
void set_int_void_anonymous_callback_entry_2 (void* a_feature);
void set_int_void_anonymous_callback_entry_3 (void* a_feature);

int call_int_void_anonymous_callback (void *a_function);


#endif
