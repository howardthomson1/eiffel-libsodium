note

	description: "This file has been generated by EWG. Do not edit. Changes will be lost!"

	generator: "Eiffel Wrapper Generator"
-- functions wrapper
class CRYPTO_STREAM_SALSA2012_FUNCTIONS_API


feature -- Access

	crypto_stream_salsa2012_keybytes: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_stream_salsa2012_keybytes ();
			]"
		end

	crypto_stream_salsa2012_noncebytes: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_stream_salsa2012_noncebytes ();
			]"
		end

	crypto_stream_salsa2012_messagebytes_max: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_stream_salsa2012_messagebytes_max ();
			]"
		end

	crypto_stream_salsa2012 (c: POINTER; clen: INTEGER; n: POINTER; k: POINTER): INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_stream_salsa2012 ((unsigned char*)$c, (unsigned long long)$clen, (unsigned char const*)$n, (unsigned char const*)$k);
			]"
		end

	crypto_stream_salsa2012_xor (c: POINTER; m: POINTER; mlen: INTEGER; n: POINTER; k: POINTER): INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_stream_salsa2012_xor ((unsigned char*)$c, (unsigned char const*)$m, (unsigned long long)$mlen, (unsigned char const*)$n, (unsigned char const*)$k);
			]"
		end

	crypto_stream_salsa2012_keygen (k: C_STRING) 
		do
			c_crypto_stream_salsa2012_keygen (k.item)
		ensure
			instance_free: class
		end

feature -- Externals

	c_crypto_stream_salsa2012_keygen (k: POINTER)
		external
			"C inline use <sodium.h>"
		alias
			"[
				crypto_stream_salsa2012_keygen ($k);
			]"
		end

feature -- Externals Address

end
