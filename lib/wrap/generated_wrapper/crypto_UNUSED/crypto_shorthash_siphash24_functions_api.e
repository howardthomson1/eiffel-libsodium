note

	description: "This file has been generated by EWG. Do not edit. Changes will be lost!"

	generator: "Eiffel Wrapper Generator"
-- functions wrapper
class CRYPTO_SHORTHASH_SIPHASH24_FUNCTIONS_API


feature -- Access

	crypto_shorthash_siphash24_bytes: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_shorthash_siphash24_bytes ();
			]"
		end

	crypto_shorthash_siphash24_keybytes: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_shorthash_siphash24_keybytes ();
			]"
		end

	crypto_shorthash_siphash24 (a_out: POINTER; in: POINTER; inlen: INTEGER; k: POINTER): INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_shorthash_siphash24 ((unsigned char*)$a_out, (unsigned char const*)$in, (unsigned long long)$inlen, (unsigned char const*)$k);
			]"
		end

	crypto_shorthash_siphashx24_bytes: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_shorthash_siphashx24_bytes ();
			]"
		end

	crypto_shorthash_siphashx24_keybytes: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_shorthash_siphashx24_keybytes ();
			]"
		end

	crypto_shorthash_siphashx24 (a_out: POINTER; in: POINTER; inlen: INTEGER; k: POINTER): INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_shorthash_siphashx24 ((unsigned char*)$a_out, (unsigned char const*)$in, (unsigned long long)$inlen, (unsigned char const*)$k);
			]"
		end

feature -- Externals

feature -- Externals Address

end
