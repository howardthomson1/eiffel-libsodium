note

	description: "This file has been generated by EWG. Do not edit. Changes will be lost!"

	generator: "Eiffel Wrapper Generator"
-- functions wrapper
class CRYPTO_PWHASH_ARGON2I_FUNCTIONS_API


feature -- Access

	crypto_pwhash_argon2i_alg_argon2i13: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_alg_argon2i13 ();
			]"
		end

	crypto_pwhash_argon2i_bytes_min: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_bytes_min ();
			]"
		end

	crypto_pwhash_argon2i_bytes_max: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_bytes_max ();
			]"
		end

	crypto_pwhash_argon2i_passwd_min: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_passwd_min ();
			]"
		end

	crypto_pwhash_argon2i_passwd_max: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_passwd_max ();
			]"
		end

	crypto_pwhash_argon2i_saltbytes: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_saltbytes ();
			]"
		end

	crypto_pwhash_argon2i_strbytes: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_strbytes ();
			]"
		end

	crypto_pwhash_argon2i_strprefix: POINTER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_strprefix ();
			]"
		end

	crypto_pwhash_argon2i_opslimit_min: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_opslimit_min ();
			]"
		end

	crypto_pwhash_argon2i_opslimit_max: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_opslimit_max ();
			]"
		end

	crypto_pwhash_argon2i_memlimit_min: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_memlimit_min ();
			]"
		end

	crypto_pwhash_argon2i_memlimit_max: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_memlimit_max ();
			]"
		end

	crypto_pwhash_argon2i_opslimit_interactive: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_opslimit_interactive ();
			]"
		end

	crypto_pwhash_argon2i_memlimit_interactive: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_memlimit_interactive ();
			]"
		end

	crypto_pwhash_argon2i_opslimit_moderate: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_opslimit_moderate ();
			]"
		end

	crypto_pwhash_argon2i_memlimit_moderate: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_memlimit_moderate ();
			]"
		end

	crypto_pwhash_argon2i_opslimit_sensitive: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_opslimit_sensitive ();
			]"
		end

	crypto_pwhash_argon2i_memlimit_sensitive: INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_memlimit_sensitive ();
			]"
		end

	crypto_pwhash_argon2i (a_out: POINTER; outlen: INTEGER; passwd: POINTER; passwdlen: INTEGER; salt: POINTER; opslimit: INTEGER; memlimit: INTEGER; alg: INTEGER): INTEGER 
		do
			Result := c_crypto_pwhash_argon2i (a_out, outlen, passwd, passwdlen, salt, opslimit, memlimit, alg)
		ensure
			instance_free: class
		end

	crypto_pwhash_argon2i_str (a_out: C_STRING; passwd: POINTER; passwdlen: INTEGER; opslimit: INTEGER; memlimit: INTEGER): INTEGER 
		do
			Result := c_crypto_pwhash_argon2i_str (a_out.item, passwd, passwdlen, opslimit, memlimit)
		ensure
			instance_free: class
		end

	crypto_pwhash_argon2i_str_verify (str: MANAGED_POINTER; passwd: POINTER; passwdlen: INTEGER): INTEGER 
		do
			Result := c_crypto_pwhash_argon2i_str_verify (str.item, passwd, passwdlen)
		ensure
			instance_free: class
		end

	crypto_pwhash_argon2i_str_needs_rehash (str: MANAGED_POINTER; opslimit: INTEGER; memlimit: INTEGER): INTEGER 
		do
			Result := c_crypto_pwhash_argon2i_str_needs_rehash (str.item, opslimit, memlimit)
		ensure
			instance_free: class
		end

feature -- Externals

	c_crypto_pwhash_argon2i (a_out: POINTER; outlen: INTEGER; passwd: POINTER; passwdlen: INTEGER; salt: POINTER; opslimit: INTEGER; memlimit: INTEGER; alg: INTEGER): INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i ((unsigned char*const )$a_out, (unsigned long long)$outlen, (char const*const )$passwd, (unsigned long long)$passwdlen, (unsigned char const*const )$salt, (unsigned long long)$opslimit, (size_t)$memlimit, (int)$alg);
			]"
		end

	c_crypto_pwhash_argon2i_str (a_out: POINTER; passwd: POINTER; passwdlen: INTEGER; opslimit: INTEGER; memlimit: INTEGER): INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_str ($a_out, (char const*const )$passwd, (unsigned long long)$passwdlen, (unsigned long long)$opslimit, (size_t)$memlimit);
			]"
		end

	c_crypto_pwhash_argon2i_str_verify (str: POINTER; passwd: POINTER; passwdlen: INTEGER): INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_str_verify ($str, (char const*const )$passwd, (unsigned long long)$passwdlen);
			]"
		end

	c_crypto_pwhash_argon2i_str_needs_rehash (str: POINTER; opslimit: INTEGER; memlimit: INTEGER): INTEGER
		external
			"C inline use <sodium.h>"
		alias
			"[
				return crypto_pwhash_argon2i_str_needs_rehash ($str, (unsigned long long)$opslimit, (size_t)$memlimit);
			]"
		end

feature -- Externals Address

end
