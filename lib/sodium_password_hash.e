note
	description: "Summary description for {SODIUM_PASSWORD_HASH}."
	author: "Howard Thomson and others"
	date: "29-Nov-2020"

class
	SODIUM_PASSWORD_HASH

inherit
	LIBSODIUM
		undefine is_equal, copy end
	MANAGED_POINTER
		export {MANAGED_POINTER} all
		end

create
	make_store,
	make_from_stored

feature -- Creation

	make_store (a_password: STRING; a_salt: SODIUM_SALT)
		local
			i: INTEGER
		do
			make (crypto_pwhash_strbytes)
			i := c_crypto_pwhash_str (item, a_password.area.base_address, a_password.count,
				crypto_pwhash_opslimit_interactive,
				crypto_pwhash_memlimit_interactive)
			check not_out_of_memory: i = 0 end
		end

	make_from_stored (a_buffer: MANAGED_POINTER)
		require
			count_correct: a_buffer.count = crypto_pwhash_strbytes
		do
			make (crypto_pwhash_strbytes)
				-- Copy buffer contents to Current
			item.memory_copy (a_buffer.item, count)
		end

feature -- Verification

	verify (a_password: STRING): BOOLEAN
		do
			Result := c_crypto_pwhash_str_verify (item, a_password.area.base_address, a_password.count) = 0
		end

end
