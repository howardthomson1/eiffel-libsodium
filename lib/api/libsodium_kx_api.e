note
	description: "libsodium API from /usr/include/sodium/crypto_kx.h"
	additional_description: "Key Exchange API"
	author: "Howard Thomson and others"
	date: "24-Nov-2020"

	TODO: "[ ]"

class LIBSODIUM_KX_API

feature -- Constants

	crypto_kx_publickeybytes: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_kx_PUBLICKEYBYTES " end

	crypto_kx_secretkeybytes: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_kx_SECRETKEYBYTES " end

	crypto_kx_seedbytes: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_kx_SEEDBYTES " end

	crypto_kx_sessionkeybytes: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_kx_SESSIONKEYBYTES " end

feature -- Routines

--			int crypto_kx_seed_keypair(unsigned char pk[crypto_kx_PUBLICKEYBYTES],
--			                           unsigned char sk[crypto_kx_SECRETKEYBYTES],
--			                           const unsigned char seed[crypto_kx_SEEDBYTES]);

	crypto_kx_seed_keypair (a_public_key, a_secret_key, a_seed: POINTER): INTEGER
		external "C inline use <sodium.h>" end



--			int crypto_kx_keypair(unsigned char pk[crypto_kx_PUBLICKEYBYTES],
--			                      unsigned char sk[crypto_kx_SECRETKEYBYTES]);

	crypto_kx_keypair (a_public_key, a_secret_key: POINTER): INTEGER
		external "C inline use <sodium.h>" end



--			int crypto_kx_client_session_keys(unsigned char rx[crypto_kx_SESSIONKEYBYTES],
--			                                  unsigned char tx[crypto_kx_SESSIONKEYBYTES],
--			                                  const unsigned char client_pk[crypto_kx_PUBLICKEYBYTES],
--			                                  const unsigned char client_sk[crypto_kx_SECRETKEYBYTES],
--			                                  const unsigned char server_pk[crypto_kx_PUBLICKEYBYTES])

--	Compute two shared keys using the server's public key and the client's secret key.
--   		client_rx will be used by the client to receive data from the server,
--   		client_tx will by used by the client to send data to the server.


	crypto_kx_client_session_keys (a_rx_session_key, a_tx_session_key, a_client_public_key, a_client_secret_key, a_server_public_key: POINTER): INTEGER
		external "C inline use <sodium.h>" end


--	   Compute two shared keys using the client's public key and the server's secret key.
--	   server_rx will be used by the server to receive data from the client,
--	   server_tx will by used by the server to send data to the client.

--			int crypto_kx_server_session_keys(unsigned char rx[crypto_kx_SESSIONKEYBYTES],
--			                                  unsigned char tx[crypto_kx_SESSIONKEYBYTES],
--			                                  const unsigned char server_pk[crypto_kx_PUBLICKEYBYTES],
--			                                  const unsigned char server_sk[crypto_kx_SECRETKEYBYTES],
--			                                  const unsigned char client_pk[crypto_kx_PUBLICKEYBYTES])

	crypto_kx_server_session_keys (a_rx_session_key, a_tx_session_key, a_server_public_key, a_server_secret_key, a_client_public_key: POINTER): INTEGER
		external "C inline use <sodium.h>" end



end
