note
	description: "libsodium API from /usr/include/sodium/crypto_auth.h"
	author: "Howard Thomson and others"
	date: "24-Nov-2020"

	TODO: "[
		crypto_auth_keybytes
		crypto_auth_bytes
		
		crypto_auth_keygen
		crypto_auth
		crypto_auth_verify
	]"

class LIBSODIUM_AUTH_API

inherit
	CRYPTO_AUTH_FUNCTIONS_API

feature -- Constants

--	auth_keybytes: INTEGER
--			-- Authentication key size
--		external "C inline use <sodium.h>"
--		alias "crypto_auth_KEYBYTES" end

--	auth_bytes: INTEGER
--			-- mac: message authentication code size
--		external "C inline use <sodium.h>"
--		alias "crypto_auth_BYTES" end

feature -- Routines

--	void crypto_auth_keygen(unsigned char k[crypto_auth_KEYBYTES]);


--	int crypto_auth(unsigned char *out, const unsigned char *in,
--	                unsigned long long inlen, const unsigned char *k);


--	int crypto_auth_verify(const unsigned char *h, const unsigned char *in,
--	                       unsigned long long inlen, const unsigned char *k)






end
