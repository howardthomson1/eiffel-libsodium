class LIBSODIUM_RANDOMBYTES_API

feature -- Random

	randombytes_random: NATURAL_32
		external "C inline use <sodium.h>"
		end

	randombytes_uniform (upper_bound: NATURAL_32): NATURAL_32
		external "C inline use <sodium.h>"

		end

	randombytes_buf (buf: POINTER; size: INTEGER_64)
		external "C inline use <sodium.h>"
		end

	randombytes_buf_deterministic (buf: POINTER; size: INTEGER_64; a_seed: POINTER)
		external "C inline use <sodium.h>"
		end

	randombytes_seedbytes: INTEGER
		external "C inline use <sodium.h>"
		alias "randombytes_SEEDBYTES " end

end
