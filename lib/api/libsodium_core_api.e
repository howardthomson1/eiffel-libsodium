note
	description: "libsodium API from /usr/include/sodium/core.h"
	author: "Howard Thomson and others"
	date: "24-Nov-2020"

	TODO: "[
		sodium_set_misuse_handler
		sodium_misuse
	]"

deferred class LIBSODIUM_CORE_API

feature -- Initialisation

	sodium_init: INTEGER
		external "C inline use <sodium.h>"
		alias "sodium_init()" end

end
