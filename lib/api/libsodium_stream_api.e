class LIBSODIUM_STREAM_API

feature -- constants re encryption stream

	secretstream_xchacha20poly1305_ABYTES: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretstream_xchacha20poly1305_ABYTES " end

	secretstream_xchacha20poly1305_KEYBYTES: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretstream_xchacha20poly1305_KEYBYTES " end

	secretstream_xchacha20poly1305_MESSAGEBYTES_MAX: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretstream_xchacha20poly1305_MESSAGEBYTES_MAX " end

	secretstream_xchacha20poly1305_TAG_MESSAGE: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretstream_xchacha20poly1305_TAG_MESSAGE " end

	secretstream_xchacha20poly1305_TAG_PUSH: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretstream_xchacha20poly1305_TAG_PUSH " end

	secretstream_xchacha20poly1305_TAG_REKEY: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretstream_xchacha20poly1305_TAG_REKEY " end

	secretstream_xchacha20poly1305_TAG_FINAL: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretstream_xchacha20poly1305_TAG_FINAL " end

end
