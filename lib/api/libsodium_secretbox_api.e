note
	description: "libsodium API from /usr/include/sodium/crypto_secretbox.h"
	author: "Howard Thomson and others"
	date: "24-Nov-2020"

class LIBSODIUM_SECRETBOX_API

feature -- Secret Box

		-- Constants
	secretbox_keybytes: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretbox_KEYBYTES " end

	secretbox_macbytes: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretbox_MACBYTES " end

	secretbox_noncebytes: INTEGER
		external "C inline use <sodium.h>"
		alias "crypto_secretbox_NONCEBYTES " end

feature	-- Routines

	crypto_secretbox_keygen (a_ptr: POINTER)
		external "C inline use <sodium.h>"
		alias "crypto_secretbox_keygen " end

	crypto_secretbox_easy (a_encrypted_ptr, a_msg_ptr: POINTER; a_msg_size: INTEGER_64; a_nonce_ptr, a_key_ptr: POINTER): INTEGER
		external "C use <sodium.h>"
		alias "crypto_secretbox_easy " end

	crypto_secretbox_open_easy (a_decrypted_ptr, a_encrypted_ptr: POINTER; a_encrypted_size: INTEGER_64; a_nonce_ptr, a_key_ptr: POINTER): INTEGER
		external "C use <sodium.h>"
		alias "crypto_secretbox_open_easy " end


end
