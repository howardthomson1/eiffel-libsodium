note
	description: "libsodium API from /usr/include/sodium/crypto_box.h"
	author: "Howard Thomson and others"
	date: "24-Nov-2020"

	TODO: "[
		crypto_box_seedbytes
		crypto_box_noncebytes
		crypto_box_messagebytes_max
		crypto_box_beforenmbytes
		crypto_box_sealbytes
		crypto_box_


		
		crypto_box_seed_keypair
		crypto_box_keypair
		crypto_box_easy
		crypto_box_open_easy
		crypto_box_detached
		crypto_box_open_detached
		crypto_box_
		crypto_box_
		crypto_box_
		crypto_box_

	]"

class LIBSODIUM_BOX_API

inherit
	CRYPTO_BOX_FUNCTIONS_API

feature -- Public/Private key encryption/authentication

--	crypto_box_publickeybytes: INTEGER
--		external "C inline use <sodium.h>"
--		alias "crypto_box_PUBLICKEYBYTES " end

--	crypto_box_secretkeybytes: INTEGER
--		external "C inline use <sodium.h>"
--		alias "crypto_box_SECRETKEYBYTES " end

--	crypto_box_macbytes: INTEGER
--		external "C inline use <sodium.h>"
--		alias "crypto_box_MACBYTES " end

--	crypto_box_seedbytes: INTEGER
--		external "C inline use <sodium.h>"
--		alias "crypto_box_SEEDBYTES " end

--	crypto_box_noncebytes: INTEGER
--		external "C inline use <sodium.h>"
--		alias "crypto_box_NONCEBYTES " end

--	crypto_box_messagebytes_max: INTEGER
--		external "C inline use <sodium.h>"
--		alias "crypto_box_MESSAGEBYTES_MAX " end

feature -- Routines

--	crypto_box_keypair (a_publickey_ptr, a_privatekey_ptr: POINTER)
--		external "C inline use <sodium.h>"
--		alias "crypto_box_keypair " end

--=================================================================================================







--	int crypto_box_seed_keypair(unsigned char *pk, unsigned char *sk,
--	                            const unsigned char *seed);

--	int crypto_box_keypair(unsigned char *pk, unsigned char *sk);

--	int crypto_box_easy(unsigned char *c, const unsigned char *m,
--	                    unsigned long long mlen, const unsigned char *n,
--	                    const unsigned char *pk, const unsigned char *sk)

--	int crypto_box_open_easy(unsigned char *m, const unsigned char *c,
--	                         unsigned long long clen, const unsigned char *n,
--	                         const unsigned char *pk, const unsigned char *sk)

--	int crypto_box_detached(unsigned char *c, unsigned char *mac,
--	                        const unsigned char *m, unsigned long long mlen,
--	                        const unsigned char *n, const unsigned char *pk,
--	                        const unsigned char *sk)

--	int crypto_box_open_detached(unsigned char *m, const unsigned char *c,
--	                             const unsigned char *mac,
--	                             unsigned long long clen,
--	                             const unsigned char *n,
--	                             const unsigned char *pk,
--	                             const unsigned char *sk)



feature -- TODO: Precomputation interface

--	#define crypto_box_BEFORENMBYTES crypto_box_curve25519xsalsa20poly1305_BEFORENMBYTES

--	int crypto_box_beforenm(unsigned char *k, const unsigned char *pk,
--	                        const unsigned char *sk)

--	int crypto_box_easy_afternm(unsigned char *c, const unsigned char *m,
--	                            unsigned long long mlen, const unsigned char *n,
--	                            const unsigned char *k);

--	int crypto_box_open_easy_afternm(unsigned char *m, const unsigned char *c,
--	                                 unsigned long long clen, const unsigned char *n,
--	                                 const unsigned char *k)

--	int crypto_box_detached_afternm(unsigned char *c, unsigned char *mac,
--	                                const unsigned char *m, unsigned long long mlen,
--	                                const unsigned char *n, const unsigned char *k);

--	int crypto_box_open_detached_afternm(unsigned char *m, const unsigned char *c,
--	                                     const unsigned char *mac,
--	                                     unsigned long long clen, const unsigned char *n,
--	                                     const unsigned char *k)

feature -- TODO: Ephemeral SK interface

--	#define crypto_box_SEALBYTES (crypto_box_PUBLICKEYBYTES + crypto_box_MACBYTES)

--	int crypto_box_seal(unsigned char *c, const unsigned char *m,
--	                    unsigned long long mlen, const unsigned char *pk);

--	int crypto_box_seal_open(unsigned char *m, const unsigned char *c,
--	                         unsigned long long clen,
--	                         const unsigned char *pk, const unsigned char *sk)
--	            __attribute__ ((warn_unused_result));



feature -- TODO (Possibly): NaCl compatibility interface ; Requires padding

--	#define crypto_box_ZEROBYTES crypto_box_curve25519xsalsa20poly1305_ZEROBYTES

--	#define crypto_box_BOXZEROBYTES crypto_box_curve25519xsalsa20poly1305_BOXZEROBYTES

--	int crypto_box(unsigned char *c, const unsigned char *m,
--	               unsigned long long mlen, const unsigned char *n,
--	               const unsigned char *pk, const unsigned char *sk)

--	int crypto_box_open(unsigned char *m, const unsigned char *c,
--	                    unsigned long long clen, const unsigned char *n,
--	                    const unsigned char *pk, const unsigned char *sk)
--	            __attribute__ ((warn_unused_result));

--	int crypto_box_afternm(unsigned char *c, const unsigned char *m,
--	                       unsigned long long mlen, const unsigned char *n,
--	                       const unsigned char *k);

--	int crypto_box_open_afternm(unsigned char *m, const unsigned char *c,
--	                            unsigned long long clen, const unsigned char *n,
--	                            const unsigned char *k)




end
