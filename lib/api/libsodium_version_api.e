note
	description: "libsodium API from /usr/include/sodium/version.h"
	author: "Howard Thomson and others"
	date: "26-Nov-2020"

	TODO: "[
		sodium_version_string
		sodium_library_version_major
		sodium_library_version_minor
		sodium_library_minimal
	]"


class LIBSODIUM_VERSION_API

inherit
	VERSION_FUNCTIONS_API

feature

--	const char *sodium_version_string(void);

--	int         sodium_library_version_major(void);

--	int         sodium_library_version_minor(void);

--	int         sodium_library_minimal(void);

end
