note
	description: "libsodium API from /usr/include/sodium/utils.h"
	author: "Howard Thomson and others"
	date: "24-Nov-2020"

	TODO: "[
		--	sodium_bin2base64
		--	sodium_base642bin
		--	sodium_is_zero
]"

class LIBSODIUM_UTILS_API

feature -- Utility

		--	sodium_memcmp: constant time memory comparison
		-- Result = 0 implies match, -1 implies no-match
	sodium_memcmp (a_ptr1, a_ptr2: POINTER; a_length: INTEGER_64): INTEGER
		require
			valid_pointers: a_ptr1 /= default_pointer and a_ptr2 /= default_pointer
			valid_length: a_length > 0
		external "C use <sodium.h>"
		end

	sodium_bin2hex (a_hex: POINTER; a_hex_max: INTEGER_64; a_bin: POINTER; a_bin_size: INTEGER_64)
		external "C use <sodium.h>"
		end

	sodium_hex2bin (a_ptr_bin: POINTER; a_bin_maxlen: INTEGER_64;
					a_ptr_hex: POINTER; a_hex_len: INTEGER_64;
					a_ignore: POINTER; a_bin_len_ptr: POINTER;
					a_hex_end_ptr: POINTER): INTEGER
		external "C use <sodium.h>"
		end


	sodium_increment (a_ptr: POINTER; a_length: INTEGER_64)
		require
			valid_pointer: a_ptr /= default_pointer
			valid_length: a_length > 0
		external "C use <sodium.h>"
		end


	sodium_add (a_ptr1, a_ptr2: POINTER; a_length: INTEGER_64)
		require
			valid_pointers: a_ptr1 /= default_pointer and a_ptr2 /= default_pointer
			valid_length: a_length > 0
		external "C use <sodium.h>"
		end


	sodium_compare (a_ptr, b_ptr: POINTER; a_size: INTEGER_64): INTEGER
			-- Compare two byte strings in constant time
		external "C use <sodium.h>"
				--		alias "sodium_compare(void *, void *, size_t)"
		end

end
